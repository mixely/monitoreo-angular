import { VentasPage } from './app.po';

describe('ventas App', () => {
  let page: VentasPage;

  beforeEach(() => {
    page = new VentasPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
