import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgrRelMonitCartComponent } from './agr-rel-monit-cart.component';

describe('AgrRelMonitCartComponent', () => {
  let component: AgrRelMonitCartComponent;
  let fixture: ComponentFixture<AgrRelMonitCartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgrRelMonitCartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgrRelMonitCartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
