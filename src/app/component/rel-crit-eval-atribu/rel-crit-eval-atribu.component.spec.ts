import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RelCritEvalAtribuComponent } from './rel-crit-eval-atribu.component';

describe('RelCritEvalAtribuComponent', () => {
  let component: RelCritEvalAtribuComponent;
  let fixture: ComponentFixture<RelCritEvalAtribuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RelCritEvalAtribuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RelCritEvalAtribuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
