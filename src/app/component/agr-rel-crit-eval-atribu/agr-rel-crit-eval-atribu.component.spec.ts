import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgrRelCritEvalAtribuComponent } from './agr-rel-crit-eval-atribu.component';

describe('AgrRelCritEvalAtribuComponent', () => {
  let component: AgrRelCritEvalAtribuComponent;
  let fixture: ComponentFixture<AgrRelCritEvalAtribuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgrRelCritEvalAtribuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgrRelCritEvalAtribuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
