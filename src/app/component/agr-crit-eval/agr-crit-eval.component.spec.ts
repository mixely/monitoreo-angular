import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgrCritEvalComponent } from './agr-crit-eval.component';

describe('AgrCritEvalComponent', () => {
  let component: AgrCritEvalComponent;
  let fixture: ComponentFixture<AgrCritEvalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgrCritEvalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgrCritEvalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
