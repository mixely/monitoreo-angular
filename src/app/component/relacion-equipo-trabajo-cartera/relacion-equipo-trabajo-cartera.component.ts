import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { NgForm, FormGroup, FormControl, Validators } from "@angular/forms";

import { RelacionEquipoTrabajoCarteraService } from '../../services/relacion-equipo-trabajo-cartera.service';
import { ProveedorService } from '../../services/proveedor.service';
import { CarteraService } from '../../services/cartera.service';
import { EquipoTrabajoService } from '../../services/equipo-trabajo.service';
import { LoginService } from '../../services/login.service';
import { PerfilesService } from '../../services/perfiles.service';
import { UsuarioService } from '../../services/usuario.service';
import { SegmentoService } from '../../services/segmento.service';

import { RelacionEquipoTrabajoCarteraInterface } from '../../interfaces/relacion-equipo-trabajo-cartera.interface';
import { Message, MenuItem, DataTable } from 'primeng/primeng';

@Component({
  selector: 'app-relacion-equipo-trabajo-cartera',
  templateUrl: './relacion-equipo-trabajo-cartera.component.html',
  host: {'(window:keydown)': 'hotkeys($event)'}
})
export class RelacionEquipoTrabajoCarteraComponent implements OnInit {

  Formulario:FormGroup;

  display_modal:boolean = false;
  accion:string = "crear";
  msjs:Message[] = [];

  proveedores:any[] = [];
  carteras:any[] = [];
  perfiles:any[] = [];
  usuarios:any[] = [];
  equipos:any[] = [];
  segmentos:any[] = [];

  selectedRecord:RelacionEquipoTrabajoCarteraInterface;
  Tabla:RelacionEquipoTrabajoCarteraInterface[] = [];
  csv:boolean = true; // Si es "true" entonces va a desahabilitar

  can_edit = false;

  constructor(private cdRef:ChangeDetectorRef,
              private _retcs:RelacionEquipoTrabajoCarteraService,
              private _ps:ProveedorService,
              private _es:EquipoTrabajoService,
              private _cs:CarteraService,
              private _pfs:PerfilesService,
              private _us:UsuarioService,
              private _ls:LoginService,
              private _ss:SegmentoService
              ) {
    this.Formulario = new FormGroup({
      'id': new FormControl(this.objeto.id),
      'id_proveedor': new FormControl(this.objeto.id_proveedor, Validators.required),
      'id_cartera': new FormControl(this.objeto.id_cartera, Validators.required),
      'id_perfil': new FormControl(this.objeto.id_perfil, Validators.required),
      'id_usuario': new FormControl(this.objeto.id_usuario, Validators.required),
      'id_equipo': new FormControl(this.objeto.id_equipo, Validators.required),
      'id_segmento': new FormControl(this.objeto.id_segmento, Validators.required)
    });
  }

  hotkeys(event){
    // ALT + C
    if(event.altKey && event.keyCode==107){
       this.display_modal = true;
    }
  }

  objeto:RelacionEquipoTrabajoCarteraInterface = {
    id:"",
    id_proveedor:"",
    id_cartera:"",
    id_perfil:"",
    id_usuario:"",
    id_equipo:"",
    id_segmento:""
  }

  limpiarForm(){
    this.Formulario.reset({
      id:"",
      id_proveedor:"",
      id_cartera:"",
      id_perfil:"",
      id_usuario:"",
      id_equipo:"",
      id_segmento:""
    });

    setTimeout(()=>{
      document.getElementById("id_proveedor").focus();
    },500);
  };

  ngOnInit() {
    this.get();
    this._ps.get()
            .subscribe(
              data => {
                this.proveedores = data.datos;
              },
              error =>{
                this._ls.handleError(error);
              });
    this._pfs.get()
             .subscribe(
              data =>{
                this.perfiles = data.perfiles;
              },
              error =>{
                this._ls.handleError(error);
              });

    this._es.get()
            .subscribe(
              data => {
                this.equipos = data.datos;
              },
              error => {
                this._ls.handleError(error);
              });
    }

  ngAfterViewChecked(){
   this.cdRef.detectChanges();
  }

  crearModal(){
    this.display_modal = true;
    this.limpiarForm();
  }

  generarCSV(dt:DataTable){
    dt.exportCSV();
  }

  editarModal(dt:DataTable){
     this.accion = "editar";
     let obj = this.Clonar(dt.selection);
     // this.objeto["distrito"] = [...this.objeto["distrito"], obj.distrito]
     this.Formulario.setValue(obj);
     this.display_modal = true;
  }

  Edit(event){
     this.can_edit = true
     if(event.originalEvent.type=="dblclick"){
        this.accion = "editar";
        let obj = this.Clonar(event.data);
        this.Formulario.setValue(obj);
        this.display_modal = true;
     }
  }

  Selected(obj){
     this.objeto.id = obj.id;
  }
  changeAccion(){
   this.accion = "crear";
}

Clonar(r: RelacionEquipoTrabajoCarteraInterface){
   let objeto = new ClonRegistro();
   for(let prop in r){
     if(this.objeto.hasOwnProperty(prop)){
       objeto[prop] = r[prop];
     }
   }
   return objeto;
}

Crear(){
   this._retcs.set( this.objeto )
   .subscribe(
     data =>{
       this.Tabla = [];
        for (let i = 0; i < data.datos.length; i++) {
           this.Tabla = [...this.Tabla, data.datos[i]];
        }
        this.display_modal = false;
        if(this.Tabla.length>0){
          this.csv = false; // Entonces habilitamos el botón
        }
        this.showSuccess('Exito', "Hemos añadido el tipo de resultado al sistema");
      },
     error =>{
       this._ls.handleError(error);
     });
}

Editar(){
  this.display_modal = false;
  this._retcs.update( this.objeto )
           .subscribe(
            data => {
              this.showSuccess("Exito", "Hemos actualizado el tipo de resultado");
              this.get();
            },
            error =>{
              this._ls.handleError(error);
            });
}

get(){
   this.showWarn( "Espere", "Estamos obteniendo los datos" );
   this._retcs.get()
           .subscribe(
             data => {
                this.Tabla = [];
                this.Tabla = data.datos;
                this.can_edit = false;
              },
             error =>{
               this._ls.handleError(error);
             });
}

showSuccess( header, mensaje ) {
   this.msjs.push({severity:'success', summary: header, detail: mensaje});
   setTimeout(()=> this.clear(), 3000);
}

showInfo( header, mensaje ) {
   this.msjs.push({severity:'info', summary: header, detail: mensaje});
   setTimeout(()=> this.clear(), 3000);
}

showWarn( header, mensaje ) {
   this.msjs.push({severity:'warn', summary: header, detail: mensaje});
   setTimeout(()=> this.clear(), 3000);
}

showError( header, mensaje ) {
   this.msjs.push({severity:'error', summary: header, detail: mensaje});
   setTimeout(()=> this.clear(), 3000);
}

clear() {
   this.msjs = [];
}

cargarCarteras(id_proveedor){
  this._cs.getCarteras(id_proveedor)
          .subscribe(
            data => {
              this.carteras = data.datos;
            },
            error =>{
              this._ls.handleError(error);
            });
}

cargarSegmentos(id_cartera){
   this._ss.getSegmentos(id_cartera)
           .subscribe(
             data => {
               this.segmentos = data.datos;
             },
            error =>{
              this._ls.handleError(error);
            });
}

cargarResponsables(id_perfil){
   this._us.getUsuariosxPerfil(id_perfil)
           .subscribe(
            data => {
              this.usuarios = data.datos;
            },
            error =>{
              this._ls.handleError(error);
            });
}

}

class ClonRegistro implements RelacionEquipoTrabajoCarteraInterface {
   constructor(
     public id?,
     public id_proveedor?,
     public id_cartera?,
     public id_perfil?,
     public id_usuario?,
     public id_equipo?,
     public id_segmento?){}
}
