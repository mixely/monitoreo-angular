import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgrAtributoComponent } from './agr-atributo.component';

describe('AgrAtributoComponent', () => {
  let component: AgrAtributoComponent;
  let fixture: ComponentFixture<AgrAtributoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgrAtributoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgrAtributoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
