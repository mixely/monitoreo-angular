import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { NgForm, FormGroup, FormControl, Validators } from "@angular/forms";

import { CarteraService } from '../../services/cartera.service';
import { ProveedorService } from '../../services/proveedor.service';
import { LoginService } from '../../services/login.service';

import { CarteraInterface } from '../../interfaces/cartera.interface';

import { Message, MenuItem, DataTable } from 'primeng/primeng';

@Component({
  selector: 'app-cartera',
  templateUrl: './cartera.component.html',
  host: {'(window:keydown)': 'hotkeys($event)'}
})
export class CarteraComponent implements OnInit {

  Formulario:FormGroup;

  display_modal:boolean = false;
  accion:string = "crear";
  msjs:Message[] = [];
  proveedores:any[] = [];
  tipo_carteras:any[] = [];

  selectedRecord:CarteraInterface;
  Tabla:CarteraInterface[] = [];
  csv:boolean = true; // Si es "true" entonces va a desahabilitar
  can_edit = false;

  constructor(private cdRef:ChangeDetectorRef,
              private _cs:CarteraService,
              private _ls:LoginService,
              private _ps:ProveedorService
              ) {
    this.Formulario = new FormGroup({
      'id': new FormControl(this.objeto.id),
      'id_proveedor': new FormControl(this.objeto.id_proveedor, Validators.required),
      'id_tipo_cartera': new FormControl(this.objeto.id_tipo_cartera, Validators.required),
      'cartera': new FormControl(this.objeto.cartera, Validators.required)
    });
  }

  // host: {'(window:keydown)': 'hotkeys($event)'}
  hotkeys(event){
    // ALT + C
    if(event.altKey && event.keyCode==107){
       this.display_modal = true;
    }
  }

  objeto:CarteraInterface = {
    id: "",
    id_proveedor: "",
    id_tipo_cartera: "",
    cartera: ""
  }

  limpiarForm(){
    this.Formulario.reset({
      id: "",
      id_proveedor: "",
      id_tipo_cartera: "",
      cartera: ""
    });

    setTimeout(()=>{
      document.getElementById("cartera").focus();
    },500);
  };

  ngOnInit() {
    this.get();
    // cargamos los proveedores
    this._ps.get()
            .subscribe(data => {
              this.proveedores = data.datos;
            },
            error =>{
              this._ls.handleError(error);
            });
    // cargamos los tipos de cartera
    this._cs.cargarTiposCartera()
            .subscribe(data => {
              this.tipo_carteras = data.datos;
            },
            error =>{
              this._ls.handleError(error);
            });

  }

  ngAfterViewChecked(){
   this.cdRef.detectChanges();
  }

  crearModal(){
    this.display_modal = true;
    this.limpiarForm();
  }

  generarCSV(dt:DataTable){
    dt.exportCSV();
  }

  editarModal(dt:DataTable){
     this.accion = "editar";
     let obj = this.Clonar(dt.selection);
     this.Formulario.setValue(obj);
     this.display_modal = true;
  }

  Edit(event){
     this.can_edit = true
     if(event.originalEvent.type=="dblclick"){
        this.accion = "editar";
        let obj = this.Clonar(event.data);
        this.Formulario.setValue(obj);
        this.display_modal = true;
     }
  }

  Selected(obj){
     this.objeto.id = obj.id;
  }
  changeAccion(){
   this.accion = "crear";
  }

Clonar(r: CarteraInterface){
   let objeto = new ClonRegistro();
   for(let prop in r){
     if(this.objeto.hasOwnProperty(prop)){
       objeto[prop] = r[prop];
     }
   }
   return objeto;
}

Crear(){
   this._cs.set( this.objeto )
   .subscribe(data =>{
     this.Tabla = [];
      for (let i = 0; i < data.datos.length; i++) {
         this.Tabla = [...this.Tabla, data.datos[i]];
      }
      this.display_modal = false;
      if(this.Tabla.length>0){
        this.csv = false; // Entonces habilitamos el botón
      }
      this.showSuccess('Exito', "Hemos añadido el tipo de resultado al sistema");
    },
     error =>{
       this._ls.handleError(error);
     });
}

Editar(){
  this.display_modal = false;
   this._cs.update( this.objeto )
           .subscribe(
              data => {
                this.showSuccess("Exito", "Hemos actualizado el tipo de resultado");
                this.get();
              },
              error =>{
                this._ls.handleError(error);
              });
}

get(){
   this.showWarn( "Espere", "Estamos obteniendo los datos" );
   this._cs.get()
           .subscribe(
             data => {
              this.Tabla = [];
              this.Tabla = data.datos;
              this.can_edit = false;
              this.clear();
             },
             error =>{
                this._ls.handleError(error);
              });
}

  showSuccess( header, mensaje ) {
     this.msjs.push({severity:'success', summary: header, detail: mensaje});
     setTimeout(()=> this.clear(), 3000);
  }

  showInfo( header, mensaje ) {
     this.msjs.push({severity:'info', summary: header, detail: mensaje});
     setTimeout(()=> this.clear(), 3000);
  }

  showWarn( header, mensaje ) {
     this.msjs.push({severity:'warn', summary: header, detail: mensaje});
     setTimeout(()=> this.clear(), 3000);
  }

  showError( header, mensaje ) {
     this.msjs.push({severity:'error', summary: header, detail: mensaje});
     setTimeout(()=> this.clear(), 3000);
  }

  clear(){
     this.msjs = [];
  }
}

class ClonRegistro implements CarteraInterface {
   constructor(
     public id?,
     public id_proveedor?,
     public id_tipo_cartera?,
     public cartera?){}
}
