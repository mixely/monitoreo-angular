import { Component, OnInit } from '@angular/core';
import { CellActivated } from '../../interfaces/cellElementActivated.interface';

@Component({
  selector: 'app-crear-campana',
  templateUrl: './crear-campana.component.html',
  host: {'(window:keydown)': 'hotkeys($event)'}
})
export class CrearCampanaComponent implements OnInit {

  // host: {'(window:keydown)': 'hotkeys($event)'}
  hotkeys(event){
    // ALT + C
    if(event.altKey && event.keyCode==67){
      //  this.display_modal = true;
    }
  }

  tablaCabecera:string[] = ["Cobertura", "Call", "Campo", "SMS", "Mailing", "General", "Whatsapp"];
  tablaConGestion:CellActivated[] = [
    {
      selected: false,
      colActivated: false,
      value: 4500
    },
    {
      selected: false,
      colActivated: false,
      value: 500
    },
    {
      selected: false,
      colActivated: false,
      value: 3000
    },
    {
      selected: false,
      colActivated: false,
      value: 200
    },
    {
      selected: false,
      colActivated: false,
      value: 8500
    },
    {
      selected: false,
      colActivated: false,
      value: 55555
    }
  ];
  tablaSinGestion:CellActivated[] = [
    {
      selected: false,
      colActivated: false,
      value: 5500
    },
    {
      selected: false,
      colActivated: false,
      value: 3500
    },
    {
      selected: false,
      colActivated: false,
      value: 7000
    },
    {
      selected: false,
      colActivated: false,
      value: 9800
    },
    {
      selected: false,
      colActivated: false,
      value: 1500
    },
    {
      selected: false,
      colActivated: false,
      value: 77777
    }
  ];
  colActiva:number = null;

  tablaTotales:string[] = ["10000", "80500", "10000", "10000", "10000", "1200000"];
  constructor() { }

  ngOnInit() {
  }

  mostrar(ele, tipo, i){

    if(tipo=="con"){
      let activa = false;
      let pos = 0;
      for (let i = 0; i < this.tablaConGestion.length; i++) {
        let obj = this.tablaConGestion[i];
        if(obj.colActivated==true){
          activa = true;
          pos = i;
        }
      }

      if(activa==false){
        ele.colActivated = true;
        ele.selected = !ele.selected;
        this.tablaSinGestion[pos].colActivated = true;
      }

    }else{
      let activa = false;
      let pos = 0;
      for (let i = 0; i < this.tablaSinGestion.length; i++) {
        let obj = this.tablaSinGestion[i];
        if(obj.colActivated==true){
          activa = true;
          pos = i;
        }
      }

      if(activa==false){
        ele.colActivated = true;
        ele.selected = !ele.selected;
        this.tablaConGestion[pos].colActivated = true;
      }
    }
    // console.log(ele, tipo);
  }

}
