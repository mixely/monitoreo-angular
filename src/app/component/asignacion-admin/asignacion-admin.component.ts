import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { NgForm, FormGroup, FormControl, Validators } from "@angular/forms";
import { Message, MenuItem, DataTable } from 'primeng/primeng';
import { GrowlModule } from 'primeng/primeng';
import { NgModule } from '@angular/core';
import { CondicionInterface } from '../../interfaces/CondicionInterface';
import { EstadodeAsignacionesGeneradasInterface } from '../../interfaces/EstadodeAsignacionesGeneradasInterface';
import { InputTextModule, DataTableModule, ButtonModule, DialogModule } from 'primeng/primeng';
import { CalendarModule } from 'primeng/primeng';

@Component({
  selector: 'app-asignacion-admin',
  templateUrl: './asignacion-admin.component.html',
  host: {'(window:keydown)': 'hotkeys($event)'}
})
export class AsignacionAdminComponent implements OnInit {

  hotkeys(event){
     // ALT + "+"
     if(event.altKey && event.keyCode==107){
        this.display_modal = true;
        this.limpiarForm();
     }
  }

  Formulario:FormGroup;
  can_edit = false;
  display_modal = false;
  accion:string = "crear";
  msjs:Message[] = [];

  objeto:EstadodeAsignacionesGeneradasInterface = {
    id: "",
    fecha: "",
    monitor: "",
    campania: "",
    proveedor: "",
    cartera: "",
    registro: "",
    evaluado: "",
    pendiente: "",
    aprobado: "",
    desaprobado: "",
    avance_proyectado: "",
    porcent_evaluado: "",
    porcent_pendiente: "",
    porcent_aprobada: "",
    porcent_desaprobada: "",
    porcent_de_avance_proyectado: "",
  }

  Tabla:any[] = [
    {
      id: "1",
      fecha: "2017-06-01",
      monitor: "",
      campania: "",
      proveedor: "",
      cartera: "",
      registro: "",
      evaluado: "",
      pendiente: "",
      aprobado: "",
      desaprobado: "",
      avance_proyectado: "",
      porcent_evaluado: "",
      porcent_pendiente: "",
      porcent_aprobada: "",
      porcent_desaprobada: "",
      porcent_de_avance_proyectado: "",
    },
    {
      id: "2",
      fecha: "2017-06-01",
      monitor: "",
      campania: "",
      proveedor: "",
      cartera: "",
      registro: "",
      evaluado: "",
      pendiente: "",
      aprobado: "",
      desaprobado: "",
      avance_proyectado: "",
      porcent_evaluado: "",
      porcent_pendiente: "",
      porcent_aprobada: "",
      porcent_desaprobada: "",
      porcent_de_avance_proyectado: "",
    },
    {
      id: "3",
      fecha: "2017-06-01",
      monitor: "",
      campania: "",
      proveedor: "",
      cartera: "",
      registro: "",
      evaluado: "",
      pendiente: "",
      aprobado: "",
      desaprobado: "",
      avance_proyectado: "",
      porcent_evaluado: "",
      porcent_pendiente: "",
      porcent_aprobada: "",
      porcent_desaprobada: "",
      porcent_de_avance_proyectado: "",
    }
  ];

  constructor(private router:Router,
              private cdRef:ChangeDetectorRef) {

    this.Formulario = new FormGroup({
        'id': new FormControl(this.objeto.id),
        'fecha': new FormControl(this.objeto.fecha),
        'monitor' : new FormControl(this.objeto.monitor, Validators.required),
        'campania' : new FormControl(this.objeto.campania, Validators.required),
        'proveedor' : new FormControl(this.objeto.proveedor, Validators.required),
        'cartera' : new FormControl(this.objeto.cartera, Validators.required),
        'registro' : new FormControl(this.objeto.registro, Validators.required),
        'evaluado' : new FormControl(this.objeto.evaluado, Validators.required),
        'pendiente' : new FormControl(this.objeto.pendiente, Validators.required),
        'aprobado' : new FormControl(this.objeto.aprobado, Validators.required),
        'desaprobado' : new FormControl(this.objeto.desaprobado, Validators.required),
        'avance_proyectado' : new FormControl(this.objeto.avance_proyectado, Validators.required),
        'porcent_evaluado' : new FormControl(this.objeto.porcent_evaluado, Validators.required),
        'porcent_pendiente' : new FormControl(this.objeto.porcent_pendiente, Validators.required),
        'porcent_aprobada' : new FormControl(this.objeto.porcent_aprobada, Validators.required),
        'porcent_desaprobada' : new FormControl(this.objeto.porcent_desaprobada, Validators.required),
        'porcent_de_avance_proyectado' : new FormControl(this.objeto.porcent_de_avance_proyectado, Validators.required)
    });
};

  Edit(event){
    console.log(event.data);
     this.can_edit = true
     if(event.originalEvent.type=="dblclick"){
        this.accion = "editar";
        let obj = this.Clonar(event.data);
        this.Formulario.setValue(obj);
        this.display_modal = true;
     }
  }

  editarModal(dt:DataTable){
     this.accion = "editar";
     let obj = this.Clonar(dt.selection);
     // this.usuario["distrito"] = [...this.usuario["distrito"], obj.distrito]
     this.Formulario.setValue(obj);
     this.display_modal = true;
  }

  Clonar(r: CondicionInterface){
       let objeto = new ClonRegistro();
       for(let prop in r){
         if(this.objeto.hasOwnProperty(prop)){
           objeto[prop] = r[prop];
         }
       }
       return objeto;
    }

  ngOnInit() {
  }

  crearModal(){
     this.display_modal = true;
     this.limpiarForm();
  }

  limpiarForm(){
    this.Formulario.reset({
      id: "",
      fecha: "",
      monitor: "",
      campania: "",
      proveedor: "",
      cartera: "",
      registro: "",
      evaluado: "",
      pendiente: "",
      aprobado: "",
      desaprobado: "",
      avance_proyectado: "",
      porcent_evaluado: "",
      porcent_pendiente: "",
      porcent_aprobada: "",
      porcent_desaprobada: "",
      porcent_de_avance_proyectado: ""
    });
   setTimeout(()=>{
      document.getElementById("id").focus();
   },500);
};

  showSuccess( header, mensaje ) {
     this.msjs = [];
     this.msjs.push({severity:'success', summary: header, detail: mensaje});
  }

  showInfo( header, mensaje ) {
     this.msjs = [];
     this.msjs.push({severity:'info', summary: header, detail: mensaje});
  }

  showWarn( header, mensaje ) {
     this.msjs = [];
     this.msjs.push({severity:'warn', summary: header, detail: mensaje});
  }

  showError( header, mensaje ) {
     this.msjs = [];
     this.msjs.push({severity:'error', summary: header, detail: mensaje});
  }

  clear() {
     this.msjs = [];
  }

  changeAccion(){

  }

}

class ClonRegistro implements EstadodeAsignacionesGeneradasInterface {
  constructor(
              public id?,
              public fecha?,
              public monitor?,
              public campania?,
              public proveedor?,
              public cartera?,
              public registro?,
              public evaluado?,
              public pendiente?,
              public aprobado?,
              public desaprobado?,
              public avance_proyectado?,
              public porcent_evaluado?,
              public porcent_pendiente?,
              public porcent_aprobada?,
              public porcent_desaprobada?,
              public porcent_de_avance_proyectado?){}
}
