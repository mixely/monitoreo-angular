import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AsignacionAdminComponent } from './asignacion-admin.component';

describe('AsignacionAdminComponent', () => {
  let component: AsignacionAdminComponent;
  let fixture: ComponentFixture<AsignacionAdminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AsignacionAdminComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AsignacionAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
