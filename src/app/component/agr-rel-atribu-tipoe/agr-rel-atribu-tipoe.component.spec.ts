import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgrRelAtribuTipoeComponent } from './agr-rel-atribu-tipoe.component';

describe('AgrRelAtribuTipoeComponent', () => {
  let component: AgrRelAtribuTipoeComponent;
  let fixture: ComponentFixture<AgrRelAtribuTipoeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgrRelAtribuTipoeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgrRelAtribuTipoeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
