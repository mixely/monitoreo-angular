import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-consulta-clientes',
  templateUrl: './consulta-clientes.component.html',
  host: {'(window:keydown)': 'hotkeys($event)'}
})
export class ConsultaClientesComponent implements OnInit {
  // host: {'(window:keydown)': 'hotkeys($event)'}
  hotkeys(event){
    // ALT + C
    if(event.altKey && event.keyCode==67){
      //  this.display_modal = true;
    }
  }
  vistaSeleccionada:any[] = [];
  cliente:any[] = [];
  conyugue:any[] = [];
  datos_telefono:any[] = [];
  datos_direccion:any[] = [];
  datos_campana:any[] = [];
  datos_venta:any[] = [];
  vista:any[] = [];
  constructor() { }

  ngOnInit() {
  }

}
