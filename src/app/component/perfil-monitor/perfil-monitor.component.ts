import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { NgForm, FormGroup, FormControl, Validators } from "@angular/forms";
import { Message, MenuItem, DataTable } from 'primeng/primeng';
import { GrowlModule } from 'primeng/primeng';
import { NgModule } from '@angular/core';
import { CondicionInterface } from '../../interfaces/CondicionInterface';
import { PerfildeMonitorInterface } from '../../interfaces/PerfildeMonitorInterface';
import { InputTextModule, DataTableModule, ButtonModule, DialogModule } from 'primeng/primeng';
import { CalendarModule } from 'primeng/primeng';

@Component({
  selector: 'app-perfil-monitor',
  templateUrl: './perfil-monitor.component.html',
  host: {'(window:keydown)': 'hotkeys($event)'}
})

export class PerfilMonitorComponent implements OnInit {

  hotkeys(event){
     // ALT + "+"
     if(event.altKey && event.keyCode==107){
        this.display_modal = true;
        this.limpiarForm();
     }
  }

  Formulario:FormGroup;
  can_edit = false;
  display_modal = false;
  accion:string = "crear";
  msjs:Message[] = [];

  objeto:PerfildeMonitorInterface = {
      id: "",
      fecha: "",
      nombre: "",
      asignacion_mensual: "",
      produccion_l_v: "",
      produccion_sabado: "",
      descripcion: "",
      estado: "",
  }

  Tabla:any[] = [
    {
      id: "1",
      fecha: "2017-06-01",
      nombre: "A",
      asignacion_mensual: "250",
      produccion_l_v: "50",
      produccion_sabado: "40",
      descripcion: "",
      estado: "Activo",
    },
    {
      id: "2",
      fecha: "2017-06-01",
      nombre: "B",
      asignacion_mensual: "250",
      produccion_l_v: "50",
      produccion_sabado: "40",
      descripcion: "",
      estado: "Activo",
    },
    {
      id: "3",
      fecha: "2017-06-01",
      nombre: "C",
      asignacion_mensual: "250",
      produccion_l_v: "60",
      produccion_sabado: "40",
      descripcion: "",
      estado: "Activo",
    }
  ];

  constructor(private router:Router,
              private cdRef:ChangeDetectorRef) {

    this.Formulario = new FormGroup({
      'id': new FormControl(this.objeto.id),
      'fecha': new FormControl(this.objeto.fecha),
      'nombre': new FormControl(this.objeto.nombre, Validators.required),
      'asignacion_mensual': new FormControl(this.objeto.asignacion_mensual, Validators.required),
      'produccion_l_v': new FormControl(this.objeto.produccion_l_v, Validators.required),
      'produccion_sabado': new FormControl(this.objeto.produccion_sabado, Validators.required),
      'descripcion': new FormControl(this.objeto.descripcion),
      'estado': new FormControl(this.objeto.estado, Validators.required)
    });
};

  Edit(event){
    console.log(event.data);
     this.can_edit = true
     if(event.originalEvent.type=="dblclick"){
        this.accion = "editar";
        let obj = this.Clonar(event.data);
        this.Formulario.setValue(obj);
        this.display_modal = true;
     }
  }

  editarModal(dt:DataTable){
     this.accion = "editar";
     let obj = this.Clonar(dt.selection);
     // this.usuario["distrito"] = [...this.usuario["distrito"], obj.distrito]
     this.Formulario.setValue(obj);
     this.display_modal = true;
  }

  Clonar(r: CondicionInterface){
       let objeto = new ClonRegistro();
       for(let prop in r){
         if(this.objeto.hasOwnProperty(prop)){
           objeto[prop] = r[prop];
         }
       }
       return objeto;
    }

  ngOnInit() {
  }

  crearModal(){
     this.display_modal = true;
     this.limpiarForm();
  }

  limpiarForm(){
    this.Formulario.reset({
      id: "",
      fecha: "",
      nombre: "",
      asignacion_mensual: "",
      produccion_l_v: "",
      produccion_sabado: "",
      descripcion: "",
      estado: "",
    });
   setTimeout(()=>{
      document.getElementById("id").focus();
   },500);
};

  showSuccess( header, mensaje ) {
     this.msjs = [];
     this.msjs.push({severity:'success', summary: header, detail: mensaje});
  }

  showInfo( header, mensaje ) {
     this.msjs = [];
     this.msjs.push({severity:'info', summary: header, detail: mensaje});
  }

  showWarn( header, mensaje ) {
     this.msjs = [];
     this.msjs.push({severity:'warn', summary: header, detail: mensaje});
  }

  showError( header, mensaje ) {
     this.msjs = [];
     this.msjs.push({severity:'error', summary: header, detail: mensaje});
  }

  clear() {
     this.msjs = [];
  }

  changeAccion(){

  }

}

class ClonRegistro implements PerfildeMonitorInterface {
  constructor(
              public id?,
              public fecha?,
              public nombre?,
              public asignacion_mensual?,
              public produccion_l_v?,
              public produccion_sabado?,
              public descripcion?,
              public estado?){}
}
