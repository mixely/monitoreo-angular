import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PerfilMonitorComponent } from './perfil-monitor.component';

describe('PerfilMonitorComponent', () => {
  let component: PerfilMonitorComponent;
  let fixture: ComponentFixture<PerfilMonitorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PerfilMonitorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PerfilMonitorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
