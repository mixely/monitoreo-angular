import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { NgForm, FormGroup, FormControl, Validators } from "@angular/forms";

import { RelacionTipoContactoResultadoService } from '../../services/relacion-tipo-contacto-resultado.service';
import { ResultadoService } from '../../services/resultado.service';
import { TipoContactoService } from '../../services/tipo-contacto.service';
import { LoginService } from '../../services/login.service';

import { RelacionTipoContactoResultadoInterface } from '../../interfaces/relacion-tipo-contacto-resultado.interface';

import { Message, MenuItem, DataTable } from 'primeng/primeng';

@Component({
  selector: 'app-relacion-tipo-contacto-resultado',
  templateUrl: './relacion-tipo-contacto-resultado.component.html',
  host: {'(window:keydown)': 'hotkeys($event)'}
})
export class RelacionTipoContactoResultadoComponent implements OnInit {

  Formulario:FormGroup;

  display_modal:boolean = false;
  accion:string = "crear";
  msjs:Message[] = [];
  proveedores:any[] = [];
  tipo_carteras:any[] = [];

  resultados:any[] = [];
  tipos_contactos:any[] = [];

  selectedRecord:RelacionTipoContactoResultadoInterface;
  Tabla:RelacionTipoContactoResultadoInterface[] = [];
  csv:boolean = true; // Si es "true" entonces va a desahabilitar

  can_edit = false;

  constructor(private cdRef:ChangeDetectorRef,
              private _rs:ResultadoService,
              private _ls:LoginService,
              private _tcs:TipoContactoService,
              private _rtcrs:RelacionTipoContactoResultadoService
              ) {
    this.Formulario = new FormGroup({
      'id': new FormControl(this.objeto.id),
      'id_resultado': new FormControl(this.objeto.id_resultado, Validators.required),
      'id_tipo_contacto': new FormControl(this.objeto.id_tipo_contacto, Validators.required),
      'estado': new FormControl(this.objeto.estado)
    });
  }

  // host: {'(window:keydown)': 'hotkeys($event)'}
  hotkeys(event){
    // ALT + C
    if(event.altKey && event.keyCode==107){
       this.display_modal = true;
    }
  }

  objeto:RelacionTipoContactoResultadoInterface = {
    id: "",
    id_resultado: "",
    id_tipo_contacto: "",
    estado: ""
  }

  limpiarForm(){
    this.Formulario.reset({
      id: "",
      id_resultado: "",
      id_tipo_contacto: "",
      estado: ""
    });

    setTimeout(()=>{
      document.getElementById("id_resultado").focus();
    },500);
  };

  ngOnInit() {
   this.get();
   this._rs.get()
           .subscribe(
            data =>{
              this.resultados = data.datos;
            },
            error =>{
              this._ls.handleError(error);
            });
   this._tcs.get()
            .subscribe(
            data =>{
              this.tipos_contactos = data.datos;
            },
            error =>{
              this._ls.handleError(error);
            });
  }

  ngAfterViewChecked(){
   this.cdRef.detectChanges();
  }

  crearModal(){
    this.display_modal = true;
    this.limpiarForm();
  }

  generarCSV(dt:DataTable){
    dt.exportCSV();
  }

  editarModal(dt:DataTable){
     this.accion = "editar";
     let obj = this.Clonar(dt.selection);
     this.Formulario.setValue(obj);
     this.display_modal = true;
  }

  Edit(event){
     this.can_edit = true
     if(event.originalEvent.type=="dblclick"){
        this.accion = "editar";
        let obj = this.Clonar(event.data);
        this.Formulario.setValue(obj);
        this.display_modal = true;
     }
  }

  Selected(obj){
     this.objeto.id = obj.id;
  }
  changeAccion(){
   this.accion = "crear";
  }

Clonar(r: RelacionTipoContactoResultadoInterface){
   let objeto = new ClonRegistro();
   for(let prop in r){
     if(this.objeto.hasOwnProperty(prop)){
       objeto[prop] = r[prop];
     }
   }
   return objeto;
}

Crear(){
   this._rtcrs.set( this.objeto )
   .subscribe(
     data =>{
        this.Tabla = [];
        for (let i = 0; i < data.datos.length; i++) {
          this.Tabla = [...this.Tabla, data.datos[i]];
        }
        this.display_modal = false;
        if(this.Tabla.length>0){
          this.csv = false; // Entonces habilitamos el botón
        }
        this.showSuccess('Exito', "Hemos añadido el tipo de resultado al sistema");
      },
      error =>{
        this._ls.handleError(error);
      });
}

Editar(){
  this.display_modal = false;
   this._rtcrs.update( this.objeto )
           .subscribe(
            data => {
              this.showSuccess("Exito", "Hemos actualizado el tipo de resultado");
              this.get();
            },
            error =>{
              this._ls.handleError(error);
            });
}

get(){
   this.showWarn( "Espere", "Estamos obteniendo los datos" );
   this._rtcrs.get()
           .subscribe(
            data => {
              this.Tabla = [];
              this.Tabla = data.datos;
              this.can_edit = false;
              this.clear();
            },
            error =>{
              this._ls.handleError(error);
            });
}

showSuccess( header, mensaje ) {
   this.msjs.push({severity:'success', summary: header, detail: mensaje});
   setTimeout(()=> this.clear(), 3000);
}

showInfo( header, mensaje ) {
   this.msjs.push({severity:'info', summary: header, detail: mensaje});
   setTimeout(()=> this.clear(), 3000);
}

showWarn( header, mensaje ) {
   this.msjs.push({severity:'warn', summary: header, detail: mensaje});
   setTimeout(()=> this.clear(), 3000);
}

showError( header, mensaje ) {
   this.msjs.push({severity:'error', summary: header, detail: mensaje});
   setTimeout(()=> this.clear(), 3000);
}

clear(){
   this.msjs = [];
}
}

class ClonRegistro implements RelacionTipoContactoResultadoInterface {
   constructor(
     public id?,
     public id_resultado?,
     public id_tipo_contacto?,
     public estado?){}
}
