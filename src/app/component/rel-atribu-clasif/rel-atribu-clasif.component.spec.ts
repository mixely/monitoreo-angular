import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RelAtribuClasifComponent } from './rel-atribu-clasif.component';

describe('RelAtribuClasifComponent', () => {
  let component: RelAtribuClasifComponent;
  let fixture: ComponentFixture<RelAtribuClasifComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RelAtribuClasifComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RelAtribuClasifComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
