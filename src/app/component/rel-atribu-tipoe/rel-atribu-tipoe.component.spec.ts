import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RelAtribuTipoeComponent } from './rel-atribu-tipoe.component';

describe('RelAtribuTipoeComponent', () => {
  let component: RelAtribuTipoeComponent;
  let fixture: ComponentFixture<RelAtribuTipoeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RelAtribuTipoeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RelAtribuTipoeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
