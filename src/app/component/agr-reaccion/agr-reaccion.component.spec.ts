import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgrReaccionComponent } from './agr-reaccion.component';

describe('AgrReaccionComponent', () => {
  let component: AgrReaccionComponent;
  let fixture: ComponentFixture<AgrReaccionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgrReaccionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgrReaccionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
