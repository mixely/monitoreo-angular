import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RelReaccCritEvalComponent } from './rel-reacc-crit-eval.component';

describe('RelReaccCritEvalComponent', () => {
  let component: RelReaccCritEvalComponent;
  let fixture: ComponentFixture<RelReaccCritEvalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RelReaccCritEvalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RelReaccCritEvalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
