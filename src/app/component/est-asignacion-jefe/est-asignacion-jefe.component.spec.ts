import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EstAsignacionJefeComponent } from './est-asignacion-jefe.component';

describe('EstAsignacionJefeComponent', () => {
  let component: EstAsignacionJefeComponent;
  let fixture: ComponentFixture<EstAsignacionJefeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EstAsignacionJefeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EstAsignacionJefeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
