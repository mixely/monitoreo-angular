import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { NgForm, FormGroup, FormControl, Validators } from "@angular/forms";
import { Message, MenuItem, DataTable } from 'primeng/primeng';
import { GrowlModule } from 'primeng/primeng';
import { NgModule } from '@angular/core';
import { TiempoInterface } from '../../interfaces/TiempoInterface';
import { InputTextModule, DataTableModule, ButtonModule, DialogModule} from 'primeng/primeng';
import { CalendarModule } from 'primeng/primeng';

@Component({
  selector: 'app-tiempo',
  templateUrl: './tiempo.component.html',
  host: {'(window:keydown)': 'hotkeys($event)'}
})
export class TiempoComponent implements OnInit {

  hotkeys(event){
     // ALT + "+"
     if(event.altKey && event.keyCode==107){
        this.display_modal = true;
        this.limpiarForm();
     }
  }

  Formulario:FormGroup;
  can_edit = false;
  display_modal = false;
  accion:string = "crear";
  msjs:Message[] = [];

  objeto:TiempoInterface = {
    id: "",
    fecha: "",
    analizar_historial: "",
    presentar_informe: "",
    estado: ""

  }

  Tabla:any[] = [
    {
      id: "1",
      fecha: "2017-07-01",
      analizar_historial: "1",
      presentar_informe: "Ripley",
      estado: "Activo"
    },
    {
      id: "2",
      fecha: "2017-07-17",
      analizar_historial: "1",
      presentar_informe: "Ripley",
      estado: "Activo"
    },
    {
      id: "3",
      fecha: "2017-06-01",
      analizar_historial: "1",
      presentar_informe: "Ripley",
      estado: "Activo"
    }
  ];

  constructor(private router:Router,
              private cdRef:ChangeDetectorRef) {

    this.Formulario = new FormGroup({
      'id': new FormControl(this.objeto.id),
      'fecha': new FormControl(this.objeto.fecha),
      'analizar_historial': new FormControl(this.objeto.analizar_historial, Validators.required),
      'presentar_informe': new FormControl(this.objeto.presentar_informe, Validators.required),
      'estado': new FormControl(this.objeto.estado, Validators.required)
    });
  };


    Edit(event){
      console.log(event.data);
       this.can_edit = true
       if(event.originalEvent.type=="dblclick"){
          this.accion = "editar";
          let obj = this.Clonar(event.data);
          this.Formulario.setValue(obj);
          this.display_modal = true;
       }
    }

    editarModal(dt:DataTable){
       this.accion = "editar";
       let obj = this.Clonar(dt.selection);
       // this.usuario["distrito"] = [...this.usuario["distrito"], obj.distrito]
       this.Formulario.setValue(obj);
       this.display_modal = true;
    }

    Clonar(r: TiempoInterface){
         let objeto = new ClonRegistro();
         for(let prop in r){
           if(this.objeto.hasOwnProperty(prop)){
             objeto[prop] = r[prop];
           }
         }
         return objeto;
      }

    ngOnInit() {
    }

    crearModal(){
       this.display_modal = true;
       this.limpiarForm();
    }

    limpiarForm(){
      this.Formulario.reset({
        id: "",
        fecha: "",
        nombre: "",
        descripcion: "",
        estado: ""
      });
     setTimeout(()=>{
        document.getElementById("id").focus();
     },500);
  };

    showSuccess( header, mensaje ) {
       this.msjs = [];
       this.msjs.push({severity:'success', summary: header, detail: mensaje});
    }

    showInfo( header, mensaje ) {
       this.msjs = [];
       this.msjs.push({severity:'info', summary: header, detail: mensaje});
    }

    showWarn( header, mensaje ) {
       this.msjs = [];
       this.msjs.push({severity:'warn', summary: header, detail: mensaje});
    }

    showError( header, mensaje ) {
       this.msjs = [];
       this.msjs.push({severity:'error', summary: header, detail: mensaje});
    }

    clear() {
       this.msjs = [];
    }

    changeAccion(){

    }
  }

  class ClonRegistro implements TiempoInterface {
    constructor(
                public id?,
                public fecha?,
                public analizar_historial?,
                public presentar_informe?,
                public estado?){}
  }
