import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AsignacionJefeComponent } from './asignacion-jefe.component';

describe('AsignacionJefeComponent', () => {
  let component: AsignacionJefeComponent;
  let fixture: ComponentFixture<AsignacionJefeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AsignacionJefeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AsignacionJefeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
