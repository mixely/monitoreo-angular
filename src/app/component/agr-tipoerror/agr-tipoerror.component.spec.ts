import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgrTipoerrorComponent } from './agr-tipoerror.component';

describe('AgrTipoerrorComponent', () => {
  let component: AgrTipoerrorComponent;
  let fixture: ComponentFixture<AgrTipoerrorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgrTipoerrorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgrTipoerrorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
