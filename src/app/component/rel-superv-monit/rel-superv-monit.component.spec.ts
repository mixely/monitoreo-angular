import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RelSupervMonitComponent } from './rel-superv-monit.component';

describe('RelSupervMonitComponent', () => {
  let component: RelSupervMonitComponent;
  let fixture: ComponentFixture<RelSupervMonitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RelSupervMonitComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RelSupervMonitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
