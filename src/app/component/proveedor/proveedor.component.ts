import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { NgForm, FormGroup, FormControl, Validators } from "@angular/forms";

import { ProveedorService } from '../../services/proveedor.service';
import { RubroService } from '../../services/rubro.service';
import { ErroresService } from '../../services/errores.service';
import { LoginService } from '../../services/login.service';

import { ProveedorInterface } from '../../interfaces/proveedor.interface';

import { Message, MenuItem, DataTable } from 'primeng/primeng';

@Component({
  selector: 'app-proveedor',
  templateUrl: './proveedor.component.html',
  host: {'(window:keydown)': 'hotkeys($event)'}
})
export class ProveedorComponent implements OnInit {

   accion:string = "crear";
   msjs:Message[] = [];
   rubros:any[] = [];
   selectedRecord:ProveedorInterface;
   Tabla:ProveedorInterface[] = [];
   csv:boolean = true; // Si es "true" entonces va a desahabilitar
   display_modal = false;
   can_edit = false;

   Formulario:FormGroup;

  constructor(private router:Router,
              private _ps:ProveedorService,
              private _es:ErroresService,
              private _rs:RubroService,
              private _ls:LoginService,
              private cdRef:ChangeDetectorRef) {

    this.Formulario = new FormGroup({
      'id': new FormControl(this.objeto.id),
      'razon_social': new FormControl(this.objeto.razon_social, Validators.required),
      'ruc': new FormControl(this.objeto.ruc, [Validators.required, Validators.minLength(11), Validators.maxLength(11)] ),
      'id_rubro': new FormControl(this.objeto.id_rubro, Validators.required),
      'telefono1': new FormControl(this.objeto.telefono1, Validators.required),
      'telefono2': new FormControl(this.objeto.telefono2),
      'telefono3': new FormControl(this.objeto.telefono3),
      'representante': new FormControl(this.objeto.representante, Validators.required),
      'contacto': new FormControl(this.objeto.contacto, Validators.required),
      'telf_contacto': new FormControl(this.objeto.telf_contacto, Validators.required)
    })
};

  objeto:ProveedorInterface = {
    id:"",
    razon_social:"",
    ruc:"",
    id_rubro:0,
    telefono1:"",
    telefono2:"",
    telefono3:"",
    representante:"",
    contacto:"",
    telf_contacto:""
};

  limpiarForm(){
    this.Formulario.reset({
      id:"",
      razon_social:"",
      ruc:"",
      id_rubro:0,
      telefono1:"",
      telefono2:"",
      telefono3:"",
      representante:"",
      contacto:"",
      telf_contacto:""
    });
   setTimeout(()=>{
      document.getElementById("razon_social").focus();
   },500);
};

ngOnInit() {
   this.get();
   this._rs.get()
           .subscribe(data=>{
             this.rubros = data.rubros;
           })
}

hotkeys(event){
   // ALT + "+"
   if(event.altKey && event.keyCode==107){
      this.display_modal = true;
      this.limpiarForm();
   }
}

// Este método nos permite evitar el error que lanza angular cuando detecta algún cambio en algún elemento.
// Es un error feo, es necesario ponerlo siempre.
ngAfterViewChecked(){
   this.cdRef.detectChanges();
}

// verRegistro(reg:ProveedorInterface){
//    this.msjs = [];
//    this.msjs.push({severity: 'info', summary: 'Proveedor seleccionado', detail: reg.nombres + ' - ' + reg.ap_paterno});
// }

crearModal(){
  this.display_modal = true;
  // this.can_edit = false;
  this.limpiarForm();
}

generarCSV(dt:DataTable){
   dt.exportCSV();
}

editarModal(dt:DataTable){
   this.accion = "editar";
   let obj = this.Clonar(dt.selection);
   this.Formulario.setValue(obj);
   this.display_modal = true;
}

Edit(event){
   this.can_edit = true
  //  console.log(event.data);
   if(event.originalEvent.type=="dblclick"){
      this.accion = "editar";
      let obj = this.Clonar(event.data);
      this.Formulario.setValue(obj);
      this.display_modal = true;
   }
}

Selected(obj){
  //  this.objeto.ruc = obj.ruc;
}

changeAccion(){
   this.accion = "crear";
}

Clonar(r: ProveedorInterface){
   let objeto = new ClonRegistro();
   for(let prop in r){
     if(this.objeto.hasOwnProperty(prop)){
       objeto[prop] = r[prop];
     }
   }
   return objeto;
}

Crear(){
   this._ps.set( this.objeto )
   .subscribe(
     data =>{
        this.Tabla = [];
        for (let i = 0; i < data.datos.length; i++) {
           this.Tabla = [...this.Tabla, data.datos[i]];
        }
        this.display_modal = false;
        if(this.Tabla.length>0){
          this.csv = false; // Entonces habilitamos el botón
        }
        this.showSuccess('Exito', "Hemos añadido el proveedor al sistema");
     },
     error =>{
       this._ls.handleError(error);
     });
}

Editar(){
  this._ps.update( this.objeto )
  .subscribe(
    data =>{
      this.Tabla = [];
       for (let i = 0; i < data.datos.length; i++) {
          this.Tabla = [...this.Tabla, data.datos[i]];
       }
       this.display_modal = false;
       this.showSuccess('Exito', "Hemos añadido el proveedor al sistema")
    },
    error =>{
      this._ls.handleError(error);
    });
}

get(){
   this.showWarn( "Espere", "Estamos obteniendo los datos" );
   this._ps.get()
   .subscribe(
     data=>{
       if(data.status){
         this.Tabla = [];
         this.Tabla = data.datos;
         this.clear();
       }else{
         let msj = this._es.getErrores(data.code);
         this.showError("Error", msj);
       }
     },
     error =>{
       this._ls.handleError(error);
     });
}


  showSuccess( header, mensaje ) {
     this.msjs = [];
     this.msjs.push({severity:'success', summary: header, detail: mensaje});
  }

  showInfo( header, mensaje ) {
     this.msjs = [];
     this.msjs.push({severity:'info', summary: header, detail: mensaje});
  }

  showWarn( header, mensaje ) {
     this.msjs = [];
     this.msjs.push({severity:'warn', summary: header, detail: mensaje});
  }

  showError( header, mensaje ) {
     this.msjs = [];
     this.msjs.push({severity:'error', summary: header, detail: mensaje});
  }

  clear() {
     this.msjs = [];
  }

}

class ClonRegistro implements ProveedorInterface {
   constructor(
     public razon_social?, public ruc?, public id_rubro?, public telefono1?, public telefono2?,
     public telefono3?, public representante?, public contacto?, public telf_contacto?
   ){}
}
