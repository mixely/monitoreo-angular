import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgrPerfilMonitorComponent } from './agr-perfil-monitor.component';

describe('AgrPerfilMonitorComponent', () => {
  let component: AgrPerfilMonitorComponent;
  let fixture: ComponentFixture<AgrPerfilMonitorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgrPerfilMonitorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgrPerfilMonitorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
