import { Component, OnInit } from '@angular/core';
import { Settings } from '../../config';
import { NgForm, FormGroup, FormControl, Validators } from "@angular/forms";

import { LoginService } from '../../services/login.service';

import { CargaValidacionService } from '../../services/carga-validacion.service';
import { CargaValidacionesInterface } from '../../interfaces/carga-validaciones.interface';


@Component({
  selector: 'app-carga-validacion',
  templateUrl: './carga-validacion.component.html',
  host: {'(window:keydown)': 'hotkeys($event)'}
})
export class CargaValidacionComponent implements OnInit {
  // host: {'(window:keydown)': 'hotkeys($event)'}
  hotkeys(event){
    // ALT + C
    if(event.altKey && event.keyCode==67){
       this.display_modal = true;
    }
  }

  Formulario:FormGroup;

  display_modal:boolean = false;

  constructor(
    private _cvs:CargaValidacionService,
    private _ls:LoginService){
      this.Formulario = new FormGroup({
        "archivo" : new FormControl(this.objeto.archivo, Validators.required),
        "nombre_asignacion" : new FormControl(this.objeto.nombre_asignacion, Validators.required)
      });
    }

  objeto:CargaValidacionesInterface = {
    archivo: "",
    nombre_asignacion: ""
  }

  Evento:any;

  Enviar(){
    this.activo = true;
    let archivo:FileList = this.Evento.target.files;
    this._cvs.upload(archivo, this.objeto.nombre_asignacion)
            .subscribe(
              data =>{
                this.display_modal = false;
              },
              error =>{
                this._ls.handleError(error);
              });

  }

  fileChange(event) {
     this.Evento = event;
   }

  limpiarForm(){
    this.Formulario.reset({
      archivo: "",
      nombre_asignacion: ""
    });
  };

  ngOnInit(){
  }

  mostrarDialog(){
    this.limpiarForm();
    this.activo = false;
    this.display_modal = true;
  }

  cerrarDialog(){
    this.display_modal = true;
  }

  uploadedFiles:any[] = [];
  activo:boolean = false;

}
