import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgrCondicionComponent } from './agr-condicion.component';

describe('AgrCondicionComponent', () => {
  let component: AgrCondicionComponent;
  let fixture: ComponentFixture<AgrCondicionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgrCondicionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgrCondicionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
