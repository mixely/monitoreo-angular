import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { NgForm, FormGroup, FormControl, Validators } from "@angular/forms";

import { RelacionProductoProveedorService } from '../../services/relacion-producto-proveedor.service';
import { ProductoService } from '../../services/producto.service';
import { ProveedorService } from '../../services/proveedor.service';
import { LoginService } from '../../services/login.service';

import { RelacionProductoProveedorInterface } from '../../interfaces/relacion-producto-proveedor.interface';

import { Message, MenuItem, DataTable } from 'primeng/primeng';

@Component({
  selector: 'app-relacion-producto-proveedor',
  templateUrl: './relacion-producto-proveedor.component.html',
  host: {'(window:keydown)': 'hotkeys($event)'}
})
export class RelacionProductoProveedorComponent implements OnInit {

  Formulario:FormGroup;

  display_modal:boolean = false;
  accion:string = "crear";
  msjs:Message[] = [];

  productos:any[] = [];
  proveedores:any[] = [];

  selectedRecord:RelacionProductoProveedorInterface;
  Tabla:RelacionProductoProveedorInterface[] = [];
  csv:boolean = true; // Si es "true" entonces va a desahabilitar

  can_edit = false;

  constructor(private cdRef:ChangeDetectorRef,
              private _prods:ProductoService,
              private _ls:LoginService,
              private _provs:ProveedorService,
              private _rpps:RelacionProductoProveedorService
              ) {
    this.Formulario = new FormGroup({
      'id': new FormControl(this.objeto.id),
      'id_proveedor': new FormControl(this.objeto.id_proveedor, Validators.required),
      'id_producto': new FormControl(this.objeto.id_producto, Validators.required),
      'estado': new FormControl(this.objeto.estado)
    });
  }

  // host: {'(window:keydown)': 'hotkeys($event)'}
  hotkeys(event){
    // ALT + C
    if(event.altKey && event.keyCode==107){
       this.display_modal = true;
    }
  }

  objeto:RelacionProductoProveedorInterface = {
    id: "",
    id_proveedor: "",
    id_producto: "",
    estado: ""
  }

  limpiarForm(){
    this.Formulario.reset({
      id: "",
      id_proveedor: "",
      id_producto: "",
      estado: ""
    });

    setTimeout(()=>{
      document.getElementById("id_producto").focus();
    },500);
  };

  ngOnInit() {
    this.get();
    this._prods.get()
              .subscribe(
                data=>{
                  this.productos = data.datos;
                },
                error =>{
                  this._ls.handleError(error);
                });

    this._provs.get()
               .subscribe(
                  data=>{
                    this.proveedores = data.datos;
                  },
                  error =>{
                    this._ls.handleError(error);
                  });

  }

  ngAfterViewChecked(){
   this.cdRef.detectChanges();
  }

  crearModal(){
    this.display_modal = true;
    this.limpiarForm();
  }

  generarCSV(dt:DataTable){
    dt.exportCSV();
  }

  editarModal(dt:DataTable){
     this.accion = "editar";
     let obj = this.Clonar(dt.selection);
     this.Formulario.setValue(obj);
     this.display_modal = true;
  }

  Edit(event){
     this.can_edit = true
     if(event.originalEvent.type=="dblclick"){
        this.accion = "editar";
        let obj = this.Clonar(event.data);
        this.Formulario.setValue(obj);
        this.display_modal = true;
     }
  }

  Selected(obj){
     this.objeto.id = obj.id;
  }
  changeAccion(){
   this.accion = "crear";
  }

Clonar(r: RelacionProductoProveedorInterface){
   let objeto = new ClonRegistro();
   for(let prop in r){
     if(this.objeto.hasOwnProperty(prop)){
       objeto[prop] = r[prop];
     }
   }
   return objeto;
}

Crear(){
   this._rpps.set( this.objeto )
   .subscribe(
      data =>{
        this.Tabla = [];
        for (let i = 0; i < data.datos.length; i++) {
          this.Tabla = [...this.Tabla, data.datos[i]];
        }
        this.display_modal = false;
        if(this.Tabla.length>0){
          this.csv = false; // Entonces habilitamos el botón
        }
        this.showSuccess('Exito', "Hemos añadido el tipo de resultado al sistema");
      },
      error =>{
        this._ls.handleError(error);
      });
}

Editar(){
  this.display_modal = false;
   this._rpps.update( this.objeto )
           .subscribe(
            data => {
              this.showSuccess("Exito", "Hemos actualizado el tipo de resultado");
              this.get();
            },
            error =>{
              this._ls.handleError(error);
            });
}

get(){
   this.showWarn( "Espere", "Estamos obteniendo los datos" );
   this._rpps.get()
            .subscribe(
              data => {
                this.Tabla = [];
                this.Tabla = data.datos;
                this.can_edit = false;
                this.clear();
              },
              error =>{
                this._ls.handleError(error);
              });
}

showSuccess( header, mensaje ) {
   this.msjs.push({severity:'success', summary: header, detail: mensaje});
   setTimeout(()=> this.clear(), 3000);
}

showInfo( header, mensaje ) {
   this.msjs.push({severity:'info', summary: header, detail: mensaje});
   setTimeout(()=> this.clear(), 3000);
}

showWarn( header, mensaje ) {
   this.msjs.push({severity:'warn', summary: header, detail: mensaje});
   setTimeout(()=> this.clear(), 3000);
}

showError( header, mensaje ) {
   this.msjs.push({severity:'error', summary: header, detail: mensaje});
   setTimeout(()=> this.clear(), 3000);
}

clear(){
   this.msjs = [];
}
}

class ClonRegistro implements RelacionProductoProveedorInterface {
   constructor(
     public id?,
     public id_proveedor?,
     public id_producto?,
     public estado?){}
}
