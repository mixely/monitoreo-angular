import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { NgForm, FormGroup, FormControl, Validators } from "@angular/forms";
import { Message, MenuItem, DataTable } from 'primeng/primeng';
import { GrowlModule } from 'primeng/primeng';
import { NgModule } from '@angular/core';
import { DetalleDeAsignacionInterface } from '../../interfaces/DetalleDeAsignacionInterface';
import { InputTextModule, DataTableModule, ButtonModule, DialogModule} from 'primeng/primeng';
import { CalendarModule } from 'primeng/primeng';

@Component({
  selector: 'app-est-asignacion-admin',
  templateUrl: './est-asignacion-admin.component.html',
  host: {'(window:keydown)': 'hotkeys($event)'}
})
export class EstAsignacionAdminComponent implements OnInit {

  hotkeys(event){
     // ALT + "+"
     if(event.altKey && event.keyCode==107){
        this.display_modal = true;
        this.limpiarForm();
     }
  }

  Formulario:FormGroup;
  can_edit = false;
  display_modal = false;
  accion:string = "crear";
  msjs:Message[] = [];

  objeto:DetalleDeAsignacionInterface = {
      id: "",
      periodo: "",
      fecha_de_asignacion: "",
      monitor: "",
      tipo_de_cartera: "",
      proveedor: "",
      cartera: "",
      agente: "",
      nro_de_evaluacion: "",
      nro_de_audio: "",
      reaccion: "",
      dni: "",
      telefono: "",
      fecha_de_llamada: "",
      fecha_de_evaluacion: "",
      audio: "",
      resultado: "",
      porcent_alcance: ""
  }

  Tabla:any[] = [
    {
      id: "1",
      periodo: "",
      fecha_de_asignacion: "",
      monitor: "",
      tipo_de_cartera: "",
      proveedor: "",
      cartera: "",
      agente: "",
      nro_de_evaluacion: "",
      nro_de_audio: "",
      reaccion: "",
      dni: "48484745",
      telefono: "949857848",
      fecha_de_llamada: "2017-07-20",
      fecha_de_evaluacion: "2017-07-20",
      audio: "",
      resultado: "",
      porcent_alcance: ""
    },
    {
      id: "2",
      periodo: "",
      fecha_de_asignacion: "2017-07-01",
      monitor: "",
      tipo_de_cartera: "",
      proveedor: "",
      cartera: "",
      agente: "",
      nro_de_evaluacion: "",
      nro_de_audio: "",
      reaccion: "",
      dni: "45896583",
      telefono: "4784572",
      fecha_de_llamada: "2017-07-20",
      fecha_de_evaluacion: "2017-07-20",
      audio: "",
      resultado: "",
      porcent_alcance: ""
    },
    {
      id: "3",
      periodo: "",
      fecha_de_asignacion: "",
      monitor: "",
      tipo_de_cartera: "",
      proveedor: "",
      cartera: "",
      agente: "",
      nro_de_evaluacion: "",
      nro_de_audio: "",
      reaccion: "",
      dni: "",
      telefono: "",
      fecha_de_llamada: "",
      fecha_de_evaluacion: "",
      audio: "",
      resultado: "",
      porcent_alcance: ""
    }
  ];

  constructor(private router:Router,
              private cdRef:ChangeDetectorRef) {

    this.Formulario = new FormGroup({
      'id': new FormControl(this.objeto.id),
      'periodo': new FormControl(this.objeto.periodo, Validators.required),
      'fecha_de_asignacion': new FormControl(this.objeto.fecha_de_asignacion),
      'monitor': new FormControl(this.objeto.monitor, Validators.required),
      'tipo_de_cartera': new FormControl(this.objeto.tipo_de_cartera, Validators.required),
      'proveedor': new FormControl(this.objeto.proveedor, Validators.required),
      'cartera': new FormControl(this.objeto.cartera, Validators.required),
      'agente': new FormControl(this.objeto.agente, Validators.required),
      'nro_de_evaluacion': new FormControl(this.objeto.nro_de_evaluacion, Validators.required),
      'nro_de_audio': new FormControl(this.objeto.nro_de_audio, Validators.required),
      'reaccion': new FormControl(this.objeto.reaccion, Validators.required),
      'dni': new FormControl(this.objeto.dni, Validators.required),
      'telefono': new FormControl(this.objeto.telefono, Validators.required),
      'fecha_de_llamada': new FormControl(this.objeto.fecha_de_llamada),
      'fecha_de_evaluacion': new FormControl(this.objeto.fecha_de_evaluacion),
      'audio': new FormControl(this.objeto.audio, Validators.required),
      'resultado': new FormControl(this.objeto.resultado, Validators.required),
      'porcent_alcance': new FormControl(this.objeto.porcent_alcance, Validators.required)
    });
  };


    Edit(event){
      console.log(event.data);
       this.can_edit = true
       if(event.originalEvent.type=="dblclick"){
          this.accion = "editar";
          let obj = this.Clonar(event.data);
          this.Formulario.setValue(obj);
          this.display_modal = true;
       }
    }

    editarModal(dt:DataTable){
       this.accion = "editar";
       let obj = this.Clonar(dt.selection);
       // this.usuario["distrito"] = [...this.usuario["distrito"], obj.distrito]
       this.Formulario.setValue(obj);
       this.display_modal = true;
    }

    Clonar(r: DetalleDeAsignacionInterface){
           let objeto = new ClonRegistro();
           for(let prop in r){
             if(this.objeto.hasOwnProperty(prop)){
               objeto[prop] = r[prop];
             }
           }
           return objeto;
        }

    ngOnInit() {
    }

    crearModal(){
       this.display_modal = true;
       this.limpiarForm();
    }

    limpiarForm(){
      this.Formulario.reset({
        id: "",
        periodo: "",
        fecha_de_asignacion: "",
        monitor: "",
        tipo_de_cartera: "",
        proveedor: "",
        cartera: "",
        agente: "",
        nro_de_evaluacion: "",
        nro_de_audio: "",
        reaccion: "",
        dni: "",
        telefono: "",
        fecha_de_llamada: "",
        fecha_de_evaluacion: "",
        audio: "",
        resultado: "",
        porcent_alcance: ""
      });
     setTimeout(()=>{
        document.getElementById("id").focus();
     },500);
  };

    showSuccess( header, mensaje ) {
       this.msjs = [];
       this.msjs.push({severity:'success', summary: header, detail: mensaje});
    }

    showInfo( header, mensaje ) {
       this.msjs = [];
       this.msjs.push({severity:'info', summary: header, detail: mensaje});
    }

    showWarn( header, mensaje ) {
       this.msjs = [];
       this.msjs.push({severity:'warn', summary: header, detail: mensaje});
    }

    showError( header, mensaje ) {
       this.msjs = [];
       this.msjs.push({severity:'error', summary: header, detail: mensaje});
    }

    clear() {
       this.msjs = [];
    }

    changeAccion(){

    }

  }

  class ClonRegistro implements DetalleDeAsignacionInterface {
    constructor(
                public id?,
                public periodo?,
                public fecha_de_asignacion?,
                public monitor?,
                public tipo_de_cartera?,
                public proveedor?,
                public cartera?,
                public agente?,
                public nro_de_evaluacion?,
                public nro_de_audio?,
                public reaccion?,
                public dni?,
                public telefono?,
                public fecha_de_llamada?,
                public fecha_de_evaluacion?,
                public audio?,
                public resultado?,
                public porcent_alcance?){}
  }
