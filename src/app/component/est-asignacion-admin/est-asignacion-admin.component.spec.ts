import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EstAsignacionAdminComponent } from './est-asignacion-admin.component';

describe('EstAsignacionAdminComponent', () => {
  let component: EstAsignacionAdminComponent;
  let fixture: ComponentFixture<EstAsignacionAdminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EstAsignacionAdminComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EstAsignacionAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
