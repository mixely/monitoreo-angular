import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgrDiacorteCartComponent } from './agr-diacorte-cart.component';

describe('AgrDiacorteCartComponent', () => {
  let component: AgrDiacorteCartComponent;
  let fixture: ComponentFixture<AgrDiacorteCartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgrDiacorteCartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgrDiacorteCartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
