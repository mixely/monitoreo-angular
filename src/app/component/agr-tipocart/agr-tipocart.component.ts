import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { NgForm, FormGroup, FormControl, Validators } from "@angular/forms";
import { Message, MenuItem, DataTable } from 'primeng/primeng';
import { GrowlModule } from 'primeng/primeng';
import { NgModule } from '@angular/core';
import { TipoCarteraInterface } from '../../interfaces/TipoCarteraInterface';
import { InputTextModule, DataTableModule, ButtonModule, DialogModule} from 'primeng/primeng';


@Component({
  selector: 'app-agr-tipocart',
  templateUrl: './agr-tipocart.component.html',
  host: {'(window:keydown)': 'hotkeys($event)'}
})

export class AgrTipocartComponent implements OnInit {

  hotkeys(event){
     // ALT + "+"
     if(event.altKey && event.keyCode==107){
        this.display_modal = true;
        this.limpiarForm();
     }
  }

  Formulario:FormGroup;
  can_edit = false;
  display_modal = false;
  accion:string = "crear";
  msjs:Message[] = [];

  objeto:TipoCarteraInterface = {
    id: "",
    fecha: "",
    nombre: "",
    descripcion: "",
    estado: ""
  }

  Tabla:any[] = [
    {
      id: "1",
      fecha: "2017-07-01",
      nombre: "Ripley",
      descripcion: "",
      estado: "Activo"
    },
    {
      id: "2",
      fecha: "2017-07-01",
      nombre: "Claro",
      descripcion: "",
      estado: "Activo"
    },
    {
      id: "3",
      fecha: "2017-07-01",
      nombre: "Clt",
      descripcion: "Multicartera",
      estado: "Inactivo"
    }
  ];

  constructor(private router:Router,
              private cdRef:ChangeDetectorRef) {

    this.Formulario = new FormGroup({
      'id': new FormControl(this.objeto.id),
      'fecha': new FormControl(this.objeto.fecha),
      'nombre': new FormControl(this.objeto.nombre, Validators.required),
      'descripcion': new FormControl(this.objeto.descripcion, Validators.required),
      'estado': new FormControl(this.objeto.estado, Validators.required)
    });
};

  Edit(event){
    console.log(event.data);
     this.can_edit = true
     if(event.originalEvent.type=="dblclick"){
        this.accion = "editar";
        let obj = this.Clonar(event.data);
        this.Formulario.setValue(obj);
        this.display_modal = true;
     }
  }

  editarModal(dt:DataTable){
     this.accion = "editar";
     let obj = this.Clonar(dt.selection);
     // this.usuario["distrito"] = [...this.usuario["distrito"], obj.distrito]
     this.Formulario.setValue(obj);
     this.display_modal = true;
  }

  Clonar(r: TipoCarteraInterface){
    let objeto = new ClonRegistro();
    for(let prop in r){
      if(this.objeto.hasOwnProperty(prop)){
        objeto[prop] = r[prop];
      }
    }
    return objeto;
 }

  ngOnInit() {
  }


  crearModal(){
     this.display_modal = true;
     this.limpiarForm();
  }

  limpiarForm(){
    this.Formulario.reset({
      id: "",
      fecha: "",
      nombre: "",
      descripcion: "",
      estado: ""
    });
   setTimeout(()=>{
      document.getElementById("fecha").focus();
   },500);
};


  showSuccess( header, mensaje ) {
     this.msjs = [];
     this.msjs.push({severity:'success', summary: header, detail: mensaje});
  }

  showInfo( header, mensaje ) {
     this.msjs = [];
     this.msjs.push({severity:'info', summary: header, detail: mensaje});
  }

  showWarn( header, mensaje ) {
     this.msjs = [];
     this.msjs.push({severity:'warn', summary: header, detail: mensaje});
  }

  showError( header, mensaje ) {
     this.msjs = [];
     this.msjs.push({severity:'error', summary: header, detail: mensaje});
  }

  clear() {
     this.msjs = [];
  }

  changeAccion(){

  }

}

class ClonRegistro implements TipoCarteraInterface {
  constructor(
              public id?,
              public fecha?,
              public nombre?,
              public descripcion?,
              public estado?){}
}
