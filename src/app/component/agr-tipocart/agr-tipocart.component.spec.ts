import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgrTipocartComponent } from './agr-tipocart.component';

describe('AgrTipocartComponent', () => {
  let component: AgrTipocartComponent;
  let fixture: ComponentFixture<AgrTipocartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgrTipocartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgrTipocartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
