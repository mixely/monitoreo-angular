import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgrRelTipocartCartComponent } from './agr-rel-tipocart-cart.component';

describe('AgrRelTipocartCartComponent', () => {
  let component: AgrRelTipocartCartComponent;
  let fixture: ComponentFixture<AgrRelTipocartCartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgrRelTipocartCartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgrRelTipocartCartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
