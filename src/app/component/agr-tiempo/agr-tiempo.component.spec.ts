import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgrTiempoComponent } from './agr-tiempo.component';

describe('AgrTiempoComponent', () => {
  let component: AgrTiempoComponent;
  let fixture: ComponentFixture<AgrTiempoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgrTiempoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgrTiempoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
