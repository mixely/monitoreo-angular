import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgrRelJustifReaccComponent } from './agr-rel-justif-reacc.component';

describe('AgrRelJustifReaccComponent', () => {
  let component: AgrRelJustifReaccComponent;
  let fixture: ComponentFixture<AgrRelJustifReaccComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgrRelJustifReaccComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgrRelJustifReaccComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
