import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgrRelMonitPerfComponent } from './agr-rel-monit-perf.component';

describe('AgrRelMonitPerfComponent', () => {
  let component: AgrRelMonitPerfComponent;
  let fixture: ComponentFixture<AgrRelMonitPerfComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgrRelMonitPerfComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgrRelMonitPerfComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
