import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { NgForm, FormGroup, FormControl, Validators } from "@angular/forms";
import { Message, MenuItem, DataTable } from 'primeng/primeng';
import { GrowlModule } from 'primeng/primeng';
import { NgModule } from '@angular/core';
import { RelacionMonitorCarteraInterface } from '../../interfaces/RelacionMonitorCarteraInterface';
import { InputTextModule, DataTableModule, ButtonModule, DialogModule } from 'primeng/primeng';
import { CalendarModule } from 'primeng/primeng';

@Component({
  selector: 'app-rel-monit-cart',
  templateUrl: './rel-monit-cart.component.html',
  host: {'(window:keydown)': 'hotkeys($event)'}
})

export class RelMonitCartComponent implements OnInit {

  hotkeys(event){
     // ALT + "+"
     if(event.altKey && event.keyCode==107){
        this.display_modal = true;
        this.limpiarForm();
     }
  }

  Formulario:FormGroup;
  can_edit = false;
  display_modal = false;
  accion:string = "crear";
  msjs:Message[] = [];

  objeto:RelacionMonitorCarteraInterface = {
    id: "",
    fecha: "",
    monitor: "",
    tipo_de_cartera: "",
    proveedor: "",
    cartera: "",
    antiguo: "",
    nuevo: "",
    transito: "",
    asignacion: "",
    estado: "",
  }

  Tabla:any[] = [
    {
      id: "1",
      fecha: "2017-05-15",
      monitor: "Jose Perez",
      tipo_de_cartera: "Castigada",
      proveedor: "SCI",
      cartera: "Tardia",
      antiguo: "3",
      nuevo: "2",
      transito: "2",
      asignacion: "18",
      estado: "Activo",
    },
    {
      id: "2",
      fecha: "2017-07-15",
      monitor: "Maria Lopez",
      tipo_de_cartera: "Castigada",
      proveedor: "Banco Ripley",
      cartera: "Recovery",
      antiguo: "4",
      nuevo: "2",
      transito: "2",
      asignacion: "20",
      estado: "Activo",
    },
    {
      id: "3",
      fecha: "2017-07-20",
      monitor: "",
      tipo_de_cartera: "",
      proveedor: "",
      cartera: "",
      antiguo: "",
      nuevo: "",
      transito: "",
      asignacion: "",
      estado: "Activo",
    }
  ];

  constructor(private router:Router,
              private cdRef:ChangeDetectorRef) {

    this.Formulario = new FormGroup({
      'id': new FormControl(this.objeto.id),
      'fecha': new FormControl(this.objeto.fecha),
      'monitor': new FormControl(this.objeto.monitor, Validators.required),
      'tipo_de_cartera': new FormControl(this.objeto.tipo_de_cartera, Validators.required),
      'proveedor': new FormControl(this.objeto.proveedor, Validators.required),
      'cartera': new FormControl(this.objeto.cartera, Validators.required),
      'antiguo': new FormControl(this.objeto.antiguo, Validators.required),
      'nuevo': new FormControl(this.objeto.nuevo, Validators.required),
      'transito': new FormControl(this.objeto.transito, Validators.required),
      'asignacion': new FormControl(this.objeto.asignacion),
      'estado': new FormControl(this.objeto.estado, Validators.required)
    });
  };

  Edit(event){
    console.log(event.data);
     this.can_edit = true
     if(event.originalEvent.type=="dblclick"){
        this.accion = "editar";
        let obj = this.Clonar(event.data);
        this.Formulario.setValue(obj);
        this.display_modal = true;
     }
  }

  editarModal(dt:DataTable){
     this.accion = "editar";
     let obj = this.Clonar(dt.selection);
     this.Formulario.setValue(obj);
     this.display_modal = true;
  }

  Clonar(r: RelacionMonitorCarteraInterface){
       let objeto = new ClonRegistro();
       for(let prop in r){
         if(this.objeto.hasOwnProperty(prop)){
           objeto[prop] = r[prop];
         }
       }
       return objeto;
    }

  ngOnInit() {
  }

  crearModal(){
     this.display_modal = true;
     this.limpiarForm();
  }

  limpiarForm(){
    this.Formulario.reset({
      id: "",
      fecha: "",
      monitor: "",
      tipo_de_cartera: "",
      proveedor: "",
      cartera: "",
      antiguo: "",
      nuevo: "",
      transito: "",
      asignacion: "",
      estado: "",
    });
   setTimeout(()=>{
      document.getElementById("id").focus();
   },500);
  };

  showSuccess( header, mensaje ) {
     this.msjs = [];
     this.msjs.push({severity:'success', summary: header, detail: mensaje});
  }

  showInfo( header, mensaje ) {
     this.msjs = [];
     this.msjs.push({severity:'info', summary: header, detail: mensaje});
  }

  showWarn( header, mensaje ) {
     this.msjs = [];
     this.msjs.push({severity:'warn', summary: header, detail: mensaje});
  }

  showError( header, mensaje ) {
     this.msjs = [];
     this.msjs.push({severity:'error', summary: header, detail: mensaje});
  }

  clear() {
     this.msjs = [];
  }

  changeAccion(){

  }

  }

  class ClonRegistro implements RelacionMonitorCarteraInterface {
  constructor(
              public id?,
              public fecha?,
              public monitor?,
              public tipo_de_cartera?,
              public proveedor?,
              public cartera?,
              public antiguo?,
              public nuevo?,
              public transito?,
              public asignacion?,
              public estado?){}
  }
