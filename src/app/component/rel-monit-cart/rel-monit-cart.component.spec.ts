import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RelMonitCartComponent } from './rel-monit-cart.component';

describe('RelMonitCartComponent', () => {
  let component: RelMonitCartComponent;
  let fixture: ComponentFixture<RelMonitCartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RelMonitCartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RelMonitCartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
