import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { NgForm, FormGroup, FormControl, Validators } from "@angular/forms";

import { PaletaResultadosService } from '../../services/paleta-resultados.service';
import { ProveedorService } from '../../services/proveedor.service';
import { CarteraService } from '../../services/cartera.service';
import { ResultadoService } from '../../services/resultado.service';
import { JustificacionService } from '../../services/justificacion.service';
import { LoginService } from '../../services/login.service';

import { PaletaResultadosInterface } from '../../interfaces/paleta-resultados.interface';

import { Message, MenuItem, DataTable } from 'primeng/primeng';

@Component({
  selector: 'app-paleta-resultado',
  templateUrl: './paleta-resultado.component.html',
  host: {'(window:keydown)': 'hotkeys($event)'}
})
export class PaletaResultadoComponent implements OnInit {

  Formulario:FormGroup;

  display_modal:boolean = false;
  accion:string = "crear";
  msjs:Message[] = [];

  proveedores:any[] = [];
  carteras:any[] = [];
  resultados:any[] = [];
  justificaciones:any[] = [];

  selectedRecord:PaletaResultadosInterface;
  Tabla:PaletaResultadosInterface[] = [];
  csv:boolean = true; // Si es "true" entonces va a desahabilitar
  

  can_edit = false;

  constructor(private cdRef:ChangeDetectorRef,
              private _prs:PaletaResultadosService,
              private _ps:ProveedorService,
              private _cs:CarteraService,
              private _ls:LoginService,
              private _rs:ResultadoService,
              private _js:JustificacionService
              ) {
    this.Formulario = new FormGroup({
      'id': new FormControl(this.objeto.id),
      'id_proveedor': new FormControl(this.objeto.id_proveedor, Validators.required),
      'id_cartera': new FormControl(this.objeto.id_cartera, Validators.required),
      'id_resultado': new FormControl(this.objeto.id_resultado, Validators.required),
      'id_justificacion': new FormControl(this.objeto.id_justificacion, Validators.required),
      'estado': new FormControl(this.objeto.estado)
    });
  }

  // host: {'(window:keydown)': 'hotkeys($event)'}
  hotkeys(event){
    // ALT + C
    if(event.altKey && event.keyCode==107){
       this.display_modal = true;
    }
  }

  objeto:PaletaResultadosInterface = {
    id: "",
    id_proveedor: "",
    id_cartera: "",
    id_resultado: "",
    id_justificacion: "",
    estado: ""
  }

  limpiarForm(){
    this.Formulario.reset({
      id: "",
      id_proveedor: "",
      id_cartera: "",
      id_resultado: "",
      id_justificacion: "",
      estado: ""
    });

    setTimeout(()=>{
      document.getElementById("id_proveedor").focus();
    },500);
  };

  ngOnInit(){
    this.get();
    this._ps.get()
            .subscribe(
              data => {
               this.proveedores = data.datos;
              },
              error =>{
                this._ls.handleError(error);
              });
    this._rs.get()
           .subscribe(
             data => {
                this.resultados = data.datos;
             },
             error =>{
               this._ls.handleError(error);
             });
    this._js.get()
           .subscribe(
             data => {
                this.justificaciones = data.datos;
             },
             error =>{
               this._ls.handleError(error);
             });

  }

  ngAfterViewChecked(){
   this.cdRef.detectChanges();
  }

  crearModal(){
    this.display_modal = true;
    this.limpiarForm();
  }

  generarCSV(dt:DataTable){
    dt.exportCSV();
  }

  editarModal(dt:DataTable){
     this.accion = "editar";
     let obj = this.Clonar(dt.selection);
     this.Formulario.setValue(obj);
     this.display_modal = true;
  }

  Edit(event){
     this.can_edit = true
     if(event.originalEvent.type=="dblclick"){
        this.accion = "editar";
        let obj = this.Clonar(event.data);
        this.Formulario.setValue(obj);
        this.display_modal = true;
     }
  }

  Selected(obj){
     this.objeto.id = obj.id;
  }
  changeAccion(){
   this.accion = "crear";
  }

Clonar(r: PaletaResultadosInterface){
   let objeto = new ClonRegistro();
   for(let prop in r){
     if(this.objeto.hasOwnProperty(prop)){
       objeto[prop] = r[prop];
     }
   }
   return objeto;
}

Crear(){
   this._prs.set( this.objeto )
   .subscribe(
     data =>{
       this.Tabla = [];
        for (let i = 0; i < data.datos.length; i++) {
           this.Tabla = [...this.Tabla, data.datos[i]];
        }
        this.display_modal = false;
        if(this.Tabla.length>0){
          this.csv = false; // Entonces habilitamos el botón
        }
        this.showSuccess('Exito', "Hemos añadido el tipo de resultado al sistema");
     },
     error =>{
       this._ls.handleError(error);
     });
}

Editar(){
  this.display_modal = false;
   this._prs.update( this.objeto )
           .subscribe(
             data => {
               this.showSuccess("Exito", "Hemos actualizado el tipo de resultado");
               this.get();
             },
             error =>{
               this._ls.handleError(error);
             });
}

get(){
   this.showWarn( "Espere", "Estamos obteniendo los datos" );
   this._prs.get()
           .subscribe(
             data => {
                this.Tabla = [];
                this.Tabla = data.datos;
                this.can_edit = false;
                this.clear();
             },
             error =>{
               this._ls.handleError(error);
             });
}

showSuccess( header, mensaje ) {
   this.msjs.push({severity:'success', summary: header, detail: mensaje});
   setTimeout(()=> this.clear(), 3000);
}

showInfo( header, mensaje ) {
   this.msjs.push({severity:'info', summary: header, detail: mensaje});
   setTimeout(()=> this.clear(), 3000);
}

showWarn( header, mensaje ) {
   this.msjs.push({severity:'warn', summary: header, detail: mensaje});
   setTimeout(()=> this.clear(), 3000);
}

showError( header, mensaje ) {
   this.msjs.push({severity:'error', summary: header, detail: mensaje});
   setTimeout(()=> this.clear(), 3000);
}

clear(){
   this.msjs = [];
}

cargarCarteras(id_proveedor){
   this._cs.getCarteras(id_proveedor)
           .subscribe(
            data =>{
              this.carteras = data.datos;
            },
            error =>{
              this._ls.handleError(error);
            });
}

}

class ClonRegistro implements PaletaResultadosInterface {
   constructor(
     public id?,
     public id_proveedor?,
     public id_cartera?,
     public id_resultado?,
     public id_justificacion?,
     public estado?){}
}
