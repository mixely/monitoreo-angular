import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { NgForm, FormGroup, FormControl, Validators } from "@angular/forms";
import { Message, MenuItem, DataTable } from 'primeng/primeng';
import { GrowlModule } from 'primeng/primeng';
import { NgModule } from '@angular/core';
import { CondicionInterface } from '../../interfaces/CondicionInterface';
import { EquiposDeTrabajoInterface } from '../../interfaces/EquiposDeTrabajoInterface';
import { InputTextModule, DataTableModule, ButtonModule, DialogModule } from 'primeng/primeng';
import { CalendarModule } from 'primeng/primeng';

@Component({
  selector: 'app-equi-trabajo',
  templateUrl: './equi-trabajo.component.html',
  host: {'(window:keydown)': 'hotkeys($event)'}
})
export class EquiTrabajoComponent implements OnInit {

  hotkeys(event){
     // ALT + "+"
     if(event.altKey && event.keyCode==107){
        this.display_modal = true;
        this.limpiarForm();
     }
  }

  Formulario:FormGroup;
  can_edit = false;
  display_modal = false;
  accion:string = "crear";
  msjs:Message[] = [];

  objeto:EquiposDeTrabajoInterface = {
    id: "",
    fecha_de_ingreso: "",
    proveedor: "",
    cartera: "",
    agente: "",
    turno: "",
    perfil: "",
    condicion: "",
    estado: "",
  }

  Tabla:any[] = [
    {
      id: "1",
      fecha_de_ingreso: "2017-05-15",
      proveedor: "SCI",
      cartera: "PROPIA",
      agente: "Luis Perez",
      turno: "PT-Mañana",
      perfil: "Call",
      condicion: "Antiguo",
      estado: "Activo",
    },
    {
      id: "2",
      fecha_de_ingreso: "2017-07-15",
      proveedor: "SCI",
      cartera: "PROPIA",
      agente: "Maria Perez",
      turno: "PT-Tarde",
      perfil: "Call",
      condicion: "Nuevo",
      estado: "Activo",
    },
    {
      id: "3",
      fecha_de_ingreso: "2017-07-20",
      proveedor: "Banco Ripley",
      cartera: "Tardia",
      agente: "Maria Torres",
      turno: "Full Time",
      perfil: "Call",
      condicion: "Nuevo",
      estado: "Activo",
    }
  ];

  constructor(private router:Router,
              private cdRef:ChangeDetectorRef) {

    this.Formulario = new FormGroup({
      'id': new FormControl(this.objeto.id),
      'fecha_de_ingreso': new FormControl(this.objeto.fecha_de_ingreso),
      'proveedor': new FormControl(this.objeto.proveedor, Validators.required),
      'cartera': new FormControl(this.objeto.cartera, Validators.required),
      'agente': new FormControl(this.objeto.agente, Validators.required),
      'turno': new FormControl(this.objeto.turno, Validators.required),
      'perfil': new FormControl(this.objeto.perfil, Validators.required),
      'condicion': new FormControl(this.objeto.condicion, Validators.required),
      'estado': new FormControl(this.objeto.estado, Validators.required)
    });
  };

  Edit(event){
    console.log(event.data);
     this.can_edit = true
     if(event.originalEvent.type=="dblclick"){
        this.accion = "editar";
        let obj = this.Clonar(event.data);
        this.Formulario.setValue(obj);
        this.display_modal = true;
     }
  }

  editarModal(dt:DataTable){
     this.accion = "editar";
     let obj = this.Clonar(dt.selection);
     // this.usuario["distrito"] = [...this.usuario["distrito"], obj.distrito]
     this.Formulario.setValue(obj);
     this.display_modal = true;
  }

  Clonar(r: CondicionInterface){
       let objeto = new ClonRegistro();
       for(let prop in r){
         if(this.objeto.hasOwnProperty(prop)){
           objeto[prop] = r[prop];
         }
       }
       return objeto;
    }

  ngOnInit() {
  }

  crearModal(){
     this.display_modal = true;
     this.limpiarForm();
  }

  limpiarForm(){
    this.Formulario.reset({
      id: "",
      fecha_de_ingreso: "",
      proveedor: "",
      cartera: "",
      agente: "",
      turno: "",
      perfil: "",
      condicion: "",
      estado: "",
    });
   setTimeout(()=>{
      document.getElementById("id").focus();
   },500);
  };

  showSuccess( header, mensaje ) {
     this.msjs = [];
     this.msjs.push({severity:'success', summary: header, detail: mensaje});
  }

  showInfo( header, mensaje ) {
     this.msjs = [];
     this.msjs.push({severity:'info', summary: header, detail: mensaje});
  }

  showWarn( header, mensaje ) {
     this.msjs = [];
     this.msjs.push({severity:'warn', summary: header, detail: mensaje});
  }

  showError( header, mensaje ) {
     this.msjs = [];
     this.msjs.push({severity:'error', summary: header, detail: mensaje});
  }

  clear() {
     this.msjs = [];
  }

  changeAccion(){

  }

  }

  class ClonRegistro implements EquiposDeTrabajoInterface {
  constructor(
              public id?,
              public fecha_de_ingreso?,
              public proveedor?,
              public cartera?,
              public agente?,
              public turno?,
              public perfil?,
              public condicion?,
              public estado?){}
  }
