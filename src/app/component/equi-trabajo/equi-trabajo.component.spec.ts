import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EquiTrabajoComponent } from './equi-trabajo.component';

describe('EquiTrabajoComponent', () => {
  let component: EquiTrabajoComponent;
  let fixture: ComponentFixture<EquiTrabajoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EquiTrabajoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EquiTrabajoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
