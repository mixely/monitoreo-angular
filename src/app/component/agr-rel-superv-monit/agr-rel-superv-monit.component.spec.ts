import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgrRelSupervMonitComponent } from './agr-rel-superv-monit.component';

describe('AgrRelSupervMonitComponent', () => {
  let component: AgrRelSupervMonitComponent;
  let fixture: ComponentFixture<AgrRelSupervMonitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgrRelSupervMonitComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgrRelSupervMonitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
