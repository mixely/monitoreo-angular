import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { NgForm, FormGroup, FormControl, Validators } from "@angular/forms";

import { CarteraService } from '../../services/cartera.service';
import { SegmentoService } from '../../services/segmento.service';
import { LoginService } from '../../services/login.service';

import { SegmentoInterface } from '../../interfaces/segmento.interface';
import { Message, MenuItem, DataTable } from 'primeng/primeng';

@Component({
  selector: 'app-segmento',
  templateUrl: './segmento.component.html',
  styles: []
})
export class SegmentoComponent implements OnInit {

  Formulario:FormGroup;

  display_modal:boolean = false;
  accion:string = "crear";
  msjs:Message[] = [];
  // esta variable la usaremos para saber si mostrar o no el listado de segmentos de una cartera en la vista.
  CarteraSeleccionada = false;

  carteras:any[] = [];
  segmentos:any[] = [];

  selectedRecord:SegmentoInterface;
  Tabla:SegmentoInterface[] = [];
  csv:boolean = true; // Si es "true" entonces va a desahabilitar
  can_edit = false;

  constructor(private cdRef:ChangeDetectorRef,
              private _cs:CarteraService,
              private _ls:LoginService,
              private _ss:SegmentoService
              ) {
    this.Formulario = new FormGroup({
      'id': new FormControl(this.objeto.id),
      'nombre': new FormControl(this.objeto.nombre, Validators.required),
      'id_cartera': new FormControl(this.objeto.id_cartera, Validators.required),
      'estado': new FormControl(this.objeto.estado)
    });
  }

  // host: {'(window:keydown)': 'hotkeys($event)'}
  hotkeys(event){
    // ALT + C
    if(event.altKey && event.keyCode==107){
       this.display_modal = true;
    }
  }

  objeto:SegmentoInterface = {
    id:"",
    nombre:"",
    id_cartera:"",
    estado:""
  }

  verificarCarteraSeleccionada(id_cartera){
    if(id_cartera!="undefined"){
      this.CarteraSeleccionada = true;
      this._cs.getSegmentosDeCartera(id_cartera)
              .subscribe(
              data =>{
                this.segmentos = data.datos;
              },
              error =>{
                this._ls.handleError(error);
              });
    }
  }

  limpiarForm(){
    this.Formulario.reset({
      id:"",
      nombre:"",
      id_cartera:"",
      estado:""
    });

    setTimeout(()=>{
      document.getElementById("nombre").focus();
    },500);
  };

  ngOnInit() {
    this.get();
    this._cs.get()
            .subscribe(
              data => {
                this.carteras = data.datos;
              },
              error =>{
                this._ls.handleError(error);
              });

  }

  ngAfterViewChecked(){
   this.cdRef.detectChanges();
  }

  crearModal(){
    this.segmentos = [];
    this.display_modal = true;
    this.limpiarForm();
  }

  generarCSV(dt:DataTable){
    dt.exportCSV();
  }

  editarModal(dt:DataTable){
     this.accion = "editar";
     let obj = this.Clonar(dt.selection);
     this.Formulario.setValue(obj);
     this.display_modal = true;
  }

  Edit(event){
     this.can_edit = true
     if(event.originalEvent.type=="dblclick"){
        this.accion = "editar";
        let obj = this.Clonar(event.data);
        this.Formulario.setValue(obj);
        this.display_modal = true;
     }
  }

  Selected(obj){
     this.objeto.id = obj.id;
  }
  changeAccion(){
   this.accion = "crear";
   this.CarteraSeleccionada = false;
}

Clonar(r: SegmentoInterface){
   let objeto = new ClonRegistro();
   for(let prop in r){
     if(this.objeto.hasOwnProperty(prop)){
       objeto[prop] = r[prop];
     }
   }
   return objeto;
}

Crear(){
   this._ss.set( this.objeto )
   .subscribe(
     data =>{
       this.Tabla = [];
        for (let i = 0; i < data.datos.length; i++) {
           this.Tabla = [...this.Tabla, data.datos[i]];
        }
        this.display_modal = false;
        if(this.Tabla.length>0){
          this.csv = false; // Entonces habilitamos el botón
        }
        this.showSuccess('Exito', "Hemos añadido al sistema");
     },
     error =>{
       this._ls.handleError(error);
     });
}

Editar(){
  this.display_modal = false;
   this._ss.update( this.objeto )
           .subscribe(
             data => {
               this.showSuccess("Exito", "Hemos actualizado el tipo de resultado");
               this.get();
             },
             error =>{
               this._ls.handleError(error);
             });
}

get(){
   this.showWarn( "Espere", "Estamos obteniendo los datos" );
   this._ss.get()
           .subscribe(
             data => {
                this.Tabla = [];
                this.Tabla = data.datos;
                this.can_edit = false;
             },
             error =>{
               this._ls.handleError(error);
             });
}

showSuccess( header, mensaje ) {
   this.msjs.push({severity:'success', summary: header, detail: mensaje});
   setTimeout(()=> this.clear(), 3000);
}

showInfo( header, mensaje ) {
   this.msjs.push({severity:'info', summary: header, detail: mensaje});
   setTimeout(()=> this.clear(), 3000);
}

showWarn( header, mensaje ) {
   this.msjs.push({severity:'warn', summary: header, detail: mensaje});
   setTimeout(()=> this.clear(), 3000);
}

showError( header, mensaje ) {
   this.msjs.push({severity:'error', summary: header, detail: mensaje});
   setTimeout(()=> this.clear(), 3000);
}

clear() {
   this.msjs = [];
}

}

class ClonRegistro implements SegmentoInterface {
   constructor(
     public id?,
     public nombre?,
     public id_cartera?,
     public estado?){}
}
