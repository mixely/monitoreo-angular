import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { NgForm, FormGroup, FormControl, Validators } from "@angular/forms";

import { UsuarioService } from '../../services/usuario.service';
import { UbigeoService } from '../../services/ubigeo.service';
import { PerfilesService } from '../../services/perfiles.service';
import { LoginService } from '../../services/login.service';

import { UsuarioInterface } from '../../interfaces/usuario.interface';
import { Message, MenuItem, DataTable } from 'primeng/primeng';

@Component({
  selector: 'app-usuario',
  templateUrl: './usuario.component.html',
  styleUrls: ['./usuario.component.css'],
  styles: [`
    div.ui-dialog-content {
      height: 120% !important;
      background: red;
    }
  `],
  host: {'(window:keydown)': 'hotkeys($event)'}
})

export class UsuarioComponent implements OnInit {

   es: any;
   accion:string = "crear";
   msjs:Message[] = [];
   results:any[] = [];
   selectedRecord:UsuarioInterface;
   Tabla:UsuarioInterface[] = [];
   csv:boolean = true; // Si es "true" entonces va a desahabilitar
   estado_civil:string[] = [ "Soltero/a", "Casado/a", "Viudo/a", "Divorciado/a" ];
   turnos:string[] = [ "Mañana", "Tarde", "Full" ];
   perfiles:any[] = [];

   display_modal = false;
  //  display_alert = false;
   can_edit = false;

   Formulario:FormGroup;
   arr_elementos:any[] = [];

  constructor(private router:Router,
              private _us:UsuarioService,
              private _ls:LoginService,
              private _ubs:UbigeoService,
              private cdRef:ChangeDetectorRef,
              private _ps:PerfilesService) {

    this.Formulario = new FormGroup({
      'id': new FormControl(this.objeto.id),
      'dni': new FormControl(this.objeto.dni, [Validators.required, Validators.minLength(8)]),
      'ap_paterno': new FormControl(this.objeto.ap_paterno, Validators.required),
      'ap_materno': new FormControl(this.objeto.ap_materno, Validators.required),
      'nombres': new FormControl(this.objeto.nombres, Validators.required),
      'celular': new FormControl(this.objeto.celular, Validators.required),
      'fijo': new FormControl(this.objeto.fijo, Validators.required),
      'fecnac': new FormControl(this.objeto.fecnac, Validators.required),
      'est_civil': new FormControl(this.objeto.est_civil, Validators.required),
      'direccion': new FormControl(this.objeto.direccion, Validators.required),
      'ubigeo': new FormControl(this.objeto.ubigeo, Validators.required),
      'email_corp': new FormControl(this.objeto.email_corp, Validators.required),
      'email_per': new FormControl(this.objeto.email_per, Validators.required),
      'contacto_emergencia': new FormControl(this.objeto.contacto_emergencia, Validators.required),
      'telef_contacto': new FormControl(this.objeto.telef_contacto, Validators.required),
      'login': new FormControl(this.objeto.login, Validators.required),
      'idusuario_sede': new FormControl(this.objeto.idusuario_sede, Validators.required),
      'idproveedor': new FormControl(this.objeto.idproveedor, Validators.required),
      'idcartera': new FormControl(this.objeto.idcartera, Validators.required),
      'proveedor': new FormControl(this.objeto.proveedor, Validators.required),
      'cartera': new FormControl(this.objeto.cartera, Validators.required),
      'fec_ingreso': new FormControl(this.objeto.fec_ingreso, Validators.required),
      'perfil': new FormControl(this.objeto.perfil, Validators.required),
      'turno': new FormControl(this.objeto.turno, Validators.required),
      'estado': new FormControl(this.objeto.estado, Validators.required)
      // 'ubigeo': new FormControl(this.objeto.ubigeo, Validators.required),
      // 'distrito': new FormControl(this.objeto.distrito, Validators.required),
      // 'id_perfil': new FormControl(this.objeto.id_perfil, Validators.required),
      // 'user': new FormControl(this.objeto.user, Validators.required)
    });

    this._ps.get()
            .subscribe(data => {
              this.perfiles = data.data;
            },
            error =>{
              this._ls.handleError(error);
            });
};

  objeto:UsuarioInterface = {
    // id:"",
    // dni:"",
    // idubigeo: '',
    // nombres:"",
    // fecnac: "",
    // est_civil:"",
    // fec_ingreso: "",
    // movil:"",
    // fijo:"",
    // direccion:"",
    // email_corp:"",
    // email_per:"",
    // user:"",
    // contacto_emergencia:"",
    // telef_contacto:"",
    // perfil:"",
    // id_perfil:"",
    // turno:"",
    // ubigeo:"",
    // distrito:""
    id: "",
    dni: "",
    ap_paterno: "",
    ap_materno: "",
    nombres: "",
    celular: "",
    fijo: "",
    fecnac: "",
    est_civil: "",
    direccion: "",
    ubigeo: "",
    email_corp: "",
    email_per: "",
    contacto_emergencia: "",
    telef_contacto: "",
    login: "",
    idusuario_sede: "",
    idproveedor: "",
    idcartera: "",
    proveedor: "",
    cartera: "",
    fec_ingreso: "",
    perfil: "",
    turno: "",
    estado: ""
};


  limpiarForm(){
    this.Formulario.reset({
      id: "",
      dni: "",
      ap_paterno: "",
      ap_materno: "",
      nombres: "",
      celular: "",
      fijo: "",
      fecnac: "",
      est_civil: "",
      direccion: "",
      ubigeo: "",
      email_corp: "",
      email_per: "",
      contacto_emergencia: "",
      telef_contacto: "",
      login: "",
      idusuario_sede: "",
      idproveedor: "",
      idcartera: "",
      proveedor: "",
      cartera: "",
      fec_ingreso: "",
      perfil: "",
      turno: "",
      estado: ""
    });
   setTimeout(()=>{
      document.getElementById("dni").focus();
   },500);
};

ngOnInit() {
   this.get();

   this.es = {
       firstDayOfWeek: 0,
       dayNames: ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sabado"],
       dayNamesShort: ["Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab"],
       dayNamesMin: ["Do","Lu","Ma","Mi","Ju","Vi","Sa"],
       monthNames: [ "Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Setiembre","Octubre","Noviembre","Diciembre" ],
       monthNamesShort: [ "Ene", "Feb", "Mar", "Abr", "May", "Jun","Jul", "Ago", "Set", "Oct", "Nov", "Dic" ]
   };

}

hotkeys(event){
   // ALT + "+"
   if(event.altKey && event.keyCode==107){
      this.display_modal = true;
      this.limpiarForm();
   }
}

// Este método nos permite evitar el error que lanza angular cuando detecta algún cambio en algún elemento.
// Es un error feo, es necesario ponerlo siempre.
ngAfterViewChecked(){
   this.cdRef.detectChanges();
}

// verRegistro(reg:UsuarioInterface){
//    this.msjs = [];
//    this.msjs.push({severity: 'info', summary: 'Usuario seleccionado', detail: reg.nombres + ' - ' + reg.ap_paterno});
// }

crearModal(){
  this.display_modal = true;
  // this.can_edit = false;
  this.limpiarForm();
}

Buscar(event){
   this._ubs.cargarUbigeo( event.query )
            .subscribe(
              data => {
                 this.results = [];
                 this.arr_elementos = [];
                 for (let i = 0; i < data.ubigeo.length; i++) {
                    this.results = [...this.results, data.ubigeo[i]];
                 }
              },
              error =>{
                this._ls.handleError(error);
              });
}

generarCSV(dt:DataTable){
   dt.exportCSV();
}

editarModal(dt:DataTable){
   this.accion = "editar";
   let obj = this.Clonar(dt.selection);
   // this.objeto["distrito"] = [...this.objeto["distrito"], obj.distrito]
   this.Formulario.setValue(obj);
   this.display_modal = true;
}

Edit(event){
   this.can_edit = true
   if(event.originalEvent.type=="dblclick"){
      this.accion = "editar";
      let obj = this.Clonar(event.data);
      this.Formulario.setValue(obj);
      this.display_modal = true;
   }
}

// Selected(obj){
//    this.objeto.idubigeo = obj.idubigeo;
// }

Selected(obj){
   this.objeto.ubigeo = obj.ubigeo;
}


changeAccion(){
   this.accion = "crear";
}

Clonar(r: UsuarioInterface){
   let objeto = new ClonRegistro();
   for(let prop in r){
      if(objeto.hasOwnProperty(prop)){
         objeto[prop] = r[prop];
      }
   }
   return objeto;
}

Crear(){
   this._us.set( this.objeto )
   .subscribe(
     data =>{
        for (let i = 0; i < data.usuarios.length; i++) {
           this.Tabla = [...this.Tabla, data.usuarios[i]];
        }
        this.display_modal = false;
        this.showSuccess('Exito', "Hemos añadido el objeto al sistema");
        if(this.Tabla.length>0){
          this.csv = false; // Entonces habilitamos el botón
        }
     },
     error =>{
       this._ls.handleError(error);
     });
}

Editar(){
  this.display_modal = false;
   this._us.update( this.objeto )
           .subscribe(
             data => {
               this.get();
             },
             error =>{
               this._ls.handleError(error);
             });
}

get(){
   this.showWarn( "Espere", "Estamos obteniendo los datos" );
   this._us.get()
           .subscribe(
             data => {
                this.Tabla = data.usuarios;
                this.clear();
             },
             error =>{
               this._ls.handleError(error);
             });
}

showSuccess( header, mensaje ) {
   this.msjs = [];
   this.msjs.push({severity:'success', summary: header, detail: mensaje});
}

showInfo( header, mensaje ) {
   this.msjs = [];
   this.msjs.push({severity:'info', summary: header, detail: mensaje});
}

showWarn( header, mensaje ) {
   this.msjs = [];
   this.msjs.push({severity:'warn', summary: header, detail: mensaje});
}

showError( header, mensaje ) {
   this.msjs = [];
   this.msjs.push({severity:'error', summary: header, detail: mensaje});
}

clear() {
   this.msjs = [];
}

}

class ClonRegistro implements UsuarioInterface {
   constructor(
     public id?,
     public dni?,
     public  ap_paterno?,
     public  ap_materno?,
     public  nombres?,
     public  celular?,
     public  fijo?,
     public fecnac?,
     public est_civil?,
     public direccion?,
     public ubigeo?,
     public email_corp?,
     public email_per?,
     public contacto_emergencia?,
     public telef_contacto?,
     public login?,
     public idusuario_sede?,
     public idproveedor?,
     public idcartera?,
     public proveedor?,
     public cartera?,
     public fec_ingreso?,
     public perfil?,
     public turno?,
     public estado?
      // public id?, public dni?, public idubigeo?, public ap_paterno?, public ap_materno?,
      // public nombres?, public fecnac?, public est_civil?, public fec_ingreso?, public movil?,
      // public fijo?, public direccion?, public ubigeo?, public email_corp?, public email_per?, public user?,
      // public contacto_emergencia?, public telef_contacto?, public perfil?, public id_perfil?, public turno?, public distrito?
    ){}
}
