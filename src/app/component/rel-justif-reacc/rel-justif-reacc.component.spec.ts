import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RelJustifReaccComponent } from './rel-justif-reacc.component';

describe('RelJustifReaccComponent', () => {
  let component: RelJustifReaccComponent;
  let fixture: ComponentFixture<RelJustifReaccComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RelJustifReaccComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RelJustifReaccComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
