import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { NgForm, FormGroup, FormControl, Validators } from "@angular/forms";
import { Message, MenuItem, DataTable } from 'primeng/primeng';
import { GrowlModule } from 'primeng/primeng';
import { NgModule } from '@angular/core';
import { RelacionJustificacionReaccionInterface } from '../../interfaces/RelacionJustificacionReaccionInterface';
import { InputTextModule, DataTableModule, ButtonModule, DialogModule } from 'primeng/primeng';
import { CalendarModule } from 'primeng/primeng';

@Component({
  selector: 'app-rel-justif-reacc',
  templateUrl: './rel-justif-reacc.component.html',
  host: {'(window:keydown)': 'hotkeys($event)'}
})
export class RelJustifReaccComponent implements OnInit {

  hotkeys(event){
     // ALT + "+"
     if(event.altKey && event.keyCode==107){
        this.display_modal = true;
        this.limpiarForm();
     }
  }

  selectedRecord:Object;

  Formulario:FormGroup;
  can_edit = false;
  display_modal = false;
  accion:string = "crear";
  msjs:Message[] = [];

  objeto:RelacionJustificacionReaccionInterface = {
    id: "",
    fecha: "",
    resultado: "",
    justificacion: "",
    reaccion: "",
    descripcion: "",
    estado: "",
  }

  Tabla:any[] = [
    {
      id: "1",
      fecha: "2017-05-15",
      resultado: "Promesa de Pago",
      justificacion: "PDP Total",
      reaccion: "DFPG_TIT",
      descripcion: "",
      estado: "Activo",
    },
    {
      id: "2",
      fecha: "2017-07-15",
      resultado: "Promesa de Pago",
      justificacion: "PDP Total",
      reaccion: "DFPG_TIT",
      descripcion: "",
      estado: "Activo",
    },
    {
      id: "3",
      fecha: "2017-07-20",
      resultado: "Promesa de Pago",
      justificacion: "PDP Total",
      reaccion: "DFPG_TIT",
      descripcion: "",
      estado: "Activo",
    }
  ];

  constructor(private router:Router,
              private cdRef:ChangeDetectorRef) {

    this.Formulario = new FormGroup({
      'id': new FormControl(this.objeto.id),
      'fecha': new FormControl(this.objeto.fecha),
      'resultado': new FormControl(this.objeto.resultado, Validators.required),
      'justificacion': new FormControl(this.objeto.justificacion, Validators.required),
      'reaccion': new FormControl(this.objeto.reaccion, Validators.required),
      'descripcion': new FormControl(this.objeto.descripcion),
      'estado': new FormControl(this.objeto.estado, Validators.required)
    });
  };

  Edit(event){
    console.log(event.data);
     this.can_edit = true
     if(event.originalEvent.type=="dblclick"){
        this.accion = "editar";
        let obj = this.Clonar(event.data);
        this.Formulario.setValue(obj);
        this.display_modal = true;
     }
  }

  editarModal(dt:DataTable){
     this.accion = "editar";
     let obj = this.Clonar(dt.selection);
     this.Formulario.setValue(obj);
     this.display_modal = true;
  }

  Clonar(r: RelacionJustificacionReaccionInterface){
       let objeto = new ClonRegistro();
       for(let prop in r){
         if(this.objeto.hasOwnProperty(prop)){
           objeto[prop] = r[prop];
         }
       }
       return objeto;
    }

  ngOnInit() {
  }

  crearModal(){
     this.display_modal = true;
     this.limpiarForm();
  }

  limpiarForm(){
    this.Formulario.reset({
      id: "",
      fecha: "",
      resultado: "",
      justificacion: "",
      reaccion: "",
      descripcion: "",
      estado: "",
    });
   setTimeout(()=>{
      document.getElementById("id").focus();
   },500);
  };

  showSuccess( header, mensaje ) {
     this.msjs = [];
     this.msjs.push({severity:'success', summary: header, detail: mensaje});
  }

  showInfo( header, mensaje ) {
     this.msjs = [];
     this.msjs.push({severity:'info', summary: header, detail: mensaje});
  }

  showWarn( header, mensaje ) {
     this.msjs = [];
     this.msjs.push({severity:'warn', summary: header, detail: mensaje});
  }

  showError( header, mensaje ) {
     this.msjs = [];
     this.msjs.push({severity:'error', summary: header, detail: mensaje});
  }

  clear() {
     this.msjs = [];
  }

  changeAccion(){

  }

  }

  class ClonRegistro implements RelacionJustificacionReaccionInterface {
  constructor(
              public id?,
              public fecha?,
              public resultado?,
              public justificacion?,
              public reaccion?,
              public descripcion?,
              public estado?){}
  }
