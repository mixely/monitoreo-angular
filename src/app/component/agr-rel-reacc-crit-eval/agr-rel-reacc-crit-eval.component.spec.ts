import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgrRelReaccCritEvalComponent } from './agr-rel-reacc-crit-eval.component';

describe('AgrRelReaccCritEvalComponent', () => {
  let component: AgrRelReaccCritEvalComponent;
  let fixture: ComponentFixture<AgrRelReaccCritEvalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgrRelReaccCritEvalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgrRelReaccCritEvalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
