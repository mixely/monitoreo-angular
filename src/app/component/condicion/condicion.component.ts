import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { NgForm, FormGroup, FormControl, Validators } from "@angular/forms";
import { Message, MenuItem, DataTable } from 'primeng/primeng';
import { GrowlModule } from 'primeng/primeng';
import { NgModule } from '@angular/core';
import { CondicionInterface } from '../../interfaces/CondicionInterface';
import { InputTextModule, DataTableModule, ButtonModule, DialogModule } from 'primeng/primeng';
import { CalendarModule } from 'primeng/primeng';

@Component({
  selector: 'app-condicion',
  templateUrl: './condicion.component.html',
  host: {'(window:keydown)': 'hotkeys($event)'}
})

export class CondicionComponent implements OnInit {

  hotkeys(event){
     // ALT + "+"
     if(event.altKey && event.keyCode==107){
        this.display_modal = true;
        this.limpiarForm();
     }
  }

  Formulario:FormGroup;
  can_edit = false;
  display_modal = false;
  accion:string = "crear";
  msjs:Message[] = [];

  objeto:CondicionInterface = {
    id: "",
    fecha : "",
    tipo_de_cartera: "",
    condicion: "",
    empresa: "",
    cartera: "",
    nro_de_evaluaciones: "",
    nro_de_audios: "",
    intervalo_de_dias: "",
    estado: "",
  }

  Tabla:any[] = [
    {
      id: "1",
      fecha: "2017-06-01",
      tipo_de_cartera: "Recovery",
      condicion: "Antiguo",
      empresa: "> 3 meses",
      cartera: "> 1 mes",
      nro_de_evaluaciones: "2",
      nro_de_audios: "3",
      intervalo_de_dias: "15",
      estado: "Activo",
    },
    {
      id: "1",
      fecha: "2017-07-01",
      tipo_de_cartera: "Preventiva",
      condicion: "Nuevo",
      empresa: "< 3 meses",
      cartera: "> 1 mes",
      nro_de_evaluaciones: "2",
      nro_de_audios: "3",
      intervalo_de_dias: "15",
      estado: "Activo",
    },
    {
      id: "1",
      fecha: "2017-07-01",
      tipo_de_cartera: "Recovery",
      condicion: "Antiguo",
      empresa: "> 3 meses",
      cartera: "> 1 mes",
      nro_de_evaluaciones: "2",
      nro_de_audios: "3",
      intervalo_de_dias: "15",
      estado: "Activo",
    }
  ];

  constructor(private router:Router,
              private cdRef:ChangeDetectorRef) {

    this.Formulario = new FormGroup({
      'id': new FormControl(this.objeto.id),
      'fecha': new FormControl(this.objeto.fecha),
      'tipo_de_cartera': new FormControl(this.objeto.tipo_de_cartera, Validators.required),
      'condicion': new FormControl(this.objeto.condicion, Validators.required),
      'empresa': new FormControl(this.objeto.empresa, Validators.required),
      'cartera': new FormControl(this.objeto.cartera, Validators.required),
      'nro_de_evaluaciones': new FormControl(this.objeto.nro_de_evaluaciones, Validators.required),
      'nro_de_audios': new FormControl(this.objeto.nro_de_audios, Validators.required),
      'intervalo_de_dias': new FormControl(this.objeto.intervalo_de_dias, Validators.required),
      'estado': new FormControl(this.objeto.estado, Validators.required)

    });
};

  Edit(event){
    console.log(event.data);
     this.can_edit = true
     if(event.originalEvent.type=="dblclick"){
        this.accion = "editar";
        let obj = this.Clonar(event.data);
        this.Formulario.setValue(obj);
        this.display_modal = true;
     }
  }

  editarModal(dt:DataTable){
     this.accion = "editar";
     let obj = this.Clonar(dt.selection);
     // this.usuario["distrito"] = [...this.usuario["distrito"], obj.distrito]
     this.Formulario.setValue(obj);
     this.display_modal = true;
  }

  Clonar(r: CondicionInterface){
       let objeto = new ClonRegistro();
       for(let prop in r){
         if(this.objeto.hasOwnProperty(prop)){
           objeto[prop] = r[prop];
         }
       }
       return objeto;
    }

  ngOnInit() {
  }

  crearModal(){
     this.display_modal = true;
     this.limpiarForm();
  }

  limpiarForm(){
    this.Formulario.reset({
      id: "",
      fecha : "",
      tipo_de_cartera: "",
      condicion: "",
      empresa: "",
      cartera: "",
      nro_de_evaluaciones: "",
      nro_de_audios: "",
      intervalo_de_dias: "",
      estado: "",
    });
   setTimeout(()=>{
      document.getElementById("id").focus();
   },500);
};

  showSuccess( header, mensaje ) {
     this.msjs = [];
     this.msjs.push({severity:'success', summary: header, detail: mensaje});
  }

  showInfo( header, mensaje ) {
     this.msjs = [];
     this.msjs.push({severity:'info', summary: header, detail: mensaje});
  }

  showWarn( header, mensaje ) {
     this.msjs = [];
     this.msjs.push({severity:'warn', summary: header, detail: mensaje});
  }

  showError( header, mensaje ) {
     this.msjs = [];
     this.msjs.push({severity:'error', summary: header, detail: mensaje});
  }

  clear() {
     this.msjs = [];
  }

  changeAccion(){

  }

}

class ClonRegistro implements CondicionInterface {
  constructor(
              public id?,
              public fecha?,
              public tipo_de_cartera?,
              public condicion?,
              public empresa?,
              public cartera?,
              public nro_de_evaluaciones?,
              public nro_de_audios?,
              public intervalo_de_dias?,
              public estado?){}
}
