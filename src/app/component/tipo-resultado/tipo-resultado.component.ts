import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { NgForm, FormGroup, FormControl, Validators } from "@angular/forms";

import { TipoResultadoService } from '../../services/tipo-resultado.service';
import { LoginService } from '../../services/login.service';

import { TipoResultadoInterface } from '../../interfaces/tipo-resultado.interface';
import { Message, MenuItem, DataTable } from 'primeng/primeng';

@Component({
  selector: 'app-tipo-resultado',
  templateUrl: './tipo-resultado.component.html',
  host: {'(window:keydown)': 'hotkeys($event)'}
})
export class TipoResultadoComponent implements OnInit {

  Formulario:FormGroup;

  display_modal:boolean = false;
  accion:string = "crear";
  msjs:Message[] = [];

  selectedRecord:TipoResultadoInterface;
  Tabla:TipoResultadoInterface[] = [];
  csv:boolean = true; // Si es "true" entonces va a desahabilitar
  can_edit = false;

  constructor(private cdRef:ChangeDetectorRef,
              private _ls:LoginService,
              private _trs:TipoResultadoService
              ) {
    this.Formulario = new FormGroup({
      'id': new FormControl(this.objeto.id),
      'tipo_resultado': new FormControl(this.objeto.tipo_resultado, Validators.required),
      'peso': new FormControl(this.objeto.peso, Validators.required),
      'estado': new FormControl(this.objeto.estado)
    });
  }

  // host: {'(window:keydown)': 'hotkeys($event)'}
  hotkeys(event){
    // ALT + C
    if(event.altKey && event.keyCode==107){
       this.display_modal = true;
    }
  }

  objeto:TipoResultadoInterface = {
    id:"",
    tipo_resultado:"",
    peso:"",
    estado:""
  }

  limpiarForm(){
    this.Formulario.reset({
      id:"",
      tipo_resultado:"",
      peso:"",
      estado:""
    });

    setTimeout(()=>{
      document.getElementById("tipo_resultado").focus();
    },500);
  };

  ngOnInit() {
    this.getTipoResultados();
  }

  ngAfterViewChecked(){
   this.cdRef.detectChanges();
  }

  crearModal(){
    this.display_modal = true;
    this.limpiarForm();
  }

  generarCSV(dt:DataTable){
    dt.exportCSV();
  }

  editarModal(dt:DataTable){
     this.accion = "editar";
     let obj = this.Clonar(dt.selection);
     // this.objeto["distrito"] = [...this.objeto["distrito"], obj.distrito]
     this.Formulario.setValue(obj);
     this.display_modal = true;
  }

  Edit(event){
     this.can_edit = true
     if(event.originalEvent.type=="dblclick"){
        this.accion = "editar";
        let obj = this.Clonar(event.data);
        this.Formulario.setValue(obj);
        this.display_modal = true;
     }
  }

  Selected(obj){
     this.objeto.id = obj.id;
  }
  changeAccion(){
   this.accion = "crear";
  }

Clonar(r: TipoResultadoInterface){
   let objeto = new ClonRegistro();
   for(let prop in r){
     if(this.objeto.hasOwnProperty(prop)){
       objeto[prop] = r[prop];
     }
   }
   return objeto;
}

Crear(){
   this._trs.setTipoResultado( this.objeto )
   .subscribe(
      data =>{
        this.Tabla = [];
        for (let i = 0; i < data.datos.length; i++) {
          this.Tabla = [...this.Tabla, data.datos[i]];
        }
        this.display_modal = false;
        if(this.Tabla.length>0){
          this.csv = false; // Entonces habilitamos el botón
        }
        this.showSuccess('Exito', "Hemos añadido el tipo de resultado al sistema");
        },
      error =>{
        this._ls.handleError(error);
      });
}

Editar(){
  this.display_modal = false;
   this._trs.updateTipoResultado( this.objeto )
           .subscribe(
             data => {
               this.showSuccess("Exito", "Hemos actualizado el tipo de resultado");
               this.getTipoResultados();
             },
             error =>{
              this._ls.handleError(error);
             });
}

getTipoResultados(){
   this.showWarn( "Espere", "Estamos obteniendo los datos" );
   this._trs.getTipoResultados()
           .subscribe(
             data => {
                this.Tabla = [];
                this.Tabla = data.datos;
                this.can_edit = false;
             },
             error =>{
              this._ls.handleError(error);
             });
}

showSuccess( header, mensaje ) {
   this.msjs.push({severity:'success', summary: header, detail: mensaje});
   setTimeout(()=> this.clear(), 3000);
}

showInfo( header, mensaje ) {
   this.msjs.push({severity:'info', summary: header, detail: mensaje});
   setTimeout(()=> this.clear(), 3000);
}

showWarn( header, mensaje ) {
   this.msjs.push({severity:'warn', summary: header, detail: mensaje});
   setTimeout(()=> this.clear(), 3000);
}

showError( header, mensaje ) {
   this.msjs.push({severity:'error', summary: header, detail: mensaje});
   setTimeout(()=> this.clear(), 3000);
}

clear() {
   this.msjs = [];
}

}

class ClonRegistro implements TipoResultadoInterface {
   constructor(
     public id?,
     public tipo_resultado?,
     public peso?,
     public estado?){}
}
