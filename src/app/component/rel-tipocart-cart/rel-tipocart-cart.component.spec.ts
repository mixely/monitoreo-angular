import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RelTipocartCartComponent } from './rel-tipocart-cart.component';

describe('RelTipocartCartComponent', () => {
  let component: RelTipocartCartComponent;
  let fixture: ComponentFixture<RelTipocartCartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RelTipocartCartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RelTipocartCartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
