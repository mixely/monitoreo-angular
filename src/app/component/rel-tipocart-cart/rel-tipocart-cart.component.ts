import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { NgForm, FormGroup, FormControl, Validators } from "@angular/forms";
import { Message, MenuItem, DataTable } from 'primeng/primeng';
import { GrowlModule } from 'primeng/primeng';
import { NgModule } from '@angular/core';
import { RelacionCarteraTipoDeCarteraInterface } from '../../interfaces/RelacionCarteraTipoDeCarteraInterface';
import { InputTextModule, DataTableModule, ButtonModule, DialogModule} from 'primeng/primeng';
import {CalendarModule} from 'primeng/primeng';


@Component({
  selector: 'app-rel-tipocart-cart',
  templateUrl: './rel-tipocart-cart.component.html',
  host: {'(window:keydown)': 'hotkeys($event)'}
})
export class RelTipocartCartComponent implements OnInit {

  hotkeys(event){
     // ALT + "+"
     if(event.altKey && event.keyCode==107){
        this.display_modal = true;
        this.limpiarForm();
     }
  }

  Formulario:FormGroup;
  can_edit = false;
  display_modal = false;
  accion:string = "crear";
  msjs:Message[] = [];

  objeto:RelacionCarteraTipoDeCarteraInterface = {
    id: "",
    fecha: "",
    tipo_de_cartera: "",
    proveedor: "",
    cartera: "",
    descripcion: "",
    estado: "",
  }

  Tabla:any[] = [
    {
      id: "1",
      fecha: "2017-07-01",
      tipo_de_cartera: "Castigo",
      proveedor: "SCI",
      cartera: "Tardia",
      descripcion: "",
      estado: "Activo"
    },
    {
      id: "2",
      fecha: "2017-07-01",
      tipo_de_cartera: "Castigo",
      proveedor: "SCI",
      cartera: "Tardia",
      descripcion: "",
      estado: "Activo"
    },
    {
      id: "3",
      fecha: "2017-07-01",
      tipo_de_cartera: "Castigo",
      proveedor: "Banco Interbank",
      cartera: "Tardia",
      descripcion: "",
      estado: "Inactivo"
    }
  ];

  constructor(private router:Router,
              private cdRef:ChangeDetectorRef) {

    this.Formulario = new FormGroup({
      'id': new FormControl(this.objeto.id),
      'fecha': new FormControl(this.objeto.fecha),
      'tipo_de_cartera': new FormControl(this.objeto.tipo_de_cartera, Validators.required),
      'proveedor': new FormControl(this.objeto.proveedor, Validators.required),
      'cartera': new FormControl(this.objeto.cartera, Validators.required),
      'descripcion': new FormControl(this.objeto.descripcion),
      'estado': new FormControl(this.objeto.estado, Validators.required)
    });
  };

  Edit(event){
    console.log(event.data);
     this.can_edit = true
     if(event.originalEvent.type=="dblclick"){
        this.accion = "editar";
        let obj = this.Clonar(event.data);
        this.Formulario.setValue(obj);
        this.display_modal = true;
     }
  }

  editarModal(dt:DataTable){
     this.accion = "editar";
     let obj = this.Clonar(dt.selection);
     this.Formulario.setValue(obj);
     this.display_modal = true;
  }

  Clonar(r: RelacionCarteraTipoDeCarteraInterface){
       let objeto = new ClonRegistro();
       for(let prop in r){
         if(this.objeto.hasOwnProperty(prop)){
           objeto[prop] = r[prop];
         }
       }
       return objeto;
    }

  ngOnInit() {
  }

  crearModal(){
     this.display_modal = true;
     this.limpiarForm();
  }

  limpiarForm(){
    this.Formulario.reset({
      id: "",
      fecha: "",
      tipo_de_cartera: "",
      proveedor: "",
      cartera: "",
      descripcion: "",
      estado: "",
    });
   setTimeout(()=>{
      document.getElementById("id").focus();
   },500);
  };

  showSuccess( header, mensaje ) {
     this.msjs = [];
     this.msjs.push({severity:'success', summary: header, detail: mensaje});
  }

  showInfo( header, mensaje ) {
     this.msjs = [];
     this.msjs.push({severity:'info', summary: header, detail: mensaje});
  }

  showWarn( header, mensaje ) {
     this.msjs = [];
     this.msjs.push({severity:'warn', summary: header, detail: mensaje});
  }

  showError( header, mensaje ) {
     this.msjs = [];
     this.msjs.push({severity:'error', summary: header, detail: mensaje});
  }

  clear() {
     this.msjs = [];
  }

  changeAccion(){

  }

  }

  class ClonRegistro implements RelacionCarteraTipoDeCarteraInterface {
  constructor(
              public id?,
              public fecha?,
              public tipo_de_cartera?,
              public proveedor?,
              public cartera?,
              public descripcion?,
              public estado?){}
  }
