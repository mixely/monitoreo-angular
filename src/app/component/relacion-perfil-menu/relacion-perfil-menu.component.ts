import { Component, OnInit } from '@angular/core';
import { NgForm, FormGroup, FormControl, Validators } from "@angular/forms";

@Component({
  selector: 'app-relacion-perfil-menu',
  templateUrl: './relacion-perfil-menu.component.html',
  host: {'(window:keydown)': 'hotkeys($event)'}
})
export class RelacionPerfilMenuComponent implements OnInit {
  // host: {'(window:keydown)': 'hotkeys($event)'}
  hotkeys(event){
    // ALT + C
    if(event.altKey && event.keyCode==67){
       this.display_modal = true;
    }
  }

  relPerfilMenu:Object = {
    idPerfil: "",
    idMenu: ""
  }

  Perfiles:any[] = [];

  frmRelPerfilMenu:FormGroup;

  display_modal:boolean = false;

  constructor() {
    this.frmRelPerfilMenu = new FormGroup({
      'idPerfil': new FormControl('', Validators.required),
      'idMenu': new FormControl('', Validators.required)
    });
  }

  ngOnInit() {
  }

  limpiarForm(){
    this.frmRelPerfilMenu.reset( this.relPerfilMenu );
    setTimeout(()=>{
      document.getElementById("relPerfilMenu").focus();
    },500);
  }

  asignarPerfilMenu(){

  }
}
