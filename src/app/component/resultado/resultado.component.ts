import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { NgForm, FormGroup, FormControl, Validators } from "@angular/forms";

import { ResultadoService } from '../../services/resultado.service';
import { TipoResultadoService } from '../../services/tipo-resultado.service'
import { LoginService } from '../../services/login.service';

import { ResultadoInterface } from '../../interfaces/resultado.interface';
import { Message, MenuItem, DataTable } from 'primeng/primeng';

@Component({
  selector: 'app-resultado',
  templateUrl: './resultado.component.html',
  host: {'(window:keydown)': 'hotkeys($event)'}
})
export class ResultadoComponent implements OnInit {

  Formulario:FormGroup;

  display_modal:boolean = false;
  accion:string = "crear";
  msjs:Message[] = [];
  tipo_resultados:any[] = [];

  selectedRecord:ResultadoInterface;
  Tabla:ResultadoInterface[] = [];
  csv:boolean = true; // Si es "true" entonces va a desahabilitar

  can_edit = false;

  constructor(private cdRef:ChangeDetectorRef,
              private _trs:TipoResultadoService,
              private _ls:LoginService,
              private _rs:ResultadoService
              ) {
    this.Formulario = new FormGroup({
      'id': new FormControl(this.objeto.id),
      'resultado': new FormControl(this.objeto.resultado, Validators.required),
      'id_tipo_resultado': new FormControl(this.objeto.id_tipo_resultado, Validators.required)
    });
  }

  // host: {'(window:keydown)': 'hotkeys($event)'}
  hotkeys(event){
    // ALT + C
    if(event.altKey && event.keyCode==107){
       this.display_modal = true;
    }
  }

  objeto:ResultadoInterface = {
    id:"",
    resultado:"",
    id_tipo_resultado:""
  }

  limpiarForm(){
    this.Formulario.reset({
      id:"",
      resultado:"",
      id_tipo_resultado:""
    });

    setTimeout(()=>{
      document.getElementById("resultado").focus();
    },500);
  };

  ngOnInit() {
    this.get();
    // traemos los tipos de resultados
    this._trs.getTipoResultados()
             .subscribe(
                data =>{
                  this.tipo_resultados = data.datos;
                },
                error =>{
                  this._ls.handleError(error);
                });
  }

  ngAfterViewChecked(){
   this.cdRef.detectChanges();
  }

  crearModal(){
    this.display_modal = true;
    this.limpiarForm();
  }

  generarCSV(dt:DataTable){
    dt.exportCSV();
  }

  editarModal(dt:DataTable){
     this.accion = "editar";
     let obj = this.Clonar(dt.selection);
     // this.objeto["distrito"] = [...this.objeto["distrito"], obj.distrito]
     this.Formulario.setValue(obj);
     this.display_modal = true;
  }

  Edit(event){
     this.can_edit = true
     if(event.originalEvent.type=="dblclick"){
        this.accion = "editar";
        let obj = this.Clonar(event.data);
        this.Formulario.setValue(obj);
        this.display_modal = true;
     }
  }

  Selected(obj){
     this.objeto.id = obj.id;
  }
  changeAccion(){
   this.accion = "crear";
}

Clonar(r: ResultadoInterface){
   let objeto = new ClonRegistro();
   for(let prop in r){
     if(this.objeto.hasOwnProperty(prop)){
       objeto[prop] = r[prop];
     }
   }
   return objeto;
}

Crear(){
   console.log(this.objeto)
   this._rs.set( this.objeto )
   .subscribe(
     data =>{
       this.Tabla = [];
        for (let i = 0; i < data.datos.length; i++) {
           this.Tabla = [...this.Tabla, data.datos[i]];
        }
        this.display_modal = false;
        if(this.Tabla.length>0){
          this.csv = false; // Entonces habilitamos el botón
        }
        this.showSuccess('Exito', "Hemos añadido el tipo de resultado al sistema");
     },
     error =>{
       this._ls.handleError(error);
     });
}

Editar(){
  this.display_modal = false;
   this._rs.update( this.objeto )
           .subscribe(
             data => {
               this.showSuccess("Exito", "Hemos actualizado el tipo de resultado");
               this.get();
             },
             error =>{
               this._ls.handleError(error);
             });
}

get(){
   this.showWarn( "Espere", "Estamos obteniendo los datos" );
   this._rs.get()
           .subscribe(
             data => {
                this.Tabla = [];
                this.Tabla = data.datos;
                this.can_edit = false;
             },
             error =>{
               this._ls.handleError(error);
             });
}

showSuccess( header, mensaje ) {
   this.msjs.push({severity:'success', summary: header, detail: mensaje});
   setTimeout(()=> this.clear(), 3000);
}

showInfo( header, mensaje ) {
   this.msjs.push({severity:'info', summary: header, detail: mensaje});
   setTimeout(()=> this.clear(), 3000);
}

showWarn( header, mensaje ) {
   this.msjs.push({severity:'warn', summary: header, detail: mensaje});
   setTimeout(()=> this.clear(), 3000);
}

showError( header, mensaje ) {
   this.msjs.push({severity:'error', summary: header, detail: mensaje});
   setTimeout(()=> this.clear(), 3000);
}

clear() {
   this.msjs = [];
}

}

class ClonRegistro implements ResultadoInterface {
   constructor(
     public id?,
     public resultado?,
     public id_tipo_resultado?){}
}
