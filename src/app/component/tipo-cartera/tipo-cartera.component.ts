import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { NgForm, FormGroup, FormControl, Validators } from "@angular/forms";

import { CarteraService } from '../../services/cartera.service';
import { LoginService } from '../../services/login.service';

import { TipoCarteraInterface } from '../../interfaces/tipo-cartera.interface';
import { Message, MenuItem, DataTable } from 'primeng/primeng';

@Component({
  selector: 'app-tipo-cartera',
  templateUrl: './tipo-cartera.component.html',
  host: {'(window:keydown)': 'hotkeys($event)'}
})
export class TipoCarteraComponent implements OnInit {

  Formulario:FormGroup;

  display_modal:boolean = false;
  accion:string = "crear";
  msjs:Message[] = [];

  selectedRecord:TipoCarteraInterface;
  Tabla:TipoCarteraInterface[] = [];
  csv:boolean = true; // Si es "true" entonces va a desahabilitar

  can_edit = false;

  constructor(private cdRef:ChangeDetectorRef,
              private _ls:LoginService,
              private _cs:CarteraService,
              ) {
    this.Formulario = new FormGroup({
      'id': new FormControl(this.objeto.id),
      'tipo_cartera': new FormControl(this.objeto.tipo_cartera, Validators.required),
      'estado': new FormControl(this.objeto.estado)
    });
  }

  // host: {'(window:keydown)': 'hotkeys($event)'}
  hotkeys(event){
    // ALT + C
    if(event.altKey && event.keyCode==107){
       this.display_modal = true;
    }
  }

  callingEnter(event){
    if(event.key=="Enter"){
      if(this.accion=="crear"){
        this.Crear();
      }else{
        this.Editar();
      }
    }
  }

  objeto:TipoCarteraInterface = {
    id:"",
    tipo_cartera:"",
    estado:""
  }

  limpiarForm(){
    this.Formulario.reset({
      id:"",
      tipo_cartera:"",
      estado:""
    });

    setTimeout(()=>{
      document.getElementById("tipo_cartera").focus();
    },500);
  };

  ngOnInit() {
    this.getTiposCartera();
  }

  ngAfterViewChecked(){
   this.cdRef.detectChanges();
  }

  crearModal(){
    this.display_modal = true;
    this.limpiarForm();
  }

  generarCSV(dt:DataTable){
    dt.exportCSV();
  }

  editarModal(dt:DataTable){
     this.accion = "editar";
     let obj = this.Clonar(dt.selection);
     // this.objeto["distrito"] = [...this.objeto["distrito"], obj.distrito]
     this.Formulario.setValue(obj);
     this.display_modal = true;
  }

  Edit(event){
     this.can_edit = true
     if(event.originalEvent.type=="dblclick"){
        this.accion = "editar";
        let obj = this.Clonar(event.data);
        this.Formulario.setValue(obj);
        this.display_modal = true;
     }
  }

  Selected(obj){
     this.objeto.id = obj.id;
  }
  changeAccion(){
   this.accion = "crear";
  }

Clonar(r: TipoCarteraInterface){
   let objeto = new ClonRegistro();
   for(let prop in r){
     if(this.objeto.hasOwnProperty(prop)){
       objeto[prop] = r[prop];
     }
   }
   return objeto;
}

Crear(){
  this.display_modal = false;
   this._cs.setTipoCartera( this.objeto )
   .subscribe(
     data => {
       this.Tabla = [];
        for (let i = 0; i < data.datos.length; i++) {
           this.Tabla = [...this.Tabla, data.datos[i]];
        }
        if(this.Tabla.length>0){
          this.csv = false; // Entonces habilitamos el botón
        }
        this.showSuccess('Exito', "Hemos añadido el rubro al sistema");
     },
     error =>{
        this._ls.handleError(error);
     });
}

Editar(){
  this.display_modal = false;
  this._cs.updateTipoCartera( this.objeto )
           .subscribe(
             data => {
               this.showSuccess("Exito", "Hemos actualizado el rubro");
               this.getTiposCartera();
             },
             error =>{
                this._ls.handleError(error);
              });
}

getTiposCartera(){
   this.showWarn( "Espere", "Estamos obteniendo las carteras" );
   this._cs.cargarTiposCartera()
           .subscribe(
             data => {
                this.Tabla = [];
                this.Tabla = data.datos;
                this.can_edit = false;
             },
             error =>{
                this._ls.handleError(error);
             });
}

showSuccess( header, mensaje ) {
   this.msjs.push({severity:'success', summary: header, detail: mensaje});
   setTimeout(()=> this.clear(), 2000);
}

showInfo( header, mensaje ) {
   this.msjs.push({severity:'info', summary: header, detail: mensaje});
   setTimeout(()=> this.clear(), 2000);
}

showWarn( header, mensaje ) {
   this.msjs.push({severity:'warn', summary: header, detail: mensaje});
   setTimeout(()=> this.clear(), 2000);
}

showError( header, mensaje ) {
   this.msjs.push({severity:'error', summary: header, detail: mensaje});
   setTimeout(()=> this.clear(), 2000);
}

clear() {
   this.msjs = [];
}

}

class ClonRegistro implements TipoCarteraInterface {
   constructor(
     public id?,
     public tipo_cartera?,
     public estado?
   ){}
}
