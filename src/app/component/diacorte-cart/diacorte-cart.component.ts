import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { NgForm, FormGroup, FormControl, Validators } from "@angular/forms";
import { Message, MenuItem, DataTable } from 'primeng/primeng';
import { GrowlModule } from 'primeng/primeng';
import { NgModule } from '@angular/core';
import { CondicionInterface } from '../../interfaces/CondicionInterface';
import { DiadeCorteporCarteraInterface } from '../../interfaces/DiadeCorteporCarteraInterface';
import { TipoCarteraInterface } from '../../interfaces/TipoCarteraInterface';
import { InputTextModule, DataTableModule, ButtonModule, DialogModule } from 'primeng/primeng';
import { CalendarModule } from 'primeng/primeng';

import { DiaCorteService } from '../../services/DiaCorte.service';

@Component({
  selector: 'app-diacorte-cart',
  templateUrl: './diacorte-cart.component.html',
  host: {'(window:keydown)': 'hotkeys($event)'}
})
export class DiacorteCartComponent implements OnInit {

    hotkeys(event){
       // ALT + "+"
       if(event.altKey && event.keyCode==107){
          this.display_modal = true;
          this.limpiarForm();
       }
    }

    Formulario:FormGroup;
    can_edit = false;
    display_modal = false;
    accion:string = "crear";
    msjs:Message[] = [];

    objeto:DiadeCorteporCarteraInterface = {
      id: "",
      // fecha: "",
      tipo_cartera: "",
      proveedor: "",
      cartera:"",
      dia_de_corte: "",
      estado: "",
    }

    Tabla:any[] = [];

    constructor(private router:Router,
                private _dcs:DiaCorteService,
                private cdRef:ChangeDetectorRef) {

      this.Formulario = new FormGroup({
        'id': new FormControl(this.objeto.id),
        'tipo_cartera': new FormControl(this.objeto.tipo_cartera, Validators.required),
        'proveedor': new FormControl(this.objeto.proveedor, Validators.required),
        'cartera': new FormControl(this.objeto.cartera, Validators.required),
        'dia_de_corte': new FormControl(this.objeto.dia_de_corte, Validators.required),
        'estado': new FormControl(this.objeto.estado, Validators.required)
      });
  };

    Edit(event){
      console.log(event.data);
       this.can_edit = true
       if(event.originalEvent.type=="dblclick"){
          this.accion = "editar";
          let obj = this.Clonar(event.data);
          this.Formulario.setValue(obj);
          this.display_modal = true;
       }
    }

    editarModal(dt:DataTable){
       this.accion = "editar";
       let obj = this.Clonar(dt.selection);
       this.Formulario.setValue(obj);
       this.display_modal = true;
    }

    Clonar(r: CondicionInterface){
         let objeto = new ClonRegistro();
         for(let prop in r){
           if(this.objeto.hasOwnProperty(prop)){
             objeto[prop] = r[prop];
           }
         }
         return objeto;
      }

    ngOnInit() {
    //   this._dcs.get()
    //            .subscribe(data => {
    //              this.Tabla = data.datos;
    //            })
    }

    crearModal(){
       this.display_modal = true;
       this.limpiarForm();
    }

    limpiarForm(){
      this.Formulario.reset({
        id: "",
        tipo_cartera: "",
        proveedor: "",
        cartera: "",
        dia_de_corte: "",
        estado: "",
      });
     setTimeout(()=>{
        document.getElementById("id").focus();
     },500);
  };

    showSuccess( header, mensaje ) {
       this.msjs = [];
       this.msjs.push({severity:'success', summary: header, detail: mensaje});
    }

    showInfo( header, mensaje ) {
       this.msjs = [];
       this.msjs.push({severity:'info', summary: header, detail: mensaje});
    }

    showWarn( header, mensaje ) {
       this.msjs = [];
       this.msjs.push({severity:'warn', summary: header, detail: mensaje});
    }

    showError( header, mensaje ) {
       this.msjs = [];
       this.msjs.push({severity:'error', summary: header, detail: mensaje});
    }

    clear() {
       this.msjs = [];
    }

    changeAccion(){
    }

  }

  class ClonRegistro implements DiadeCorteporCarteraInterface {
    constructor(
                public id?,
                public tipo_cartera?,
                public proveedor?,
                public cartera?,
                public dia_de_corte?,
                public estado?){}
  }
