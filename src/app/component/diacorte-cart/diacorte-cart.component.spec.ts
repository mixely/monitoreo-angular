import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DiacorteCartComponent } from './diacorte-cart.component';

describe('DiacorteCartComponent', () => {
  let component: DiacorteCartComponent;
  let fixture: ComponentFixture<DiacorteCartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DiacorteCartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DiacorteCartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
