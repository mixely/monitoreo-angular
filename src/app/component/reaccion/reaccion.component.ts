import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { NgForm, FormGroup, FormControl, Validators } from "@angular/forms";
import { Message, MenuItem, DataTable } from 'primeng/primeng';
import { GrowlModule } from 'primeng/primeng';
import { NgModule } from '@angular/core';
import { ReaccionInterface } from '../../interfaces/ReaccionInterface';
import { InputTextModule, DataTableModule, ButtonModule, DialogModule} from 'primeng/primeng';
import {CalendarModule} from 'primeng/primeng';

@Component({
  selector: 'app-reaccion',
  templateUrl: './reaccion.component.html',
  host: {'(window:keydown)': 'hotkeys($event)'}
})
export class ReaccionComponent implements OnInit {

  hotkeys(event){
     // ALT + "+"
     if(event.altKey && event.keyCode==107){
        this.display_modal = true;
        this.limpiarForm();
     }
  }

  Formulario:FormGroup;
  can_edit = false;
  display_modal = false;
  accion:string = "crear";
  msjs:Message[] = [];

  objeto:ReaccionInterface = {
    id: "",
    fecha: "",
    nombre: "",
    porcent_de_aprobacion: "",
    descripcion: "",
    estado: ""
  }

  Tabla:any[] = [
    {
      id: "1",
      fecha: "2017-07-01",
      nombre: "Ripley",
      porcent_de_aprobacion: "70%",
      descripcion: "",
      estado: "Activo"
    },
    {
      id: "2",
      fecha: "2017-07-01",
      nombre: "Claro",
      porcent_de_aprobacion: "80%",
      descripcion: "",
      estado: "Activo"
    },
    {
      id: "3",
      fecha: "2017-07-01",
      nombre: "Clt",
      porcent_de_aprobacion: "70%",
      descripcion: "Multicartera",
      estado: "Inactivo"
    }
  ];

  constructor(private router:Router,
              private cdRef:ChangeDetectorRef) {

    this.Formulario = new FormGroup({
      'id': new FormControl(this.objeto.id),
      'fecha': new FormControl(this.objeto.fecha),
      'nombre': new FormControl(this.objeto.nombre, Validators.required),
      'porcent_de_aprobacion': new FormControl(this.objeto.porcent_de_aprobacion, Validators.required),
      'descripcion': new FormControl(this.objeto.descripcion),
      'estado': new FormControl(this.objeto.estado, Validators.required)
    });
};

  Edit(event){
    console.log(event.data);
     this.can_edit = true
     if(event.originalEvent.type=="dblclick"){
        this.accion = "editar";
        let obj = this.Clonar(event.data);
        this.Formulario.setValue(obj);
        this.display_modal = true;
     }
  }

  editarModal(dt:DataTable){
     this.accion = "editar";
     let obj = this.Clonar(dt.selection);
     this.Formulario.setValue(obj);
     this.display_modal = true;
  }

  Clonar(r: ReaccionInterface){
       let objeto = new ClonRegistro();
       for(let prop in r){
         if(this.objeto.hasOwnProperty(prop)){
           objeto[prop] = r[prop];
         }
       }
       return objeto;
    }

  ngOnInit() {
  }

  crearModal(){
     this.display_modal = true;
     this.limpiarForm();
  }

  limpiarForm(){
    this.Formulario.reset({
      id: "",
      fecha: "",
      nombre: "",
      porcent_de_aprobacion: "",
      descripcion: "",
      estado: ""
    });
   setTimeout(()=>{
      document.getElementById("id").focus();
   },500);
};

  showSuccess( header, mensaje ) {
     this.msjs = [];
     this.msjs.push({severity:'success', summary: header, detail: mensaje});
  }

  showInfo( header, mensaje ) {
     this.msjs = [];
     this.msjs.push({severity:'info', summary: header, detail: mensaje});
  }

  showWarn( header, mensaje ) {
     this.msjs = [];
     this.msjs.push({severity:'warn', summary: header, detail: mensaje});
  }

  showError( header, mensaje ) {
     this.msjs = [];
     this.msjs.push({severity:'error', summary: header, detail: mensaje});
  }

  clear() {
     this.msjs = [];
  }

  changeAccion(){

  }

}

class ClonRegistro implements ReaccionInterface {
  constructor(
              public id?,
              public fecha?,
              public nombre?,
              public porcent_de_aprobacion?,
              public descripcion?,
              public estado?){}
}
