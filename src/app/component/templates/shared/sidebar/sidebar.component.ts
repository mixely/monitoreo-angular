import { Component, OnInit } from '@angular/core';
// import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styles: []
})
export class SidebarComponent implements OnInit {

  menu:any[] = [];
  contenedor:string = "";
  menuEle:any[] = [];
  elementos:any[] = [];
  cont:number = 1;
  imagen:string = "";
  constructor() {
    this.menu = JSON.parse(localStorage.getItem('menu'));
    for (let key in this.menu) {
        let obj = this.menu[key];
        let open = "";
        this.imagen = obj.contenedor_imagen;

        if(this.contenedor=="" || this.contenedor==obj.contenedor){
          this.contenedor = obj.contenedor;
          this.elementos.push({item_nombre: obj.item, item_url: obj.url, item_imagen: obj.imagen});
        }else{
          if(this.cont==1){
            open = "open";
            this.cont++;
          }
          this.menuEle.push({
            "contenedor": this.contenedor,
            "imagen": this.imagen,
            "open": open,
            "elementos": this.elementos
          });
          this.contenedor = obj.contenedor;
          this.elementos = [];
          this.elementos.push({item_nombre: obj.item, item_url: obj.url, item_imagen: obj.imagen });
        }
    }
    this.menuEle.push({
      "contenedor": this.contenedor,
      "imagen": this.imagen,
      "open": open,
      "elementos": this.elementos
    });
  }

  ngOnInit() {
  }
}
