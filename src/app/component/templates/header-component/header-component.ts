import { Component, OnInit } from '@angular/core';

import { LoginService } from '../../../services/login.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styles: []
})
export class HeaderComponent implements OnInit {

  constructor( private _ls:LoginService) { }

  ngOnInit() {
  }

  salir(){
    this._ls.setToken("");
    this._ls.setLoged(false);
    this._ls.redireccionar();
  }

}
