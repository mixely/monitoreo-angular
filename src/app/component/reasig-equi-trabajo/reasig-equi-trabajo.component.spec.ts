import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReasigEquiTrabajoComponent } from './reasig-equi-trabajo.component';

describe('ReasigEquiTrabajoComponent', () => {
  let component: ReasigEquiTrabajoComponent;
  let fixture: ComponentFixture<ReasigEquiTrabajoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReasigEquiTrabajoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReasigEquiTrabajoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
