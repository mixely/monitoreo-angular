import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { NgForm, FormGroup, FormControl, Validators } from "@angular/forms";
import { Message, MenuItem, DataTable } from 'primeng/primeng';
import { GrowlModule } from 'primeng/primeng';
import { NgModule } from '@angular/core';
import { ReasignaciondeEquiposdeTrabajoCarteraInterface } from '../../interfaces/ReasignaciondeEquiposdeTrabajoCarteraInterface';
import { InputTextModule, DataTableModule, ButtonModule, DialogModule} from 'primeng/primeng';
import { CalendarModule } from 'primeng/primeng';

@Component({
  selector: 'app-reasig-equi-trabajo',
  templateUrl: './reasig-equi-trabajo.component.html',
  host: {'(window:keydown)': 'hotkeys($event)'}
})

export class ReasigEquiTrabajoComponent implements OnInit {

  hotkeys(event){
     // ALT + "+"
     if(event.altKey && event.keyCode==107){
        this.display_modal = true;
        this.limpiarForm();
     }
  }

  Formulario:FormGroup;
  can_edit = false;
  display_modal = false;
  accion:string = "crear";
  msjs:Message[] = [];

  objeto:ReasignaciondeEquiposdeTrabajoCarteraInterface = {
    id: "",
    proveedor: "",
    cartera: "",
    agente: "",
    condicion: "",
    fecha_de_ingreso: ""
  }

  Tabla:any[] = [
    {
      id: "1",
      proveedor: "Ripley",
      cartera: "Ripley",
      agente: "",
      condicion: "",
      fecha_de_ingreso: "2017-07-01",
    },
    {
      id: "2",
      proveedor: "Ripley",
      cartera: "Ripley",
      agente: "",
      condicion: "",
      fecha_de_ingreso: "2017-07-01",
    },
    {
      id: "3",
      proveedor: "Ripley",
      cartera: "Ripley",
      agente: "",
      condicion: "",
      fecha_de_ingreso: "2017-07-01",
    }
  ];

  constructor(private router:Router,
              private cdRef:ChangeDetectorRef) {

    this.Formulario = new FormGroup({
      'id': new FormControl(this.objeto.id),
      'proveedor': new FormControl(this.objeto.proveedor, Validators.required),
      'cartera': new FormControl(this.objeto.cartera, Validators.required),
      'agente': new FormControl(this.objeto.agente, Validators.required),
      'condicion': new FormControl(this.objeto.condicion, Validators.required),
      'fecha_de_ingreso': new FormControl(this.objeto.fecha_de_ingreso)
    });
  };


    Edit(event){
      console.log(event.data);
       this.can_edit = true
       if(event.originalEvent.type=="dblclick"){
          this.accion = "editar";
          let obj = this.Clonar(event.data);
          this.Formulario.setValue(obj);
          this.display_modal = true;
       }
    }

    editarModal(dt:DataTable){
       this.accion = "editar";
       let obj = this.Clonar(dt.selection);
       // this.usuario["distrito"] = [...this.usuario["distrito"], obj.distrito]
       this.Formulario.setValue(obj);
       this.display_modal = true;
    }

    Clonar(r: ReasignaciondeEquiposdeTrabajoCarteraInterface){
         let objeto = new ClonRegistro();
         for(let prop in r){
           if(this.objeto.hasOwnProperty(prop)){
             objeto[prop] = r[prop];
           }
         }
         return objeto;
      }

    ngOnInit() {
    }

    crearModal(){
       this.display_modal = true;
       this.limpiarForm();
    }

    limpiarForm(){
      this.Formulario.reset({
        id: "",
        proveedor: "",
        cartera: "",
        agente: "",
        condicion: "",
        fecha_de_ingreso: ""
      });
     setTimeout(()=>{
        document.getElementById("id").focus();
     },500);
  };

    showSuccess( header, mensaje ) {
       this.msjs = [];
       this.msjs.push({severity:'success', summary: header, detail: mensaje});
    }

    showInfo( header, mensaje ) {
       this.msjs = [];
       this.msjs.push({severity:'info', summary: header, detail: mensaje});
    }

    showWarn( header, mensaje ) {
       this.msjs = [];
       this.msjs.push({severity:'warn', summary: header, detail: mensaje});
    }

    showError( header, mensaje ) {
       this.msjs = [];
       this.msjs.push({severity:'error', summary: header, detail: mensaje});
    }

    clear() {
       this.msjs = [];
    }

    changeAccion(){

    }
  }

  class ClonRegistro implements ReasignaciondeEquiposdeTrabajoCarteraInterface {
    constructor(
                public id?,
                public proveedor?,
                public cartera?,
                public agente?,
                public condicion?,
                public fecha_de_ingreso?){}
  }
