import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EstadoAsigComponent } from './estado-asig.component';

describe('EstadoAsigComponent', () => {
  let component: EstadoAsigComponent;
  let fixture: ComponentFixture<EstadoAsigComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EstadoAsigComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EstadoAsigComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
