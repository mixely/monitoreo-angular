import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RelMonitPerfComponent } from './rel-monit-perf.component';

describe('RelMonitPerfComponent', () => {
  let component: RelMonitPerfComponent;
  let fixture: ComponentFixture<RelMonitPerfComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RelMonitPerfComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RelMonitPerfComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
