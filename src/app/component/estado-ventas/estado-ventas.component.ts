import { Component, OnInit } from '@angular/core';
import { NgForm, FormGroup, FormControl, Validators } from "@angular/forms";

@Component({
  selector: 'app-estado-ventas',
  templateUrl: './estado-ventas.component.html',
  host: {'(window:keydown)': 'hotkeys($event)'}
})
export class EstadoVentasComponent implements OnInit {
  // host: {'(window:keydown)': 'hotkeys($event)'}
  hotkeys(event){
    // ALT + C
    if(event.altKey && event.keyCode==67){
       this.display_modal = true;
    }
  }

  modificarEstados:Object = {
    fecha:"",
    dni:"",
    cliente:"",
    asesor:"",
    importe:"",
    firmas:"",
    fecha_firmas:"",
    documento:"",
    documento_fecha:"",
    banco:"",
    banco_fecha:"",
    desembolso:"",
    desembolso_banco:"",
    conformidad:"",
    conformidad_banco:"",
    observacion:""
  }

  display_modal:boolean = false;

  frmModificarEstados:FormGroup;

  inicio:any[] = [];
  fin:any[] = [];
  Perfiles:any[] = [];
  date3:any[] = [];

  constructor() {
    this.frmModificarEstados = new FormGroup({
      fecha:new FormControl({value:"", disabled: true}),
      dni:new FormControl({value:"", disabled: true}),
      cliente:new FormControl({value:"", disabled: true}),
      asesor:new FormControl({value:"", disabled: true}),
      importe:new FormControl({value:"", disabled: true}),
      firmas:new FormControl('', Validators.required),
      fecha_firmas:new FormControl('', Validators.required),
      documento:new FormControl('', Validators.required),
      documento_fecha:new FormControl('', Validators.required),
      banco:new FormControl('', Validators.required),
      banco_fecha:new FormControl('', Validators.required),
      desembolso:new FormControl('', Validators.required),
      desembolso_banco:new FormControl('', Validators.required),
      conformidad:new FormControl('', Validators.required),
      conformidad_banco:new FormControl('', Validators.required),
      observacion:new FormControl('', Validators.required)
    });
  }

  ngOnInit() {
  }

  crearCartera(){

  }

  modificar(){
    console.log("No pasa nada por aquí");
  }

  limpiarForm(){
    this.frmModificarEstados.reset( this.modificarEstados );
    setTimeout(()=>{
      document.getElementById("fecha").focus();
    },500);
  }

}
