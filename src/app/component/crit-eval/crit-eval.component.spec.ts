import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CritEvalComponent } from './crit-eval.component';

describe('CritEvalComponent', () => {
  let component: CritEvalComponent;
  let fixture: ComponentFixture<CritEvalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CritEvalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CritEvalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
