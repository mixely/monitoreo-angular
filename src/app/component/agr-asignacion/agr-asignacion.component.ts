import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { NgForm, FormGroup, FormControl, Validators } from "@angular/forms";
import { Message, MenuItem, DataTable } from 'primeng/primeng';
import { GrowlModule } from 'primeng/primeng';
import { NgModule } from '@angular/core';

import { CondicionInterface } from '../../interfaces/CondicionInterface';
import { GenerarAsignacionInterface } from '../../interfaces/GenerarAsignacionInterface';
import { LoginService } from '../../services/login.service';
import { GenerarAsignacionService } from '../../services/GenerarAsignacion.service';
import { InputTextModule, DataTableModule, ButtonModule, DialogModule } from 'primeng/primeng';
import { CalendarModule } from 'primeng/primeng';

@Component({
  selector: 'app-agr-asignacion',
  templateUrl: './agr-asignacion.component.html',
  host: {'(window:keydown)': 'hotkeys($event)'}
})
export class AgrAsignacionComponent implements OnInit {
  Formulario:FormGroup;

  display_modal:boolean = false;
  accion:string = "crear";
  msjs:Message[] = [];
  proveedores:any[] = [];
  tipo_carteras:any[] = [];

  selectedRecord:GenerarAsignacionInterface;
  Tabla:GenerarAsignacionInterface[] = [];
  csv:boolean = true; // Si es "true" entonces va a desahabilitar
  can_edit = false;

  constructor(private cdRef:ChangeDetectorRef,
              private _cs:GenerarAsignacionService,
              private _ls:LoginService
              ) {
    this.Formulario = new FormGroup({
      'id': new FormControl(this.objeto.id),
      'monitor': new FormControl(this.objeto.monitor, Validators.required),
      'tipo_de_cartera': new FormControl(this.objeto.tipo_de_cartera, Validators.required),
      'cartera': new FormControl(this.objeto.cartera, Validators.required),
      'reaccion': new FormControl(this.objeto.reaccion, Validators.required),
      'registro': new FormControl(this.objeto.registro, Validators.required)
    });
  }

  // host: {'(window:keydown)': 'hotkeys($event)'}
  hotkeys(event){
    // ALT + C
    if(event.altKey && event.keyCode==107){
       this.display_modal = true;
    }
  }

  objeto:GenerarAsignacionInterface = {
    id: "",
    monitor: "",
    tipo_de_cartera: "",
    cartera: "",
    reaccion: "",
    registro: ""
  }

  limpiarForm(){
    this.Formulario.reset({
      id: "",
      monitor: "",
      tipo_de_cartera: "",
      cartera: "",
      reaccion: "",
      registro: ""
    });

    setTimeout(()=>{
      document.getElementById("cartera").focus();
    },500);
  };

  ngOnInit() {
    this.get();
    // cargamos los proveedores
    // this._ps.get()
    //         .subscribe(data => {
    //           this.proveedores = data.datos;
    //         },
    //         error =>{
    //           this._ls.handleError(error);
    //         });
    // cargamos los tipos de cartera
    this._cs.cargarTiposGenerarAsignacion()
            .subscribe(data => {
              this.tipo_carteras = data.datos;
            },
            error =>{
              this._ls.handleError(error);
            });
  }

  ngAfterViewChecked(){
   this.cdRef.detectChanges();
  }

  crearModal(){
    this.display_modal = true;
    this.limpiarForm();
  }

  generarCSV(dt:DataTable){
    dt.exportCSV();
  }

  editarModal(dt:DataTable){
     this.accion = "editar";
     let obj = this.Clonar(dt.selection);
     this.Formulario.setValue(obj);
     this.display_modal = true;
  }

  Edit(event){
     this.can_edit = true
     if(event.originalEvent.type=="dblclick"){
        this.accion = "editar";
        let obj = this.Clonar(event.data);
        this.Formulario.setValue(obj);
        this.display_modal = true;
     }
  }

  Selected(obj){
     this.objeto.id = obj.id;
  }
  changeAccion(){
   this.accion = "crear";
  }

Clonar(r: GenerarAsignacionInterface){
   let objeto = new ClonRegistro();
   for(let prop in r){
     if(this.objeto.hasOwnProperty(prop)){
       objeto[prop] = r[prop];
     }
   }
   return objeto;
}

Crear(){
   this._cs.set( this.objeto )
   .subscribe(data =>{
     this.Tabla = [];
      for (let i = 0; i < data.datos.length; i++) {
         this.Tabla = [...this.Tabla, data.datos[i]];
      }
      this.display_modal = false;
      if(this.Tabla.length>0){
        this.csv = false; // Entonces habilitamos el botón
      }
      this.showSuccess('Exito', "Hemos añadido el tipo de resultado al sistema");
    },
     error =>{
       this._ls.handleError(error);
     });
}

Editar(){
  this.display_modal = false;
   this._cs.update( this.objeto )
           .subscribe(
              data => {
                this.showSuccess("Exito", "Hemos actualizado el tipo de resultado");
                this.get();
              },
              error =>{
                this._ls.handleError(error);
              });
}

get(){
   this.showWarn( "Espere", "Estamos obteniendo los datos" );
   this._cs.get()
           .subscribe(
             data => {
              this.Tabla = [];
              this.Tabla = data.datos;
              this.can_edit = false;
              this.clear();
             },
             error =>{
                this._ls.handleError(error);
              });
}

  showSuccess( header, mensaje ) {
     this.msjs.push({severity:'success', summary: header, detail: mensaje});
     setTimeout(()=> this.clear(), 3000);
  }

  showInfo( header, mensaje ) {
     this.msjs.push({severity:'info', summary: header, detail: mensaje});
     setTimeout(()=> this.clear(), 3000);
  }

  showWarn( header, mensaje ) {
     this.msjs.push({severity:'warn', summary: header, detail: mensaje});
     setTimeout(()=> this.clear(), 3000);
  }

  showError( header, mensaje ) {
     this.msjs.push({severity:'error', summary: header, detail: mensaje});
     setTimeout(()=> this.clear(), 3000);
  }

  clear(){
     this.msjs = [];
  }
}

class ClonRegistro implements GenerarAsignacionInterface {
   constructor(
                   public id?,
                   public monitor?,
                   public tipo_de_cartera?,
                   public cartera?,
                   public reaccion?,
                   public registro?){}
}




//
//   hotkeys(event){
//      // ALT + "+"
//      if(event.altKey && event.keyCode==107){
//         this.display_modal = true;
//         this.limpiarForm();
//      }
//   }
//
//   Formulario:FormGroup;
//   can_edit = false;
//   display_modal = false;
//   accion:string = "crear";
//   msjs:Message[] = [];
//
//   objeto:GenerarAsignacionInterface = {
//     id: "",
//     monitor: "",
//     tipo_de_cartera: "",
//     cartera: "",
//     reaccion: "",
//     registro: "",
//   }
//
//   Tabla:any[] = [
//     {
//       id: "1",
//       monitor: "",
//       tipo_de_cartera: "",
//       cartera: "",
//       reaccion: "",
//       registro: "",
//     },
//     {
//       id: "3",
//       monitor: "",
//       tipo_de_cartera: "",
//       cartera: "",
//       reaccion: "",
//       registro: "",
//     }
//   ];
//
//   constructor(private router:Router,
//               private cdRef:ChangeDetectorRef) {
//
//     this.Formulario = new FormGroup({
//       'id': new FormControl(this.objeto.id),
//       'monitor': new FormControl(this.objeto.monitor, Validators.required),
//       'tipo_de_cartera': new FormControl(this.objeto.tipo_de_cartera, Validators.required),
//       'cartera': new FormControl(this.objeto.cartera, Validators.required),
//       'reaccion': new FormControl(this.objeto.reaccion, Validators.required),
//       'registro': new FormControl(this.objeto.registro, Validators.required),
//     });
// };
//
//   Edit(event){
//     console.log(event.data);
//      this.can_edit = true
//      if(event.originalEvent.type=="dblclick"){
//         this.accion = "editar";
//         let obj = this.Clonar(event.data);
//         this.Formulario.setValue(obj);
//         this.display_modal = true;
//      }
//   }
//
//   editarModal(dt:DataTable){
//      this.accion = "editar";
//      let obj = this.Clonar(dt.selection);
//      // this.usuario["distrito"] = [...this.usuario["distrito"], obj.distrito]
//      this.Formulario.setValue(obj);
//      this.display_modal = true;
//   }
//
//   Clonar(r: CondicionInterface){
//        let objeto = new ClonRegistro();
//        for(let prop in r){
//          if(this.objeto.hasOwnProperty(prop)){
//            objeto[prop] = r[prop];
//          }
//        }
//        return objeto;
//     }
//
//   ngOnInit() {
//   }
//
//   crearModal(){
//      this.display_modal = true;
//      this.limpiarForm();
//   }
//
//   limpiarForm(){
//     this.Formulario.reset({
//       id: "",
//       monitor: "",
//       tipo_de_cartera: "",
//       cartera: "",
//       reaccion: "",
//       registro: "",
//     });
//    setTimeout(()=>{
//       document.getElementById("id").focus();
//    },500);
// };
//
//   showSuccess( header, mensaje ) {
//      this.msjs = [];
//      this.msjs.push({severity:'success', summary: header, detail: mensaje});
//   }
//
//   showInfo( header, mensaje ) {
//      this.msjs = [];
//      this.msjs.push({severity:'info', summary: header, detail: mensaje});
//   }
//
//   showWarn( header, mensaje ) {
//      this.msjs = [];
//      this.msjs.push({severity:'warn', summary: header, detail: mensaje});
//   }
//
//   showError( header, mensaje ) {
//      this.msjs = [];
//      this.msjs.push({severity:'error', summary: header, detail: mensaje});
//   }
//
//   clear() {
//      this.msjs = [];
//   }
//
//   changeAccion(){
//
//   }
//
// }
//
// class ClonRegistro implements GenerarAsignacionInterface {
//   constructor(
//               public id?,
//               public monitor?,
//               public tipo_de_cartera?,
//               public cartera?,
//               public reaccion?,
//               public registro?){}
// }
