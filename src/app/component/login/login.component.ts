import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { NgForm, FormGroup, FormControl, Validators } from "@angular/forms";

import { LoginService } from '../../services/login.service';

import { LoginInterface } from '../../interfaces/login.interface';

import { Message } from 'primeng/primeng';

@Component({
   selector: 'app-login',
   templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit {

  Login:FormGroup;
  Reset:FormGroup;

  msjs:Message[] = [];

  constructor(private cdRef:ChangeDetectorRef,
              private router:Router,
              private _ls:LoginService
              ) {

    this._ls.setToken("");
    this._ls.setLoged(false);
    if(this._ls.getEstadoToken()=="caducado"){
      this.showError("Sesión ha expirado", "Por favor, vuelva a iniciar sesión");
    }

    this.Login = new FormGroup({
      'user': new FormControl(this.objeto.user, Validators.required),
      'password': new FormControl(this.objeto.password, Validators.required)
    });

    this.Reset = new FormGroup({
      'email': new FormControl(this.objeto_reset.email, Validators.required)
    });
  }

  objeto:LoginInterface = {
    user: "",
    password: ""
  }

  objeto_reset:any = {
    email: ""
  }

  limpiarForm(){
    this.Login.reset({
      user: "",
      password: ""
    });

    this.Reset.reset({
      email: ""
    });

    setTimeout(()=>{
      document.getElementById("user").focus();
    },500);
  };

  ngOnInit(){
    this._ls.compruebaValidez();
  }

  ngAfterViewChecked(){
    this.cdRef.detectChanges();
  }


Redireccionar(){
  this.router.navigate(['/perfil']);
  window.location.reload();
}

Auth(){
   this._ls.auth( this.objeto )
   .subscribe(
     (status) =>{
      if(status){
        this.Redireccionar();
      }
    },
    error =>{
        this._ls.handleError(error);
    });

}

Resetear(){
  console.log(this.objeto_reset);
}

showSuccess( header, mensaje ) {
   this.msjs.push({severity:'success', summary: header, detail: mensaje});
   setTimeout(()=> this.clear(), 3000);
}

showInfo( header, mensaje ) {
   this.msjs.push({severity:'info', summary: header, detail: mensaje});
   setTimeout(()=> this.clear(), 3000);
}

showWarn( header, mensaje ) {
   this.msjs.push({severity:'warn', summary: header, detail: mensaje});
   setTimeout(()=> this.clear(), 3000);
}

showError( header, mensaje ) {
   this.msjs.push({severity:'error', summary: header, detail: mensaje});
   setTimeout(()=> this.clear(), 3000);
}

clear(){
   this.msjs = [];
}


}
