import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgrRelAtribuClasifComponent } from './agr-rel-atribu-clasif.component';

describe('AgrRelAtribuClasifComponent', () => {
  let component: AgrRelAtribuClasifComponent;
  let fixture: ComponentFixture<AgrRelAtribuClasifComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgrRelAtribuClasifComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgrRelAtribuClasifComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
