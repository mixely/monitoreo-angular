import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TipocartComponent } from './tipocart.component';

describe('TipocartComponent', () => {
  let component: TipocartComponent;
  let fixture: ComponentFixture<TipocartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TipocartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TipocartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
