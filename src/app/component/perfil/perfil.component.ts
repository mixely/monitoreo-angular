import { Component, OnInit } from '@angular/core';
import { NgForm, FormGroup, FormControl, Validators } from "@angular/forms";
import { PerfilesService } from '../../services/perfiles.service';
import { LoginService } from '../../services/login.service';

import { Settings } from '../../config';
import { PerfilInterface } from '../../interfaces/perfil';

import { Message, MenuItem, DataTable } from 'primeng/primeng';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  host: {'(window:keydown)': 'hotkeys($event)'}
})

export class PerfilComponent implements OnInit {
  // host: {'(window:keydown)': 'hotkeys($event)'}
  hotkeys(event){
    // ALT + C
    if(event.altKey && event.keyCode==67){
       this.Formulario.reset({
         perfil: ""
       })
       this.display_modal = true;
    }
  }

  msjs:Message[] = [];
  csv:boolean = true; // Si es "true" entonces va a desahabilitar
  selectedRecord:any;
  display_modal:boolean = false;
  accion:string = "crear";
  Formulario:FormGroup;

  Perfiles:any[] = [];
  Perfil:PerfilInterface;


  usuario:PerfilInterface = {
    perfil:"",
    estado:"activo"
  }

  constructor(
    private _ps:PerfilesService,
    private _ls:LoginService) {

    this.Formulario = new FormGroup({
      'perfil': new FormControl('', Validators.required)
    });

  }

  ngOnInit() {
    this.showWarn( "Espere", "Estamos obteniendo los datos" );
    this.get();
  }
  crearModal(){
    this.display_modal = true;
    this.limpiarForm();
  }

  Edit(event){
    console.log(event);
  }

  changeAccion(){
   this.accion = "crear";
  }

  generarCSV(dt:DataTable){
    dt.exportCSV();
  }

  Crear(){
    this._ps.set( this.usuario )
          .subscribe(
            data => {
            this.cargarPerfiles(data);
            },
            error =>{
              this._ls.handleError(error);
            });
        this.showSuccess("Exito", "Perfil creado con exitosamente");
        this.display_modal = false;
  }

  get(){
    this._ps.get()
    .subscribe(
      data =>{
        this.clear();
        this.cargarPerfiles(data);
      },
      error =>{
        this._ls.handleError(error);
      });
  }

  cargarPerfiles(data){
    let p:any[] = [];
    this.Perfiles = [];
    for (let i = 0; i < data.data.length; i++) {
      let p = {
        id: data.data[i].id,
        perfil: data.data[i].perfil,
        estado: data.data[i].estado
      }
      this.Perfiles = [...this.Perfiles, p];
      if(this.Perfiles.length>0){
        this.csv = false; // Entonces habilitamos el botón
      }
    }

  }

  limpiarForm(){
    this.Formulario.reset( this.usuario );
    setTimeout(()=>{
      document.getElementById("perfil").focus();
    },500);
  }

  showSuccess( header, mensaje ) {
      this.msjs = [];
      this.msjs.push({severity:'success', summary: header, detail: mensaje});
  }

  showInfo( header, mensaje ) {
      this.msjs = [];
      this.msjs.push({severity:'info', summary: header, detail: mensaje});
  }

  showWarn( header, mensaje ) {
      this.msjs = [];
      this.msjs.push({severity:'warn', summary: header, detail: mensaje});
  }

  showError( header, mensaje ) {
      this.msjs = [];
      this.msjs.push({severity:'error', summary: header, detail: mensaje});
  }

  clear() {
      this.msjs = [];
  }
}
