import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { NgForm, FormGroup, FormControl, Validators } from "@angular/forms";

import { EquipoTrabajoService } from '../../services/equipo-trabajo.service';
import { LoginService } from '../../services/login.service';

import { EquipoTrabajoInterface } from '../../interfaces/equipo-trabajo.interface';
import { Message, MenuItem, DataTable } from 'primeng/primeng';

@Component({
  selector: 'app-equipo-trabajo',
  templateUrl: './equipo-trabajo.component.html',
  host: {'(window:keydown)': 'hotkeys($event)'},
})
export class EquipoTrabajoComponent implements OnInit {

  Formulario:FormGroup;

  display_modal:boolean = false;
  accion:string = "crear";
  msjs:Message[] = [];

  selectedRecord:EquipoTrabajoInterface;
  Tabla:EquipoTrabajoInterface[] = [];
  csv:boolean = true; // Si es "true" entonces va a desahabilitar

  can_edit = false;

  constructor(private cdRef:ChangeDetectorRef,
              private _ls:LoginService,
              private _ets:EquipoTrabajoService
              ) {
    this.Formulario = new FormGroup({
      'id': new FormControl(this.objeto.id),
      'equipo_trabajo': new FormControl(this.objeto.equipo_trabajo, Validators.required),
      'estado': new FormControl(this.objeto.estado)
    });
  }

  hotkeys(event){
    // ALT + C
    if(event.altKey && event.keyCode==107){
       this.display_modal = true;
    }
  }

  objeto:EquipoTrabajoInterface = {
    id:"",
    equipo_trabajo:"",
    estado:""
  }

  limpiarForm(){
    this.Formulario.reset({
      id:"",
      equipo_trabajo:"",
      estado:""
    });

    setTimeout(()=>{
      document.getElementById("equipo_trabajo").focus();
    },500);
  };

  ngOnInit() {
    this.get();
  }

  ngAfterViewChecked(){
   this.cdRef.detectChanges();
  }

  crearModal(){
    this.display_modal = true;
    this.limpiarForm();
  }

  generarCSV( dt:DataTable ){
    dt.exportCSV();
  }

  editarModal( dt:DataTable ){
     this.accion = "editar";
     let obj = this.Clonar(dt.selection);
     // this.objeto["distrito"] = [...this.objeto["distrito"], obj.distrito]
     this.Formulario.setValue(obj);
     this.display_modal = true;
  }

  Edit(event){
     this.can_edit = true
     if(event.originalEvent.type=="dblclick"){
       console.log(event.data);
        this.accion = "editar";
        let obj = this.Clonar(event.data);
        this.Formulario.setValue(obj);
        this.display_modal = true;
     }
  }

  Selected(obj){
     this.objeto.id = obj.id;
  }
  changeAccion(){
   this.accion = "crear";
  }

  Clonar(r: EquipoTrabajoInterface){
     let objeto = new ClonRegistro();
     for(let prop in r){
       if(this.objeto.hasOwnProperty(prop)){
         objeto[prop] = r[prop];
       }
     }
     return objeto;
  }

  Crear(){
     this._ets.set( this.objeto )
     .subscribe(
       data => {
         this.Tabla = [];
          for (let i = 0; i < data.datos.length; i++) {
             this.Tabla = [...this.Tabla, data.datos[i]];
          }
          this.display_modal = false;
          if(this.Tabla.length>0){
            this.csv = false; // Entonces habilitamos el botón
          }
          this.showSuccess('Exito', "Hemos añadido el tipo de resultado al sistema");
       },
       error =>{
         this._ls.handleError(error);
       });
  }

  Editar(){
    this.display_modal = false;
     this._ets.update( this.objeto )
             .subscribe(
               data => {
                 this.showSuccess("Exito", "Hemos actualizado el tipo de resultado");
                 this.get();
               },
               error =>{
                 this._ls.handleError(error);
               });
  }

  get(){
     this.showWarn( "Espere", "Estamos obteniendo los datos" );
     this._ets.get()
             .subscribe(
               data => {
                  this.Tabla = [];
                  this.Tabla = data.datos;
                  this.can_edit = false;
               },
               error =>{
                 this._ls.handleError(error);
               });
  }

  showSuccess( header, mensaje ) {
     this.msjs.push({severity:'success', summary: header, detail: mensaje});
     setTimeout(()=> this.clear(), 3000);
  }

  showInfo( header, mensaje ) {
     this.msjs.push({severity:'info', summary: header, detail: mensaje});
     setTimeout(()=> this.clear(), 3000);
  }

  showWarn( header, mensaje ) {
     this.msjs.push({severity:'warn', summary: header, detail: mensaje});
     setTimeout(()=> this.clear(), 3000);
  }

  showError( header, mensaje ) {
     this.msjs.push({severity:'error', summary: header, detail: mensaje});
     setTimeout(()=> this.clear(), 3000);
  }

  clear() {
     this.msjs = [];
  }

}

class ClonRegistro implements EquipoTrabajoInterface {
   constructor(
     public id?,
     public equipo_trabajo?,
     public estado?){}
}
