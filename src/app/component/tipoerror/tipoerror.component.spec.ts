import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TipoerrorComponent } from './tipoerror.component';

describe('TipoerrorComponent', () => {
  let component: TipoerrorComponent;
  let fixture: ComponentFixture<TipoerrorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TipoerrorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TipoerrorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
