import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { NgForm, FormGroup, FormControl, Validators } from "@angular/forms";

import { JustificacionService } from '../../services/justificacion.service';
import { LoginService } from '../../services/login.service';

import { JustificacionInterface } from '../../interfaces/justificacion.interface';
import { Message, MenuItem, DataTable } from 'primeng/primeng';

@Component({
  selector: 'app-justificacion',
  templateUrl: './justificacion.component.html',
  host: {'(window:keydown)': 'hotkeys($event)'}
})
export class JustificacionComponent implements OnInit {

  Formulario:FormGroup;

  display_modal:boolean = false;
  accion:string = "crear";
  msjs:Message[] = [];

  selectedRecord:JustificacionInterface;
  Tabla:JustificacionInterface[] = [];
  csv:boolean = true; // Si es "true" entonces va a desahabilitar

  can_edit = false;

  constructor(private cdRef:ChangeDetectorRef,
              private _ls:LoginService,
              private _js:JustificacionService
              ) {
    this.Formulario = new FormGroup({
      'id': new FormControl(this.objeto.id),
      'justificacion': new FormControl(this.objeto.justificacion, Validators.required),
      'estado': new FormControl(this.objeto.estado)
    });
  }

  // host: {'(window:keydown)': 'hotkeys($event)'}
  hotkeys(event){
    // ALT + C
    if(event.altKey && event.keyCode==107){
       this.display_modal = true;
    }
  }

  objeto:JustificacionInterface = {
    id:"",
    justificacion:"",
    estado:""
  }

  limpiarForm(){
    this.Formulario.reset({
      id:"",
      justificacion:"",
      estado:""
    });

    setTimeout(()=>{
      document.getElementById("justificacion").focus();
    },500);
  };

  ngOnInit() {
    this.get();
  }

  ngAfterViewChecked(){
   this.cdRef.detectChanges();
  }

  crearModal(){
    this.display_modal = true;
    this.limpiarForm();
  }

  generarCSV(dt:DataTable){
    dt.exportCSV();
  }

  editarModal(dt:DataTable){
     this.accion = "editar";
     let obj = this.Clonar(dt.selection);
     // this.objeto["distrito"] = [...this.objeto["distrito"], obj.distrito]
     this.Formulario.setValue(obj);
     this.display_modal = true;
  }

  Edit(event){
     this.can_edit = true
     if(event.originalEvent.type=="dblclick"){
        this.accion = "editar";
        let obj = this.Clonar(event.data);
        this.Formulario.setValue(obj);
        this.display_modal = true;
     }
  }

  Selected(obj){
     this.objeto.id = obj.id;
  }
  changeAccion(){
   this.accion = "crear";
}

Clonar(r: JustificacionInterface){
   let objeto = new ClonRegistro();
   for(let prop in r){
     if(this.objeto.hasOwnProperty(prop)){
       objeto[prop] = r[prop];
     }
   }
   return objeto;
}

Crear(){
   this._js.set( this.objeto )
   .subscribe(
     data =>{
       this.Tabla = [];
        for (let i = 0; i < data.datos.length; i++) {
           this.Tabla = [...this.Tabla, data.datos[i]];
        }
        this.display_modal = false;
        if(this.Tabla.length>0){
          this.csv = false; // Entonces habilitamos el botón
        }
        this.showSuccess('Exito', "Hemos añadido el tipo de resultado al sistema");
     },
     error =>{
        this._ls.handleError(error);
      });
}

Editar(){
  this.display_modal = false;
   this._js.update( this.objeto )
           .subscribe(
             data => {
               this.showSuccess("Exito", "Hemos actualizado el tipo de resultado");
               this.get();
             },
             error =>{
                this._ls.handleError(error);
             });
}

get(){
   this.showWarn( "Espere", "Estamos obteniendo los datos" );
   this._js.get()
           .subscribe(
            data => {
              this.Tabla = [];
              this.Tabla = data.datos;
              this.can_edit = false;
            },
            error =>{
              this._ls.handleError(error);
            });
}

showSuccess( header, mensaje ) {
   this.msjs.push({severity:'success', summary: header, detail: mensaje});
   setTimeout(()=> this.clear(), 3000);
}

showInfo( header, mensaje ) {
   this.msjs.push({severity:'info', summary: header, detail: mensaje});
   setTimeout(()=> this.clear(), 3000);
}

showWarn( header, mensaje ) {
   this.msjs.push({severity:'warn', summary: header, detail: mensaje});
   setTimeout(()=> this.clear(), 3000);
}

showError( header, mensaje ) {
   this.msjs.push({severity:'error', summary: header, detail: mensaje});
   setTimeout(()=> this.clear(), 3000);
}

clear() {
   this.msjs = [];
}

}

class ClonRegistro implements JustificacionInterface {
   constructor(
     public id?,
     public justificacion?,
     public estado?){}
}
