import { Component, OnInit } from '@angular/core';
import { NgForm, FormGroup, FormControl, Validators } from "@angular/forms";

@Component({
  selector: 'app-control-campanas',
  templateUrl: './control-campanas.component.html',
  host: {'(window:keydown)': 'hotkeys($event)'}
})
export class ControlCampanasComponent implements OnInit {

  // host: {'(window:keydown)': 'hotkeys($event)'}
  hotkeys(event){
    // ALT + C
    if(event.altKey && event.keyCode==67){
      //  this.display_modal = true;
    }
  }

  control_campanas:Object = {
    idSegmento: "",
    fecha: ""
  }

  cobertura:any[] = [];
  equipos_trabajo:any[] = [];

  frmControlCampanas:FormGroup;

  constructor() {
    this.frmControlCampanas = new FormGroup({
      'idSegmento': new FormControl('', Validators.required),
      'fecha': new FormControl('', Validators.required)
    });
  }

  ngOnInit() {
  }

  limpiarForm(){
    this.frmControlCampanas.reset( this.control_campanas );
    setTimeout(()=>{
      document.getElementById("idSegmento").focus();
    },500);
  }
}
