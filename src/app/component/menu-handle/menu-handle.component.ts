import { Component, OnInit } from '@angular/core';
import { NgForm, FormGroup, FormControl, Validators } from "@angular/forms";

import { MenuContenedorBaseInterface } from '../../interfaces/menu-contenedor-base.interface';
import { MenuContenedorInterface } from '../../interfaces/menu-contenedor.interface';
import { MenuItemInterface } from '../../interfaces/menu-item.interface';
import { MenuContenedorItemInterface } from '../../interfaces/menu-contenedor-item.interface';

import { PerfilesService } from '../../services/perfiles.service';
import { MenuService } from '../../services/menu.service';
import { LoginService } from '../../services/login.service';

@Component({
  selector: 'app-menu-handle',
  templateUrl: './menu-handle.component.html',
  styles: []
})
export class MenuHandleComponent implements OnInit {

  display_modal_base:boolean = false;
  display_modal_menu_contenedor:boolean = false;

  selectedRecord:any;
  TablaMenuContenedorBase:any[] = [];
  TablaMenuContenedor:any[] = [];
  TablaMenuItem:any[] = [];
  TablaContenedorItem:any[] = [];

  perfiles:any [] = [];
  perfil_menu_contenedor_base:any [] = [];
  menu_contenedor:any[] = [];
  menu_item:any[] = [];
  menu_contenedor_item:any[] = [];

  FrmMenuContenedorBase:FormGroup;
  FrmMenuContenedorBaseEdit:FormGroup;

  FrmMenuContenedor:FormGroup;
  FrmMenuContenedorEdit:FormGroup;

  FrmMenuItem:FormGroup;
  FrmMenuItemEdit:FormGroup;

  FrmMenuContenedorItem:FormGroup;
  FrmMenuContenedorItemEdit:FormGroup;

  constructor(
    private _pf:PerfilesService,
    private _ms:MenuService,
    private _ls:LoginService
  ) {
    this.FrmMenuContenedorBase = new FormGroup({
      'id': new FormControl(this.obj_menu_contenedor_base.id),
      'id_perfil': new FormControl(this.obj_menu_contenedor_base.id_perfil, Validators.required),
      'nombre': new FormControl(this.obj_menu_contenedor_base.nombre, Validators.required)
    });

          this.FrmMenuContenedorBaseEdit = new FormGroup({
            'id': new FormControl(this.obj_menu_contenedor_base.id),
            'id_perfil': new FormControl(this.obj_menu_contenedor_base.id_perfil, Validators.required),
            'nombre': new FormControl(this.obj_menu_contenedor_base.nombre, Validators.required)
          });

    this.FrmMenuContenedor = new FormGroup({
      'id': new FormControl(this.obj_menu_contenedor.id),
      'id_perfil_menu_contenedor': new FormControl(this.obj_menu_contenedor.id_perfil_menu_contenedor, Validators.required),
      'nombre': new FormControl(this.obj_menu_contenedor.nombre, Validators.required),
      'imagen': new FormControl(this.obj_menu_contenedor.imagen),
      'nivel': new FormControl(this.obj_menu_contenedor.nivel, Validators.required),
      'orden': new FormControl(this.obj_menu_contenedor.orden, Validators.required),
      'id_menu_contenedor': new FormControl(this.obj_menu_contenedor.id_menu_contenedor)
    });

          this.FrmMenuContenedorEdit = new FormGroup({
            'id': new FormControl(this.obj_menu_contenedor.id),
            'id_perfil_menu_contenedor': new FormControl(this.obj_menu_contenedor.id_perfil_menu_contenedor, Validators.required),
            'nombre': new FormControl(this.obj_menu_contenedor.nombre, Validators.required),
            'imagen': new FormControl(this.obj_menu_contenedor.imagen),
            'nivel': new FormControl(this.obj_menu_contenedor.nivel, Validators.required),
            'id_menu_contenedor': new FormControl(this.obj_menu_contenedor.id_menu_contenedor)
          });

    this.FrmMenuItem = new FormGroup({
      'id': new FormControl(this.obj_menu_item.id),
      'nombre': new FormControl(this.obj_menu_item.nombre, Validators.required),
      'url': new FormControl(this.obj_menu_item.url, Validators.required),
      'imagen': new FormControl(this.obj_menu_item.imagen)
    });

          this.FrmMenuItemEdit = new FormGroup({
            'id': new FormControl(this.obj_menu_item.id),
            'nombre': new FormControl(this.obj_menu_item.nombre, Validators.required),
            'url': new FormControl(this.obj_menu_item.url, Validators.required),
            'imagen': new FormControl(this.obj_menu_item.imagen)
          });

    this.FrmMenuContenedorItem = new FormGroup({
      'id': new FormControl(this.obj_menu_contenedor_item.id),
      'id_menu_contenedor': new FormControl(this.obj_menu_contenedor_item.id_menu_contenedor, Validators.required),
      'id_menu_item': new FormControl(this.obj_menu_contenedor_item.id_menu_item, Validators.required),
    });

          this.FrmMenuContenedorItemEdit = new FormGroup({
            'id': new FormControl(this.obj_menu_contenedor_item.id),
            'id_menu_contenedor': new FormControl(this.obj_menu_contenedor_item.id_menu_contenedor, Validators.required),
            'id_menu_item': new FormControl(this.obj_menu_contenedor_item.id_menu_item, Validators.required),
          });

  }

  ngOnInit() {
    this._pf.get()
            .subscribe( data => {
              this.perfiles = data.data;
            },
            error => {
              this._ls.handleError(error);
            });
    this._pf.get();

    this.getMenuContenedorBase();
    this.getMenuContenedores();
    this.getMenuItem();
    this.getMenuContenedorItem();
  }

  getMenuContenedorBase(){
    this._ms.getMenuContenedorBase()
            .subscribe(
              data=>{
                this.TablaMenuContenedorBase = data.datos;
                this.perfil_menu_contenedor_base = data.datos;
              },
              error =>{
                this._ls.handleError(error);
              });
  }

  getMenuContenedores(){
    this._ms.getMenuContenedores()
    .subscribe(
      data=>{
        this.TablaMenuContenedor = data.datos;
        this.obj_menu_contenedor.orden = data.datos.length + 1;
        this.menu_contenedor = data.datos;
      },
      error =>{
        this._ls.handleError(error);
      });
  };

  getMenuItem(){
    this._ms.getMenuItem()
    .subscribe(
      data=>{
        this.TablaMenuItem = data.datos;
        this.menu_item = data.datos;
      },
      error =>{
        this._ls.handleError(error);
      });
  }

  getMenuContenedorItem(){
    this._ms.getMenuContenedorItem()
    .subscribe(
      data=>{
        this.TablaContenedorItem = data.datos;
        this.menu_contenedor_item = data.datos;
      },
      error =>{
        this._ls.handleError(error);
      });
  }

  obj_menu_contenedor_base:MenuContenedorBaseInterface = {
    id:"",
    id_perfil:"",
    nombre:""
  };

  obj_menu_contenedor_baseEdit:MenuContenedorBaseInterface = {
    id:"",
    id_perfil:"",
    nombre:""
  };

  obj_menu_contenedor:MenuContenedorInterface = {
    id: "",
    id_perfil_menu_contenedor: "",
    nombre: "",
    imagen: "",
    nivel: "",
    orden: "",
    id_menu_contenedor: ""
};

  obj_menu_contenedorEdit:MenuContenedorInterface = {
      id: "",
      id_perfil_menu_contenedor: "",
      nombre: "",
      imagen: "",
      nivel: "",
      orden: "",
      id_menu_contenedor: ""
  };

  obj_menu_item:MenuItemInterface = {
    id: "",
    nombre: "",
    url: "",
    imagen: ""
  };

  obj_menu_contenedor_item:MenuContenedorItemInterface = {
    id: "",
    id_menu_contenedor: "",
    id_menu_item: ""
  };

  limpiarMenuContenedorBase(){
    this.FrmMenuContenedor.reset( this.obj_menu_contenedor_base );
    setTimeout(()=>{
      document.getElementById("nombre").focus();
    },500);
  }

  SelectTab(event){
    this._pf.get()
    .subscribe(
      data=>{
        this.perfiles = data.data;
      },
      error =>{
        this._ls.handleError(error);
      });

    if(event.index==0)  {
      this.getMenuContenedorBase();
    }else if(event.index==1){
      this.getMenuContenedores();
      this.getMenuContenedorBase();
    }else if(event.index==2){
      this.getMenuItem();
    }else if(event.index==3){
      this.getMenuContenedorItem();
    }

  }

  CrearMenuBase(){
      this._ms.setMenuContenedorBase(this.obj_menu_contenedor_base)
              .subscribe(
                data =>{
                  this.TablaMenuContenedorBase = data.datos;
                },
                error =>{
                  this._ls.handleError(error);
                });
  }
  CrearMenuContenedor(){
      this._ms.setMenuContenedores(this.obj_menu_contenedor)
              .subscribe(
                data =>{
                  this.TablaMenuContenedor = data.datos;
                  this.cargarContenedores();
                },
                error =>{
                  this._ls.handleError(error);
                });
  }

  CrearMenuItem(){
      this._ms.setMenuItem(this.obj_menu_item)
              .subscribe(
                data =>{
                  this.TablaMenuItem = data.datos;
                  this.menu_item = data.datos;
                },
                error =>{
                  this._ls.handleError(error);
                });
  }

  CrearMenuContenedorItem(){
      this._ms.setMenuContenedorItem(this.obj_menu_contenedor_item)
              .subscribe(
                data =>{
                  this.TablaContenedorItem = data.datos;
                  this.menu_contenedor_item = data.datos;
                },
                error =>{
                  this._ls.handleError(error);
                });
  }

  cargarContenedores(nivel=""){
    this._ms.cargarContenedores(nivel)
            .subscribe(
              data =>{
                this.menu_contenedor = data.datos;
              },
              error =>{
                this._ls.handleError(error);
              });
  }

  EditBase(event){
     if(event.originalEvent.type=="dblclick"){
        let obj = this.ClonarBase(event.data);
        this.FrmMenuContenedorBaseEdit.setValue(obj);
        this.display_modal_base = true;
     }
  }

  ClonarBase(r: MenuContenedorBaseInterface) {
     let objeto = new ClonRegistroBase();
     for(let prop in r){
       if(this.obj_menu_contenedor_base.hasOwnProperty(prop)){
         objeto[prop] = r[prop];
       }
     }
     return objeto;
  }

  EditarBase(){
    this._ms.updateMenuContenedorBase(this.obj_menu_contenedor_baseEdit)
            .subscribe(data => {
              this.display_modal_base = false;
              this.TablaMenuContenedorBase = data.datos;
            })
  }

  EditMenuContenedor(event){
     if(event.originalEvent.type=="dblclick"){
        let obj = this.ClonarBase(event.data);
        this.FrmMenuContenedorBaseEdit.setValue(obj);
        this.display_modal_menu_contenedor = true;
     }
  }

  ClonarMenuContenedor(r: MenuContenedorInterface) {
     let objeto = new ClonRegistroMenuContenedor();
     for(let prop in r){
       if(this.obj_menu_contenedor_base.hasOwnProperty(prop)){
         objeto[prop] = r[prop];
       }
     }
     return objeto;
  }

  EditarMenuContenedor(){
    this._ms.updateMenuContenedor(this.obj_menu_contenedorEdit)
            .subscribe(data => {
              this.display_modal_menu_contenedor = false;
              this.TablaMenuContenedorBase = data.datos;
            })
  }
}

class ClonRegistroBase implements MenuContenedorBaseInterface {
   constructor(
     public id?,
     public id_perfil?,
     public nombre?){}
}

class ClonRegistroMenuContenedor implements MenuContenedorInterface {
   constructor(
     public id?,
     public id_perfil_menu_contenedor?,
     public nombre?,
     public imagen?,
     public nivel?,
     public orden?,
     public id_menu_contenedor?){}
}
