import { RouterModule, Routes } from '@angular/router';

import { MenuHandleComponent } from './component/menu-handle/menu-handle.component';
import { PerfilComponent } from './component/perfil/perfil.component';
import { UsuarioComponent } from './component/usuario/usuario.component';
import { ProveedorComponent } from './component/proveedor/proveedor.component';
import { CarteraComponent } from './component/cartera/cartera.component';
import { SegmentoComponent } from './component/segmento/segmento.component';
import { ProductoComponent } from './component/producto/producto.component';
import { ResultadoComponent } from './component/resultado/resultado.component';
import { JustificacionComponent } from './component/justificacion/justificacion.component';

import { RubroComponent } from './component/rubro/rubro.component';
import { TipoCarteraComponent } from './component/tipo-cartera/tipo-cartera.component';
import { TipoContactoComponent } from './component/tipo-contacto/tipo-contacto.component';
import { TipoDireccionComponent } from './component/tipo-direccion/tipo-direccion.component';
import { TipoProductoComponent } from './component/tipo-producto/tipo-producto.component';
import { TipoResultadoComponent } from './component/tipo-resultado/tipo-resultado.component';
import { TipoTelefonoComponent } from './component/tipo-telefono/tipo-telefono.component';

import { EquipoTrabajoComponent } from './component/equipo-trabajo/equipo-trabajo.component';
import { RelacionPerfilMenuComponent } from './component/relacion-perfil-menu/relacion-perfil-menu.component';
import { RelacionEquipoTrabajoCarteraComponent } from './component/relacion-equipo-trabajo-cartera/relacion-equipo-trabajo-cartera.component';
import { RelacionProductoCarteraComponent } from './component/relacion-producto-cartera/relacion-producto-cartera.component';
import { RelacionProductoProveedorComponent } from './component/relacion-producto-proveedor/relacion-producto-proveedor.component';
import { RelacionTipoContactoResultadoComponent } from './component/relacion-tipo-contacto-resultado/relacion-tipo-contacto-resultado.component';
import { PaletaResultadoComponent } from './component/paleta-resultado/paleta-resultado.component';

import { CargaValidacionComponent } from './component/carga-validacion/carga-validacion.component';
import { EstadoAsignacionesComponent } from './component/estado-asignaciones/estado-asignaciones.component';

import { ControlCampanasComponent } from './component/control-campanas/control-campanas.component';
import { CrearCampanaComponent } from './component/crear-campana/crear-campana.component';

import { ConsultaClientesComponent } from './component/consulta-clientes/consulta-clientes.component';
import { EstadoVentasComponent } from './component/estado-ventas/estado-ventas.component';
import { ModificacionMasivaEstadosComponent } from './component/modificacion-masiva-estados/modificacion-masiva-estados.component';

import { LoginComponent } from './component/login/login.component';

const APP_ROUTES: Routes = [
  { path: 'menu-handle', component: MenuHandleComponent },
  { path: 'perfil', component: PerfilComponent },
  { path: 'usuario', component: UsuarioComponent },
  { path: 'proveedor', component: ProveedorComponent },
  { path: 'cartera', component: CarteraComponent },
  { path: 'segmento', component: SegmentoComponent },
  { path: 'producto', component: ProductoComponent },
  { path: 'resultado', component: ResultadoComponent },
  { path: 'justificacion', component: JustificacionComponent },
  { path: 'equipo-trabajo', component: EquipoTrabajoComponent },

  { path: 'rubro', component: RubroComponent },
  { path: 'tipo-cartera', component: TipoCarteraComponent },
  { path: 'tipo-contacto', component: TipoContactoComponent },
  { path: 'tipo-direccion', component: TipoDireccionComponent },
  { path: 'tipo-producto', component: TipoProductoComponent },
  { path: 'tipo-resultado', component: TipoResultadoComponent },
  { path: 'tipo-telefono', component: TipoTelefonoComponent },

  { path: 'equipo-trabajo', component: EquipoTrabajoComponent },
  { path: 'relacion-perfil-menu', component: RelacionPerfilMenuComponent },
  { path: 'relacion-equipo-trabajo-cartera', component: RelacionEquipoTrabajoCarteraComponent },
  { path: 'relacion-producto-cartera', component: RelacionProductoCarteraComponent },
  { path: 'relacion-producto-proveedor', component: RelacionProductoProveedorComponent },
  { path: 'relacion-tipo-contacto-resultado', component: RelacionTipoContactoResultadoComponent },
  { path: 'paleta-resultado', component: PaletaResultadoComponent },

  { path: 'carga-validacion', component: CargaValidacionComponent },
  { path: 'estado-asignaciones', component: EstadoAsignacionesComponent },

  { path: 'control-campanas', component: ControlCampanasComponent },
  { path: 'crear-campana', component: CrearCampanaComponent },

  { path: 'consulta-clientes', component: ConsultaClientesComponent },
  { path: 'estado-ventas', component: EstadoVentasComponent },
  { path: 'modificacion-masiva-estados', component: ModificacionMasivaEstadosComponent },

  { path: 'login', component: LoginComponent },

  { path: '**', pathMatch: 'full', redirectTo: 'login' }
];

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES, { useHash: true});
