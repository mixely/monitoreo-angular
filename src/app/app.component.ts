import { Component, OnDestroy } from '@angular/core';
import { LoginService } from './services/login.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnDestroy {
  loged:boolean = false;
  constructor(
    private _ls:LoginService
  ) {
    this.loged = this._ls.compruebaValidez();
    // Obtenemos la URL actual para verificar que no estemos en el login. En caso no estar ahí entonces redireccionamos.
    let url = window.location.href.split("#")[1].split("/")[1];
    if(this.loged==false && url!="login"){
      this._ls.redireccionar();
    }
  }

  ngOnDestroy(){
  }
}
