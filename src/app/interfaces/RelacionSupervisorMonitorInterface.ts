export interface RelacionSupervisorMonitorInterface {
    id?:string;
    fecha ?:string;
    supervisor?:string;
    monitor?:string;
    estado?:string;
}
