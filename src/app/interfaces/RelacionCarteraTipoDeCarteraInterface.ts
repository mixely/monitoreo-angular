export interface RelacionCarteraTipoDeCarteraInterface {
  id?:string;
  fecha?:string;
  tipo_de_cartera?:string;
  proveedor?:string;
  cartera?:string;
  descripcion?:string;
  estado?:string;
}
