export interface CriteriosdeEvaluacionInterface {
    id?:string;
    fecha?:string;
    nombre?:string;
    prioridad?:string;
    peso?:string;
    descripcion?:string;
    estado?:string;
}
