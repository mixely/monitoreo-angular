export interface RelacionEquipoTrabajoCarteraInterface {
   id?:string;
   id_proveedor?:string;
   id_cartera?:string;
   id_perfil?:string;
   id_usuario?:string;
   id_equipo?:string;
   id_segmento?:string;
}
