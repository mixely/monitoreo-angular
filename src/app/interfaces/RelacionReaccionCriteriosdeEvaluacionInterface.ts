export interface RelacionReaccionCriteriosdeEvaluacionInterface {
    id?:string;
    fecha?:string;
    reaccion?:string;
    criterio_de_evaluacion?:string;
    descripcion?:string;
    estado?:string;
}
