export interface MenuItemInterface {
  id?:string;
  nombre?:string;
  url?:string;
  imagen?:string;
}
