export interface RelacionMonitorCarteraInterface {
id?:string;
fecha?:string;
monitor?:string;
tipo_de_cartera?:string;
proveedor?:string;
cartera?:string;
antiguo?:string;
nuevo?:string;
transito?:string;
asignacion?:string;
estado?:string;
}
