export interface EquiposDeTrabajoInterface {
  id?:string;
  fecha_de_ingreso?:string;
  proveedor?:string;
  cartera?:string;
  agente?:string;
  turno?:string;
  perfil?:string;
  condicion?:string;
  estado?:string;
}
