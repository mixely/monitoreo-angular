export interface PerfildeMonitorInterface {
  id?:string;
  fecha?:string;
  nombre?:string;
  asignacion_mensual?:string;
  produccion_l_v?:string;
  produccion_sabado?:string;
  descripcion?:string;
  estado?:string;
}
