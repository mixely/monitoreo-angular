export interface RelacionProductoCarteraInterface {
   id?:string;
   id_proveedor?:string;
   id_cartera?:string;
   id_producto?:string;
   estado?:string;
}
