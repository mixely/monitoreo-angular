export interface DiadeCorteporCarteraInterface {
    id?:string;
    tipo_cartera?:string;
    proveedor?:string;
    cartera?:string;
    dia_de_corte?:string;
    estado?:string;
}
