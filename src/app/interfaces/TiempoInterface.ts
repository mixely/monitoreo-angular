export interface TiempoInterface {
  id?:string;
  fecha?:string;
  analizar_historial?:string;
  presentar_informe?:string;
  estado?:string;
}
