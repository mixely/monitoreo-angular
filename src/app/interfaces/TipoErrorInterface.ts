export interface TipoErrorInterface{
    id?:string;
    fecha?:string;
    prioridad?:string;
    nombre?:string;
    descripcion?:string;
    estado?:string;
}
