export interface DetalleDeAsignacionInterface {
id?:string;
periodo?:string;
fecha_de_asignacion?:string;
monitor?:string;
tipo_de_cartera?:string;
proveedor?:string;
cartera?:string;
agente?:string;
nro_de_evaluacion?:string;
nro_de_audio?:string;
reaccion?:string;
dni?:string;
telefono?:string;
fecha_de_llamada?:string;
fecha_de_evaluacion?:string;
audio?:string;
resultado?:string;
porcent_alcance?:string;
}
