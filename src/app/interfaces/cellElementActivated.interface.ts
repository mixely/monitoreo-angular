export interface CellActivated {
  selected: boolean;
  colActivated: boolean;
  value: number;
}
