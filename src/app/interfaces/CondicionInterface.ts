export interface CondicionInterface {
    id?:string;
    fecha ?:string;
    tipo_de_cartera?:string;
    condicion?:string;
    empresa?:string;
    cartera?:string;
    nro_de_evaluaciones?:string;
    nro_de_audios?:string;
    intervalo_de_dias?:string;
    estado?:string;
}
