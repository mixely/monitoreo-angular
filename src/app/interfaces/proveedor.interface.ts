export interface ProveedorInterface {
  id?:string;
  razon_social?:string;
  ruc?:string;
  id_rubro?:number;
  telefono1?:string;
  telefono2?:string;
  telefono3?:string;
  representante?:string;
  contacto?:string;
  telf_contacto?:string;
  estado?:string;
}
