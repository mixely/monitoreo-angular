export interface TipoCarteraInterface {
  id?:string;
  fecha?:string;
  nombre?:string;
  descripcion?:string;
  estado?:string;
}
