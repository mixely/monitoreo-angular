export interface UsuarioInterface {
  id?: string;
  dni?: string;
  ap_paterno?: string;
  ap_materno?: string;
  nombres?: string;
  celular?: string;
  fijo?: string;
  fecnac?: string;
  est_civil?: string;
  direccion?: string;
  ubigeo?: string;
  email_corp?: string;
  email_per?: string;
  contacto_emergencia?: string;
  telef_contacto?: string;
  login?: string;
  idusuario_sede?: string;
  idproveedor?: string;
  idcartera?: string;
  proveedor?: string;
  cartera?: string;
  fec_ingreso?: string;
  perfil?: string;
  turno?: string;
  estado?: string;
}
