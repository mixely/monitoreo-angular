export interface MenuContenedorInterface {
  id?:string;
  id_perfil_menu_contenedor?:string;
  nombre?:string;
  imagen?:string;
  nivel?:string;
  orden?:string;
  id_menu_contenedor?:string;
}
