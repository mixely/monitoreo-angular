export interface ReaccionInterface {
  id?:string;
  fecha?:string;
  nombre?:string;
  porcent_de_aprobacion?:string;
  descripcion?:string;
  estado?:string;
}
