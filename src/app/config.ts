import { Injectable } from '@angular/core';

@Injectable()
export class Settings {
  url:string =  "";
  email:string = "";
  constructor(){
    this.url = 'http://monitoreo.dev/api';
    // this.url = 'http://192.168.30.156/api';
    this.email = 'menriquez@kobsa.com.pe';
  }
}
