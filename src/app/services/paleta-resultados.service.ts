import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Settings } from '../config';
import { LoginService } from './login.service'
import { PaletaResultadosInterface } from '../interfaces/paleta-resultados.interface';
import 'rxjs/add/operator/map';

@Injectable()
export class PaletaResultadosService {

  constructor(private sett:Settings,
              private _ls:LoginService,
              private http:Http) { }


  set(form:PaletaResultadosInterface){
    let token = this._ls.getToken();
    let body = JSON.stringify(form);
    let headers = new Headers({
      'Content-Type': 'application/json'
    });
    return this.http.post(`${this.sett.url}/paleta-resultados?token=${token}`, body, { headers })
                    .map(data => {
                      return data.json();
                    })
  }

  get(){
    let token = this._ls.getToken();
    return this.http.get(`${this.sett.url}/paleta-resultados?token=${token}`)
                    .map(data => {
                      return data.json();
                    })
  }

  update( form:PaletaResultadosInterface ){
    let token = this._ls.getToken();
    let body = JSON.stringify(form);
    let headers = new Headers({
      'Content-Type': 'application/json'
    });
    return this.http.put(`${this.sett.url}/paleta-resultados?token=${token}`, body, { headers })
                    .map( data => {
                      return data.json();
                    })
  }

/*  getSegmentosDeCartera(id){
let token = this._ls.getToken();
    // let body = JSON.stringify(id);
    // let headers = new Headers({
    //   'Content-Type': 'application/json'
    // });

    return this.http.get(`${this.sett.url}/cartera/${id}`)
                    .map(res =>{
                      return res.json();
                    });
  }
*/
}
