import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Settings } from '../config';
import { ProveedorInterface } from '../interfaces/proveedor.interface';
import { LoginService } from './login.service'
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class CargaValidacionService {

  constructor(private http:Http,
              private _ls:LoginService,
              private sett:Settings) { }

  upload( dat:any, nombre_asignacion:string ){
    let token = this._ls.getToken();
    if(dat.length > 0) {
      
        let file: File = dat[0];
        let body:FormData = new FormData();

        body.append('archivo', file, file.name);
        body.append('nombre_asignacion', nombre_asignacion);

        let headers = new Headers({
          'Content-Type': 'application/json'
        });
        // let options = new RequestOptions({ headers: headers });

        return this.http.post(`${this.sett.url}/cargar-asignacion?token=${token}`, body, headers)
                        .map(res => {
                          return res.json();
                        });

    }
    // let body = JSON.stringify(form);

    // return this.http.post(`${this.sett.url}/proveedor`, body, { headers })
    //                 .map(res => {
    //                   console.log(res.json());
    //                   return res.json();
    //                 });
  }


}
