import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { LoginService } from './login.service'
import 'rxjs/add/operator/map';
import { Settings } from '../config';

@Injectable()
export class UbigeoService {

  constructor(private http:Http,
              private _ls:LoginService,
              private sett:Settings) { }

  cargarUbigeo(term:string){
    let token = this._ls.getToken();
    let obj = {term: term};
    let body = JSON.stringify(obj);
    let headers = new Headers({
      'Content-Type': 'application/json'
    });

    return this.http.post(`${this.sett.url}/ubigeo?token=${token}`, body, { headers })
                    .map(res => {
                      return res.json();
                    });
  }
}
