import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Settings } from '../config';
import { RubroInterface } from '../interfaces/rubro.interface';
import { LoginService } from './login.service'
import 'rxjs/add/operator/map';


@Injectable()
export class RubroService {

  constructor(private http:Http,
              private _ls:LoginService,
              private sett:Settings) { }

  get(){
    let token = this._ls.getToken();
    return this.http.get(`${this.sett.url}/rubro?token=${token}`)
                    .map(res =>{
                      return res.json();
                    });
  }

  set( form:RubroInterface ){
    let token = this._ls.getToken();
    let body = JSON.stringify(form);
    let headers = new Headers({
      'Content-Type': 'application/json'
    });

    return this.http.post(`${this.sett.url}/rubro?token=${token}`, body, { headers })
                    .map(res => {
                      return res.json();
                    });
  }

  update( form:RubroInterface ){
    let token = this._ls.getToken();
    let body = JSON.stringify(form);
    let headers = new Headers({
      'Content-Type': 'application/json'
    });

    return this.http.put(`${this.sett.url}/rubro?token=${token}`, body, { headers })
                    .map(res => {
                      return res.json();
                    });
  }

}
