import { Injectable } from '@angular/core';

@Injectable()
export class DiaCorteService {

  constructor() { }

  errores:any[] = [
     {
        code: "23000",
        msj: "El registro no se realizó por que ya existe"
     }
  ];

  getErrores(code:string) {
     let msj = `Error número ${ code } no reconocido. Informar a soporte técnico`;
     this.errores.forEach(function(obj){
       if(obj.code==code){
          msj = obj.msj;
       }
     });
     return msj;
  }

}
