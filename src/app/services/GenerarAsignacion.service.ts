import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Settings } from '../config';
import { CarteraInterface } from '../interfaces/cartera.interface';
import { LoginService } from './login.service'
import { TipoCarteraInterface } from '../interfaces/tipo-cartera.interface';
import 'rxjs/add/operator/map'

@Injectable()
export class GenerarAsignacionService {

constructor(private sett:Settings,
              private _ls:LoginService,
              private http:Http) { }

  cargarTiposGenerarAsignacion(){
    let token = this._ls.getToken();
    return this.http.get(`${this.sett.url}/tipos-cartera?token=${token}`)
                    .map(data => {
                      return data.json();
                    })
  }

  setTipoCartera(form:TipoCarteraInterface){
    let token = this._ls.getToken();
    let body = JSON.stringify(form);
    let headers = new Headers({
      'Content-Type': 'application/json'
    });
    return this.http.post(`${this.sett.url}/tipos-cartera?token=${token}`, body, { headers })
                    .map(res => {
                      return res.json();
                    });
  }

  // Obtenemos las carteras de un proveedor
  getCarteras(id_proveedor){
    let token = this._ls.getToken();
    return this.http.get(`${this.sett.url}/cartera/proveedor/${id_proveedor}?token=${token}`)
                    .map(res =>{
                      return res.json();
                    });
  }

  updateTipoCartera(form:TipoCarteraInterface){
    let token = this._ls.getToken();
    let body = JSON.stringify(form);
    let headers = new Headers({
      'Content-Type': 'application/json'
    });
    return this.http.put(`${this.sett.url}/tipos-cartera?token=${token}`, body, { headers })
                    .map(res => {
                      return res.json();
                    });
  }

  set(form:CarteraInterface){
    let token = this._ls.getToken();
    let body = JSON.stringify(form);
    let headers = new Headers({
      'Content-Type': 'application/json'
    });
    return this.http.post(`${this.sett.url}/cartera?token=${token}`, body, { headers })
                    .map(data => {
                      return data.json();
                    })
  }

  get(){
    let token = this._ls.getToken();
    return this.http.get(`${this.sett.url}/cartera?token=${token}`)
                    .map(data => {
                      return data.json();
                    })
  }

  update( form:CarteraInterface ){
    let token = this._ls.getToken();
    let body = JSON.stringify(form);
    let headers = new Headers({
      'Content-Type': 'application/json'
    });
    return this.http.put(`${this.sett.url}/cartera?token=${token}`, body, { headers })
                    .map( data => {
                      return data.json();
                    })
  }

  getSegmentosDeCartera(id){
    let token = this._ls.getToken();
    return this.http.get(`${this.sett.url}/cartera/${id}?token=${token}`)
                    .map(res =>{
                      return res.json();
                    });
  }

}
