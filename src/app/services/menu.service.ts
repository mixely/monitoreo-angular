import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { Settings } from '../config';
import { LoginService } from './login.service';

import { MenuContenedorBaseInterface } from '../interfaces/menu-contenedor-base.interface';
import { MenuContenedorInterface } from '../interfaces/menu-contenedor.interface';
import { MenuItemInterface } from '../interfaces/menu-item.interface';
import { MenuContenedorItemInterface } from '../interfaces/menu-contenedor-item.interface';

@Injectable()
export class MenuService {

  constructor(private http:Http,
              private _ls:LoginService,
              private sett:Settings) { }


  setMenuContenedorBase(form:MenuContenedorBaseInterface){

    let token = this._ls.getToken();
    let body = JSON.stringify(form);
    let headers = new Headers({
      'Content-Type': 'application/json'
    });

    return this.http.post(`${this.sett.url}/menu/perfil-menu-contenedor-base?token=${token}`, body, { headers })
                    .map(res => {
                      return res.json();
                    });
  }

  getMenuContenedorBase(){
    let token = this._ls.getToken();
    return this.http.get(`${this.sett.url}/menu/perfil-menu-contenedor-base?token=${token}`)
                    .map(res => {
                      return res.json();
                    });
  }

  updateMenuContenedorBase(form:any){
    let token = this._ls.getToken();
    let body = JSON.stringify(form);
    let headers = new Headers({
      'Content-Type': 'application/json'
    });
    return this.http.put(`${this.sett.url}/menu/perfil-menu-contenedor-base?token=${token}`, body, { headers })
                    .map( data => {
                      return data.json();
                    })
  }

  setMenuContenedores(form:MenuContenedorInterface){
    let token = this._ls.getToken();
    let body = JSON.stringify(form);
    let headers = new Headers({
      'Content-Type': 'application/json'
    });
    return this.http.post(`${this.sett.url}/menu/menu-contenedor?token=${token}`, body, { headers })
                    .map(res => {
                      return res.json();
                    });
  }

  getMenuContenedores(){
    let token = this._ls.getToken();
    return this.http.get(`${this.sett.url}/menu/menu-contenedor?token=${token}`)
                    .map(res => {
                      return res.json();
                    });
  }

  updateMenuContenedor(form:any){
    let token = this._ls.getToken();
    let body = JSON.stringify(form);
    let headers = new Headers({
      'Content-Type': 'application/json'
    });
    return this.http.put(`${this.sett.url}/menu/menu-contenedor?token=${token}`, body, { headers })
                    .map( data => {
                      return data.json();
                    })
  }

  setMenuItem(form:MenuItemInterface){
    let token = this._ls.getToken();
    let body = JSON.stringify(form);
    let headers = new Headers({
      'Content-Type': 'application/json'
    });
    return this.http.post(`${this.sett.url}/menu/menu-item?token=${token}`, body, { headers })
                    .map(res => {
                      return res.json();
                    });
  }

  getMenuItem(){
    let token = this._ls.getToken();
    return this.http.get(`${this.sett.url}/menu/menu-item?token=${token}`)
                    .map(res => {
                      return res.json();
                    });
  }

  getMenuContenedorItem(){
    let token = this._ls.getToken();
    return this.http.get(`${this.sett.url}/menu/menu-contenedor-item?token=${token}`)
                    .map(res => {
                      return res.json();
                    });
  }

  setMenuContenedorItem(form:MenuContenedorItemInterface){
     let token = this._ls.getToken();
     let body = JSON.stringify(form);
     let headers = new Headers({
        'Content-Type': 'application/json'
     });
     return this.http.post(`${this.sett.url}/menu/menu-contenedor-item?token=${token}`, body, { headers })
     .map(res => {
        return res.json();
     });
  }

  cargarContenedores(nivel:string){
    let token = this._ls.getToken();
    return this.http.get(`${this.sett.url}/menu/menu-contenedor/contenedores?token=${token}&nivel=${nivel}`)
                    .map(res => {
                      return res.json();
                    });

  }
}
