import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Settings } from '../config';
import { LoginService } from './login.service'
import { RelacionEquipoTrabajoCarteraInterface } from '../interfaces/relacion-equipo-trabajo-cartera.interface';
import 'rxjs/add/operator/map';

@Injectable()
export class RelacionEquipoTrabajoCarteraService {

  constructor(private http:Http,
              private _ls:LoginService,
              private sett:Settings) { }

     get(){
       let token = this._ls.getToken();
       return this.http.get(`${this.sett.url}/relacion-equipo-trabajo-cartera?token=${token}`)
                       .map(res =>{
                         return res.json();
                       });
     }

    set( form:RelacionEquipoTrabajoCarteraInterface ){
      let token = this._ls.getToken();
       console.log(form);
       let body = JSON.stringify(form);
       let headers = new Headers({
         'Content-Type': 'application/json'
       });

       return this.http.post(`${this.sett.url}/relacion-equipo-trabajo-cartera?token=${token}`, body, { headers })
                    .map(res => {
                      return res.json();
                    });
   }

  update( form:RelacionEquipoTrabajoCarteraInterface ){
    let token = this._ls.getToken();
    let body = JSON.stringify(form);
    let headers = new Headers({
      'Content-Type': 'application/json'
    });

    return this.http.put(`${this.sett.url}/relacion-equipo-trabajo-cartera?token=${token}`, body, { headers })
                    .map(res => {
                      return res.json();
                    });
  }

}
