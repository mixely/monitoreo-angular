import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Settings } from '../config';
// import { CarteraInterface } from '../interfaces/cartera.interface';
import { LoginService } from './login.service'
import { TipoProductoInterface } from '../interfaces/tipo-producto.interface';
import { ProductoInterface } from '../interfaces/producto.interface';
import 'rxjs/add/operator/map';


@Injectable()
export class ProductoService {

  constructor(private sett:Settings,
              private _ls:LoginService,
              private http:Http) { }

  cargarTipoProductos(){
    let token = this._ls.getToken();
    return this.http.get(`${this.sett.url}/tipo-productos?token=${token}`)
                    .map(data => {
                      return data.json();
                    })
  }

  setTipoProducto(form:TipoProductoInterface){
    let token = this._ls.getToken();
    let body = JSON.stringify(form);
    let headers = new Headers({
      'Content-Type': 'application/json'
    });
    return this.http.post(`${this.sett.url}/tipo-productos?token=${token}`, body, { headers })
                    .map(res => {
                      return res.json();
                    });
  }

  updateTipoProducto(form:TipoProductoInterface){
    let token = this._ls.getToken();
    let body = JSON.stringify(form);
    let headers = new Headers({
      'Content-Type': 'application/json'
    });
    return this.http.put(`${this.sett.url}/tipo-productos?token=${token}`, body, { headers })
                    .map(res => {
                      return res.json();
                    });
  }

  set(form:ProductoInterface){
    let token = this._ls.getToken();
    let body = JSON.stringify(form);
    let headers = new Headers({
      'Content-Type': 'application/json'
    });

    return this.http.post(`${this.sett.url}/producto?token=${token}`, body, { headers })
                    .map(data => {
                      return data.json();
                    })
  }

  get(){
    let token = this._ls.getToken();
    return this.http.get(`${this.sett.url}/producto?token=${token}`)
                    .map(data => {
                      return data.json();
                    })
  }

  update(form:ProductoInterface){
    let token = this._ls.getToken();
    let body = JSON.stringify(form);
    let headers = new Headers({
      'Content-Type': 'application/json'
    });
    return this.http.put(`${this.sett.url}/producto?token=${token}`, body, { headers })
                    .map(data => {
                      return data.json();
                    })
  }

}
