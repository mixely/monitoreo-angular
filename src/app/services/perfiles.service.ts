import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { Settings } from '../config';
import { LoginService } from './login.service'
import { RequestService } from './request.service'
import { PerfilInterface } from '../interfaces/perfil';

@Injectable()
export class PerfilesService {

  constructor(private http:Http,
              private _request:RequestService,
              private _ls:LoginService,
              private sett:Settings) { }

  set( obj:PerfilInterface ){
    let ruta = "perfil";
    return this._request.set(ruta, obj);
  }

  get(){
    let ruta = "perfil";
    return this._request.get(ruta);
  }

}
