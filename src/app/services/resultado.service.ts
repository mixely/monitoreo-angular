import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Settings } from '../config';
import { LoginService } from './login.service'
import { ResultadoInterface } from '../interfaces/resultado.interface';
import 'rxjs/add/operator/map';

@Injectable()
export class ResultadoService {

  constructor(private http:Http,
              private _ls:LoginService,
              private sett:Settings) { }

  get(){
    let token = this._ls.getToken();
    return this.http.get(`${this.sett.url}/resultado?token=${token}`)
                    .map(res =>{
                      return res.json();
                    })
  }

  set( form:ResultadoInterface ){
    let token = this._ls.getToken();
    let body = JSON.stringify(form);
    let headers = new Headers({
      'Content-Type': 'application/json'
    });
    return this.http.post(`${this.sett.url}/resultado?token=${token}`, body, { headers })
                    .map(res => {
                      console.log(res.json());
                      return res.json();
                    });
  }

  update( form:ResultadoInterface ){
    let token = this._ls.getToken();
    let body = JSON.stringify(form);
    let headers = new Headers({
      'Content-Type': 'application/json'
    });
    return this.http.put(`${ this.sett.url }/resultado?token=${token}`, body, { headers })
                    .map(res => {
                      console.log(res.json());
                      return res.json();
                    });
  }

}
