import { Injectable } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Http, Headers } from '@angular/http';
import { Settings } from '../config';
import { LoginInterface } from '../interfaces/login.interface';
import 'rxjs/add/operator/map';

// import { LoginService } from './login.service'
// private _ls:LoginService,
// let token = this._ls.getToken();
// ?token=${token}

@Injectable()
export class LoginService {

  constructor(private http:Http,
              private router:Router,
              private ar:ActivatedRoute,
              private sett:Settings){}


  auth( form:LoginInterface ) {
    let body = JSON.stringify(form);
    let headers = new Headers({
      'Content-Type': 'application/json'
    });

    return this.http.post(`${this.sett.url}/login/auth`, body, { headers })
                    .map(res => {
                      let data = res.json();
                      if(data.token){
                        this.setToken(data.token);
                        this.setLoged(true);
                        this.setUser(data.user);
                        this.setMenu(data.menu);
                        this.setEstadoToken("vigente");
                        return true;
                      } else {
                        this.setToken(data.token);
                        this.setLoged(true);
                        return false;
                      }
                    });
  }

  compruebaValidez(){
    if(this.getToken()=="" || this.getToken()==null || this.getToken()=="undefined" || this.getLoged()==false ) {
      return false;
    }
    return true;
  }

  setToken( token:string ){
    localStorage.setItem('token', token);
  };

  setLoged( state:boolean ){
    localStorage.setItem('loged', `${state}`);
  };

  setEstadoToken(estado:string){
    localStorage.setItem('estado_token', `${estado}`);
  }

  setUser(datos){
    localStorage.setItem('user', JSON.stringify(datos));
  }

  getUser(){
    return JSON.parse(localStorage.getItem('user'))[0];
  }

  setMenu(datos){
    localStorage.setItem('menu', JSON.stringify(datos));
  }

  getMenu(){
    return JSON.parse(localStorage.getItem('menu'))[0];
  }

  getEstadoToken(){
    return localStorage.getItem('estado_token');
  }

  getToken():string {
    return localStorage.getItem('token');
  };

  getLoged():boolean {
    return JSON.parse(localStorage.getItem('loged'));
  };
// this.redireccionar();
  redireccionar(){
    // por defecto se redirecciona al login.
    this.router.navigate(['/login']);
    window.location.reload();
  };

  handleError( data:any ){
    if(data.status == 404){
      console.log("No se ha encontrado una ruta");
      console.log(data.url);
   }else if(data.json().error=="token_expired"){
      this.setToken("");
      this.setEstadoToken("caducado");
      this.setLoged(false);
      this.setUser("");
      this.setMenu("");
      // this.redireccionar();
    }else{
      console.log("-----------------");
      console.log(data);
      console.log("-----------------");
    }
  };

  reset( form:LoginInterface ){
    let body = JSON.stringify(form);
    let headers = new Headers({
      'Content-Type': 'application/json'
    });

    return this.http.post(`${this.sett.url}/login/reset`, body, { headers })
                    .map(res => {
                      return res.json();
                    });
  };

}
