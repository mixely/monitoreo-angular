import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import "rxjs/add/operator/map";

@Injectable()
export class GetAccessService {
  resultado:string = "";
  url:string = "http://localhost:3000/api/recurso";

  constructor(private http:Http) { }

  getToken(){
    return this.http.get(this.url)
                    .map(res => this.resultado = res.json());
  }

}
