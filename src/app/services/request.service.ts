import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { Settings } from '../config';
import { LoginService } from './login.service'

@Injectable()
export class RequestService {

  constructor(
                private http:Http,
                private _ls:LoginService,
                private sett:Settings
  ) { }

  set( ruta:string, obj:any ) {
    let token = this._ls.getToken();
    let body = JSON.stringify( obj );
    let headers = new Headers({
      'Content-Type': 'application/json'
    });
    return this.http.post(`${this.sett.url}/${ruta}?token=${token}`, body, { headers })
                    .map( res => {
                      return res.json();
                    });

  }

  get( ruta:string ){
    let token = this._ls.getToken();
    return this.http.get(`${this.sett.url}/${ruta}?token=${token}`)
                    .map(res => {
                      return res.json();
                    });
  }

  update( ruta:string, obj:any ){
    let token = this._ls.getToken();
    let body = JSON.stringify( obj );
    let headers = new Headers({
      'Content-Type': 'application/json'
    });
    return this.http.put(`${this.sett.url}/${ruta}?token=${token}`, body, { headers })
                    .map( res => {
                      return res.json();
                    });
  }

  handleError(Err:any){
    console.log(Err);
    if(Err.json().error=="token_expired"){
      this._ls.setToken("");
      this._ls.setLoged(false);
      this._ls.redireccionar();
    }
  }

}
