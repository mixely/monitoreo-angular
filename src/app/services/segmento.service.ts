import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Settings } from '../config';
import { SegmentoInterface } from '../interfaces/segmento.interface';
import { LoginService } from './login.service'
import 'rxjs/add/operator/map';


@Injectable()
export class SegmentoService {

  constructor(private http:Http,
              private _ls:LoginService,
              private sett:Settings) { }

  get(){
    let token = this._ls.getToken();
    return this.http.get(`${this.sett.url}/segmento?token=${token}`)
                    .map(res =>{
                      return res.json();
                    });
  }

  set( form:SegmentoInterface ){
    let token = this._ls.getToken();
    let body = JSON.stringify(form);
    let headers = new Headers({
      'Content-Type': 'application/json'
    });

    return this.http.post(`${this.sett.url}/segmento?token=${token}`, body, { headers })
                    .map(res => {
                      return res.json();
                    });
  }

  // cargar segmentos de carteras
  getSegmentos(id_cartera){
    let token = this._ls.getToken();
    return this.http.get(`${this.sett.url}/segmento/carteras/${id_cartera}?token=${token}`)
                     .map(res =>{
                       return res.json();
                     });
 }

  update( form:SegmentoInterface ){
    let token = this._ls.getToken();
    let body = JSON.stringify(form);
    let headers = new Headers({
      'Content-Type': 'application/json'
    });

    return this.http.put(`${this.sett.url}/segmento?token=${token}`, body, { headers })
                    .map(res => {
                      return res.json();
                    });
  }

}
