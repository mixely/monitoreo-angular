import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Settings } from '../config';
import { LoginService } from './login.service'
import { CarteraInterface } from '../interfaces/cartera.interface';
import { TipoCarteraInterface } from '../interfaces/tipo-cartera.interface';
import 'rxjs/add/operator/map';

@Injectable()
export class RelacionTipoContactoResultadoService {

  constructor(private sett:Settings,
              private _ls:LoginService,
              private http:Http) { }


  set(form:CarteraInterface){
    let token = this._ls.getToken();
    let body = JSON.stringify(form);
    let headers = new Headers({
      'Content-Type': 'application/json'
    });
    return this.http.post(`${this.sett.url}/relacion-tipo-contacto-resultado?token=${token}`, body, { headers })
                    .map(data => {
                      return data.json();
                    });
  }

  get(){
    let token = this._ls.getToken();
    return this.http.get(`${this.sett.url}/relacion-tipo-contacto-resultado?token=${token}`)
                    .map(data => {
                      return data.json();
                    })
  }

  update( form:CarteraInterface ){
    let token = this._ls.getToken();
    let body = JSON.stringify(form);
    let headers = new Headers({
      'Content-Type': 'application/json'
    });
    return this.http.put(`${this.sett.url}/relacion-tipo-contacto-resultado?token=${token}`, body, { headers })
                    .map( data => {
                      return data.json();
                    })
  }

}
