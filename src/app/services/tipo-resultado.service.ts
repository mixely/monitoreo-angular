import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Settings } from '../config';
import { LoginService } from './login.service'
import { TipoResultadoInterface } from '../interfaces/tipo-resultado.interface';
import 'rxjs/add/operator/map';

@Injectable()
export class TipoResultadoService {

  constructor(private http:Http,
              private _ls:LoginService,
              private sett:Settings) { }

  getTipoResultados(){
    let token = this._ls.getToken();
    return this.http.get(`${this.sett.url}/tipos-resultado?token=${token}`)
                    .map(res =>{
                      return res.json();
                    });
  }

  setTipoResultado( form:TipoResultadoInterface ){
    let token = this._ls.getToken();
    let body = JSON.stringify(form);
    let headers = new Headers({
      'Content-Type': 'application/json'
    });

    return this.http.post(`${this.sett.url}/tipos-resultado?token=${token}`, body, { headers })
                    .map(res => {
                      return res.json();
                    });
  }

  updateTipoResultado( form:TipoResultadoInterface ){
    let token = this._ls.getToken();
    let body = JSON.stringify(form);
    let headers = new Headers({
      'Content-Type': 'application/json'
    });

    return this.http.put(`${this.sett.url}/tipos-resultado?token=${token}`, body, { headers })
                    .map(res => {
                      return res.json();
                    });
  }

}
