import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
//import {  } from '@angular/animations';
import { HttpModule } from '@angular/http';

// servicios
import { Settings } from './config';
import { PerfilesService } from './services/perfiles.service';
import { UsuarioService } from './services/usuario.service';
import { ProveedorService } from './services/proveedor.service';
import { UbigeoService } from './services/ubigeo.service';
import { RubroService } from './services/rubro.service';
import { CarteraService } from './services/cartera.service';
import { ProductoService } from './services/producto.service';
import { ErroresService } from './services/errores.service';
import { TipoResultadoService } from './services/tipo-resultado.service';
import { TipoDireccionService } from './services/tipo-direccion.service';
import { TipoTelefonoService } from './services/tipo-telefono.service';
import { TipoContactoService } from './services/tipo-contacto.service';
import { ResultadoService } from './services/resultado.service';
import { JustificacionService } from './services/justificacion.service';
import { EquipoTrabajoService } from './services/equipo-trabajo.service';
import { SegmentoService } from './services/segmento.service';
import { RelacionEquipoTrabajoCarteraService } from './services/relacion-equipo-trabajo-cartera.service';
import { RelacionProductoCarteraService } from './services/relacion-producto-cartera.service';
import { RelacionProductoProveedorService } from './services/relacion-producto-proveedor.service';
import { RelacionTipoContactoResultadoService } from './services/relacion-tipo-contacto-resultado.service';
import { PaletaResultadosService } from './services/paleta-resultados.service';
import { CargaValidacionService } from './services/carga-validacion.service';
import { LoginService } from './services/login.service';
import { MenuService } from './services/menu.service';
import { RequestService } from './services/request.service';

import { AppComponent } from './app.component';
import { HeaderComponent } from './component/templates/header-component/header-component';
import { BodyComponent } from './component/templates/body-component/body.component';

// Sidebars
import { SidebarComponent } from './component/templates/shared/sidebar/sidebar.component';

// NgPrime
import { DataTableModule, SharedModule, DialogModule,
        AutoCompleteModule, ContextMenuModule, ButtonModule,
        GrowlModule, CalendarModule, DropdownModule, FileUploadModule, TabViewModule } from 'primeng/primeng';

// Ruta general
import { APP_ROUTING } from './app.router';

// Administrador
import { PerfilComponent } from './component/perfil/perfil.component';
import { UsuarioComponent } from './component/usuario/usuario.component';
import { ProveedorComponent } from './component/proveedor/proveedor.component';
import { CarteraComponent } from './component/cartera/cartera.component';
import { SegmentoComponent } from './component/segmento/segmento.component';
import { ProductoComponent } from './component/producto/producto.component';
import { ResultadoComponent } from './component/resultado/resultado.component';
import { JustificacionComponent } from './component/justificacion/justificacion.component';

import { RubroComponent } from './component/rubro/rubro.component';
import { TipoCarteraComponent } from './component/tipo-cartera/tipo-cartera.component';
import { TipoContactoComponent } from './component/tipo-contacto/tipo-contacto.component';
import { TipoDireccionComponent } from './component/tipo-direccion/tipo-direccion.component';
import { TipoProductoComponent } from './component/tipo-producto/tipo-producto.component';
import { TipoResultadoComponent } from './component/tipo-resultado/tipo-resultado.component';
import { TipoTelefonoComponent } from './component/tipo-telefono/tipo-telefono.component';

import { EquipoTrabajoComponent } from './component/equipo-trabajo/equipo-trabajo.component';
import { RelacionPerfilMenuComponent } from './component/relacion-perfil-menu/relacion-perfil-menu.component';
import { RelacionEquipoTrabajoCarteraComponent } from './component/relacion-equipo-trabajo-cartera/relacion-equipo-trabajo-cartera.component';
import { RelacionProductoCarteraComponent } from './component/relacion-producto-cartera/relacion-producto-cartera.component';
import { RelacionProductoProveedorComponent } from './component/relacion-producto-proveedor/relacion-producto-proveedor.component';
import { RelacionTipoContactoResultadoComponent } from './component/relacion-tipo-contacto-resultado/relacion-tipo-contacto-resultado.component';
import { PaletaResultadoComponent } from './component/paleta-resultado/paleta-resultado.component';

import { CargaValidacionComponent } from './component/carga-validacion/carga-validacion.component';
import { EstadoAsignacionesComponent } from './component/estado-asignaciones/estado-asignaciones.component';

import { ControlCampanasComponent } from './component/control-campanas/control-campanas.component';
import { CrearCampanaComponent } from './component/crear-campana/crear-campana.component';

import { ConsultaClientesComponent } from './component/consulta-clientes/consulta-clientes.component';
import { EstadoVentasComponent } from './component/estado-ventas/estado-ventas.component';
import { ModificacionMasivaEstadosComponent } from './component/modificacion-masiva-estados/modificacion-masiva-estados.component';

import { LoginComponent } from './component/login/login.component';
import { MenuHandleComponent } from './component/menu-handle/menu-handle.component';



@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    BodyComponent,
    SidebarComponent,
    PerfilComponent,
    UsuarioComponent,
    CarteraComponent,
    JustificacionComponent,
    ProductoComponent,
    ProveedorComponent,
    ResultadoComponent,
    RubroComponent,
    TipoCarteraComponent,
    TipoContactoComponent,
    TipoDireccionComponent,
    TipoProductoComponent,
    TipoResultadoComponent,
    TipoTelefonoComponent,
    EquipoTrabajoComponent,
    RelacionPerfilMenuComponent,
    RelacionEquipoTrabajoCarteraComponent,
    RelacionProductoCarteraComponent,
    RelacionProductoProveedorComponent,
    RelacionTipoContactoResultadoComponent,
    PaletaResultadoComponent,
    CargaValidacionComponent,
    EstadoAsignacionesComponent,
    CrearCampanaComponent,
    ControlCampanasComponent,
    ConsultaClientesComponent,
    EstadoVentasComponent,
    ModificacionMasivaEstadosComponent,
    SegmentoComponent,
    LoginComponent,
    MenuHandleComponent
  ],
  imports: [
    BrowserModule, BrowserAnimationsModule, FormsModule, ReactiveFormsModule,
    HttpModule, DataTableModule, AutoCompleteModule, ContextMenuModule, ButtonModule,
    SharedModule, DialogModule, GrowlModule, CalendarModule, DropdownModule, FileUploadModule,
    TabViewModule,
    APP_ROUTING
  ],
  providers: [
    {provide: LOCALE_ID, useValue: "es"},
    Settings,
    PerfilesService,
    UsuarioService,
    ProveedorService,
    UbigeoService,
    RubroService,
    CarteraService,
    ProductoService,
    ErroresService,
    TipoResultadoService,
    TipoDireccionService,
    TipoTelefonoService,
    TipoContactoService,
    ResultadoService,
    JustificacionService,
    EquipoTrabajoService,
    SegmentoService,
    RelacionEquipoTrabajoCarteraService,
    RelacionProductoCarteraService,
    RelacionProductoProveedorService,
    RelacionTipoContactoResultadoService,
    PaletaResultadosService,
    CargaValidacionService,
    LoginService,
    MenuService,
    RequestService
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule { }
