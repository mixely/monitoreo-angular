const path = require('path');
const ProgressPlugin = require('webpack/lib/ProgressPlugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const autoprefixer = require('autoprefixer');
const postcssUrl = require('postcss-url');

const { NoEmitOnErrorsPlugin, LoaderOptionsPlugin } = require('webpack');
const { GlobCopyWebpackPlugin, BaseHrefWebpackPlugin } = require('@angular/cli/plugins/webpack');
const { CommonsChunkPlugin } = require('webpack').optimize;
const { AotPlugin } = require('@ngtools/webpack');

const nodeModules = path.join(process.cwd(), 'node_modules');
const entryPoints = ["inline","polyfills","sw-register","scripts","styles","vendor","main"];
const baseHref = "";
const deployUrl = "";




module.exports = {
  "devtool": "source-map",
  "resolve": {
    "extensions": [
      ".ts",
      ".js"
    ],
    "modules": [
      "./node_modules"
    ]
  },
  "resolveLoader": {
    "modules": [
      "./node_modules"
    ]
  },
  "entry": {
    "main": [
      "./src/main.ts"
    ],
    "polyfills": [
      "./src/polyfills.ts"
    ],
    "scripts": [
      "script-loader!./src/assets/global/plugins/jquery.min.js",
      "script-loader!./src/assets/global/plugins/bootstrap/js/bootstrap.min.js",
      "script-loader!./src/assets/global/plugins/js.cookie.min.js",
      "script-loader!./src/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js",
      "script-loader!./src/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js",
      "script-loader!./src/assets/global/plugins/moment.min.js",
      "script-loader!./src/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js",
      "script-loader!./src/assets/global/plugins/morris/morris.min.js",
      "script-loader!./src/assets/global/plugins/morris/raphael-min.js",
      "script-loader!./src/assets/global/plugins/counterup/jquery.waypoints.min.js",
      "script-loader!./src/assets/global/plugins/counterup/jquery.counterup.min.js",
      "script-loader!./src/assets/global/plugins/amcharts/amcharts/amcharts.js",
      "script-loader!./src/assets/global/plugins/amcharts/amcharts/serial.js",
      "script-loader!./src/assets/global/plugins/amcharts/amcharts/pie.js",
      "script-loader!./src/assets/global/plugins/amcharts/amcharts/radar.js",
      "script-loader!./src/assets/global/plugins/amcharts/amcharts/themes/light.js",
      "script-loader!./src/assets/global/plugins/amcharts/amcharts/themes/patterns.js",
      "script-loader!./src/assets/global/plugins/amcharts/amcharts/themes/chalk.js",
      "script-loader!./src/assets/global/plugins/amcharts/ammap/ammap.js",
      "script-loader!./src/assets/global/plugins/amcharts/ammap/maps/js/worldLow.js",
      "script-loader!./src/assets/global/plugins/amcharts/amstockcharts/amstock.js",
      "script-loader!./src/assets/global/plugins/fullcalendar/fullcalendar.min.js",
      "script-loader!./src/assets/global/plugins/horizontal-timeline/horizontal-timeline.js",
      "script-loader!./src/assets/global/plugins/flot/jquery.flot.min.js",
      "script-loader!./src/assets/global/plugins/flot/jquery.flot.resize.min.js",
      "script-loader!./src/assets/global/plugins/flot/jquery.flot.categories.min.js",
      "script-loader!./src/assets/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js",
      "script-loader!./src/assets/global/plugins/jquery.sparkline.min.js",
      "script-loader!./src/assets/global/plugins/jqvmap/jqvmap/jquery.vmap.js",
      "script-loader!./src/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js",
      "script-loader!./src/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js",
      "script-loader!./src/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js",
      "script-loader!./src/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js",
      "script-loader!./src/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js",
      "script-loader!./src/assets/global/plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js",
      "script-loader!./src/assets/global/scripts/app.min.js",
      "script-loader!./src/assets/pages/scripts/dashboard.min.js",
      "script-loader!./src/assets/layouts/layout4/scripts/layout.min.js",
      "script-loader!./src/assets/layouts/layout4/scripts/demo.min.js",
      "script-loader!./src/assets/layouts/global/scripts/quick-sidebar.min.js",
      "script-loader!./src/assets/layouts/global/scripts/quick-nav.min.js"
    ],
    "styles": [
      "./src/assets/global/plugins/font-awesome/css/font-awesome.min.css",
      "./src/assets/global/plugins/simple-line-icons/simple-line-icons.min.css",
      "./src/assets/global/plugins/bootstrap/css/bootstrap.min.css",
      "./src/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css",
      "./src/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css",
      "./src/assets/global/plugins/morris/morris.css",
      "./src/assets/global/plugins/fullcalendar/fullcalendar.min.css",
      "./src/assets/global/plugins/jqvmap/jqvmap/jqvmap.css",
      "./src/assets/global/css/components.min.css",
      "./src/assets/global/css/plugins.min.css",
      "./src/assets/layouts/layout4/css/layout.min.css",
      "./src/assets/layouts/layout4/css/themes/default.min.css",
      "./src/assets/layouts/layout4/css/custom.min.css",
      "./src/styles.css"
    ]
  },
  "output": {
    "path": path.join(process.cwd(), "dist"),
    "filename": "[name].bundle.js",
    "chunkFilename": "[id].chunk.js"
  },
  "module": {
    "rules": [
      {
        "enforce": "pre",
        "test": /\.js$/,
        "loader": "source-map-loader",
        "exclude": [
          /\/node_modules\//
        ]
      },
      {
        "test": /\.json$/,
        "loader": "json-loader"
      },
      {
        "test": /\.html$/,
        "loader": "raw-loader"
      },
      {
        "test": /\.(eot|svg)$/,
        "loader": "file-loader?name=[name].[hash:20].[ext]"
      },
      {
        "test": /\.(jpg|png|gif|otf|ttf|woff|woff2|cur|ani)$/,
        "loader": "url-loader?name=[name].[hash:20].[ext]&limit=10000"
      },
      {
        "exclude": [
          path.join(process.cwd(), "src/assets/global/plugins/font-awesome/css/font-awesome.min.css"),
          path.join(process.cwd(), "src/assets/global/plugins/simple-line-icons/simple-line-icons.min.css"),
          path.join(process.cwd(), "src/assets/global/plugins/bootstrap/css/bootstrap.min.css"),
          path.join(process.cwd(), "src/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css"),
          path.join(process.cwd(), "src/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css"),
          path.join(process.cwd(), "src/assets/global/plugins/morris/morris.css"),
          path.join(process.cwd(), "src/assets/global/plugins/fullcalendar/fullcalendar.min.css"),
          path.join(process.cwd(), "src/assets/global/plugins/jqvmap/jqvmap/jqvmap.css"),
          path.join(process.cwd(), "src/assets/global/css/components.min.css"),
          path.join(process.cwd(), "src/assets/global/css/plugins.min.css"),
          path.join(process.cwd(), "src/assets/layouts/layout4/css/layout.min.css"),
          path.join(process.cwd(), "src/assets/layouts/layout4/css/themes/default.min.css"),
          path.join(process.cwd(), "src/assets/layouts/layout4/css/custom.min.css"),
          path.join(process.cwd(), "src/styles.css")
        ],
        "test": /\.css$/,
        "loaders": [
          "exports-loader?module.exports.toString()",
          "css-loader?{\"sourceMap\":false,\"importLoaders\":1}",
          "postcss-loader"
        ]
      },
      {
        "exclude": [
          path.join(process.cwd(), "src/assets/global/plugins/font-awesome/css/font-awesome.min.css"),
          path.join(process.cwd(), "src/assets/global/plugins/simple-line-icons/simple-line-icons.min.css"),
          path.join(process.cwd(), "src/assets/global/plugins/bootstrap/css/bootstrap.min.css"),
          path.join(process.cwd(), "src/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css"),
          path.join(process.cwd(), "src/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css"),
          path.join(process.cwd(), "src/assets/global/plugins/morris/morris.css"),
          path.join(process.cwd(), "src/assets/global/plugins/fullcalendar/fullcalendar.min.css"),
          path.join(process.cwd(), "src/assets/global/plugins/jqvmap/jqvmap/jqvmap.css"),
          path.join(process.cwd(), "src/assets/global/css/components.min.css"),
          path.join(process.cwd(), "src/assets/global/css/plugins.min.css"),
          path.join(process.cwd(), "src/assets/layouts/layout4/css/layout.min.css"),
          path.join(process.cwd(), "src/assets/layouts/layout4/css/themes/default.min.css"),
          path.join(process.cwd(), "src/assets/layouts/layout4/css/custom.min.css"),
          path.join(process.cwd(), "src/styles.css")
        ],
        "test": /\.scss$|\.sass$/,
        "loaders": [
          "exports-loader?module.exports.toString()",
          "css-loader?{\"sourceMap\":false,\"importLoaders\":1}",
          "postcss-loader",
          "sass-loader"
        ]
      },
      {
        "exclude": [
          path.join(process.cwd(), "src/assets/global/plugins/font-awesome/css/font-awesome.min.css"),
          path.join(process.cwd(), "src/assets/global/plugins/simple-line-icons/simple-line-icons.min.css"),
          path.join(process.cwd(), "src/assets/global/plugins/bootstrap/css/bootstrap.min.css"),
          path.join(process.cwd(), "src/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css"),
          path.join(process.cwd(), "src/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css"),
          path.join(process.cwd(), "src/assets/global/plugins/morris/morris.css"),
          path.join(process.cwd(), "src/assets/global/plugins/fullcalendar/fullcalendar.min.css"),
          path.join(process.cwd(), "src/assets/global/plugins/jqvmap/jqvmap/jqvmap.css"),
          path.join(process.cwd(), "src/assets/global/css/components.min.css"),
          path.join(process.cwd(), "src/assets/global/css/plugins.min.css"),
          path.join(process.cwd(), "src/assets/layouts/layout4/css/layout.min.css"),
          path.join(process.cwd(), "src/assets/layouts/layout4/css/themes/default.min.css"),
          path.join(process.cwd(), "src/assets/layouts/layout4/css/custom.min.css"),
          path.join(process.cwd(), "src/styles.css")
        ],
        "test": /\.less$/,
        "loaders": [
          "exports-loader?module.exports.toString()",
          "css-loader?{\"sourceMap\":false,\"importLoaders\":1}",
          "postcss-loader",
          "less-loader"
        ]
      },
      {
        "exclude": [
          path.join(process.cwd(), "src/assets/global/plugins/font-awesome/css/font-awesome.min.css"),
          path.join(process.cwd(), "src/assets/global/plugins/simple-line-icons/simple-line-icons.min.css"),
          path.join(process.cwd(), "src/assets/global/plugins/bootstrap/css/bootstrap.min.css"),
          path.join(process.cwd(), "src/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css"),
          path.join(process.cwd(), "src/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css"),
          path.join(process.cwd(), "src/assets/global/plugins/morris/morris.css"),
          path.join(process.cwd(), "src/assets/global/plugins/fullcalendar/fullcalendar.min.css"),
          path.join(process.cwd(), "src/assets/global/plugins/jqvmap/jqvmap/jqvmap.css"),
          path.join(process.cwd(), "src/assets/global/css/components.min.css"),
          path.join(process.cwd(), "src/assets/global/css/plugins.min.css"),
          path.join(process.cwd(), "src/assets/layouts/layout4/css/layout.min.css"),
          path.join(process.cwd(), "src/assets/layouts/layout4/css/themes/default.min.css"),
          path.join(process.cwd(), "src/assets/layouts/layout4/css/custom.min.css"),
          path.join(process.cwd(), "src/styles.css")
        ],
        "test": /\.styl$/,
        "loaders": [
          "exports-loader?module.exports.toString()",
          "css-loader?{\"sourceMap\":false,\"importLoaders\":1}",
          "postcss-loader",
          "stylus-loader?{\"sourceMap\":false,\"paths\":[]}"
        ]
      },
      {
        "include": [
          path.join(process.cwd(), "src/assets/global/plugins/font-awesome/css/font-awesome.min.css"),
          path.join(process.cwd(), "src/assets/global/plugins/simple-line-icons/simple-line-icons.min.css"),
          path.join(process.cwd(), "src/assets/global/plugins/bootstrap/css/bootstrap.min.css"),
          path.join(process.cwd(), "src/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css"),
          path.join(process.cwd(), "src/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css"),
          path.join(process.cwd(), "src/assets/global/plugins/morris/morris.css"),
          path.join(process.cwd(), "src/assets/global/plugins/fullcalendar/fullcalendar.min.css"),
          path.join(process.cwd(), "src/assets/global/plugins/jqvmap/jqvmap/jqvmap.css"),
          path.join(process.cwd(), "src/assets/global/css/components.min.css"),
          path.join(process.cwd(), "src/assets/global/css/plugins.min.css"),
          path.join(process.cwd(), "src/assets/layouts/layout4/css/layout.min.css"),
          path.join(process.cwd(), "src/assets/layouts/layout4/css/themes/default.min.css"),
          path.join(process.cwd(), "src/assets/layouts/layout4/css/custom.min.css"),
          path.join(process.cwd(), "src/styles.css")
        ],
        "test": /\.css$/,
        "loaders": ExtractTextPlugin.extract({
  "use": [
    "css-loader?{\"sourceMap\":false,\"importLoaders\":1}",
    "postcss-loader"
  ],
  "fallback": "style-loader",
  "publicPath": ""
})
      },
      {
        "include": [
          path.join(process.cwd(), "src/assets/global/plugins/font-awesome/css/font-awesome.min.css"),
          path.join(process.cwd(), "src/assets/global/plugins/simple-line-icons/simple-line-icons.min.css"),
          path.join(process.cwd(), "src/assets/global/plugins/bootstrap/css/bootstrap.min.css"),
          path.join(process.cwd(), "src/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css"),
          path.join(process.cwd(), "src/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css"),
          path.join(process.cwd(), "src/assets/global/plugins/morris/morris.css"),
          path.join(process.cwd(), "src/assets/global/plugins/fullcalendar/fullcalendar.min.css"),
          path.join(process.cwd(), "src/assets/global/plugins/jqvmap/jqvmap/jqvmap.css"),
          path.join(process.cwd(), "src/assets/global/css/components.min.css"),
          path.join(process.cwd(), "src/assets/global/css/plugins.min.css"),
          path.join(process.cwd(), "src/assets/layouts/layout4/css/layout.min.css"),
          path.join(process.cwd(), "src/assets/layouts/layout4/css/themes/default.min.css"),
          path.join(process.cwd(), "src/assets/layouts/layout4/css/custom.min.css"),
          path.join(process.cwd(), "src/styles.css")
        ],
        "test": /\.scss$|\.sass$/,
        "loaders": ExtractTextPlugin.extract({
  "use": [
    "css-loader?{\"sourceMap\":false,\"importLoaders\":1}",
    "postcss-loader",
    "sass-loader"
  ],
  "fallback": "style-loader",
  "publicPath": ""
})
      },
      {
        "include": [
          path.join(process.cwd(), "src/assets/global/plugins/font-awesome/css/font-awesome.min.css"),
          path.join(process.cwd(), "src/assets/global/plugins/simple-line-icons/simple-line-icons.min.css"),
          path.join(process.cwd(), "src/assets/global/plugins/bootstrap/css/bootstrap.min.css"),
          path.join(process.cwd(), "src/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css"),
          path.join(process.cwd(), "src/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css"),
          path.join(process.cwd(), "src/assets/global/plugins/morris/morris.css"),
          path.join(process.cwd(), "src/assets/global/plugins/fullcalendar/fullcalendar.min.css"),
          path.join(process.cwd(), "src/assets/global/plugins/jqvmap/jqvmap/jqvmap.css"),
          path.join(process.cwd(), "src/assets/global/css/components.min.css"),
          path.join(process.cwd(), "src/assets/global/css/plugins.min.css"),
          path.join(process.cwd(), "src/assets/layouts/layout4/css/layout.min.css"),
          path.join(process.cwd(), "src/assets/layouts/layout4/css/themes/default.min.css"),
          path.join(process.cwd(), "src/assets/layouts/layout4/css/custom.min.css"),
          path.join(process.cwd(), "src/styles.css")
        ],
        "test": /\.less$/,
        "loaders": ExtractTextPlugin.extract({
  "use": [
    "css-loader?{\"sourceMap\":false,\"importLoaders\":1}",
    "postcss-loader",
    "less-loader"
  ],
  "fallback": "style-loader",
  "publicPath": ""
})
      },
      {
        "include": [
          path.join(process.cwd(), "src/assets/global/plugins/font-awesome/css/font-awesome.min.css"),
          path.join(process.cwd(), "src/assets/global/plugins/simple-line-icons/simple-line-icons.min.css"),
          path.join(process.cwd(), "src/assets/global/plugins/bootstrap/css/bootstrap.min.css"),
          path.join(process.cwd(), "src/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css"),
          path.join(process.cwd(), "src/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css"),
          path.join(process.cwd(), "src/assets/global/plugins/morris/morris.css"),
          path.join(process.cwd(), "src/assets/global/plugins/fullcalendar/fullcalendar.min.css"),
          path.join(process.cwd(), "src/assets/global/plugins/jqvmap/jqvmap/jqvmap.css"),
          path.join(process.cwd(), "src/assets/global/css/components.min.css"),
          path.join(process.cwd(), "src/assets/global/css/plugins.min.css"),
          path.join(process.cwd(), "src/assets/layouts/layout4/css/layout.min.css"),
          path.join(process.cwd(), "src/assets/layouts/layout4/css/themes/default.min.css"),
          path.join(process.cwd(), "src/assets/layouts/layout4/css/custom.min.css"),
          path.join(process.cwd(), "src/styles.css")
        ],
        "test": /\.styl$/,
        "loaders": ExtractTextPlugin.extract({
  "use": [
    "css-loader?{\"sourceMap\":false,\"importLoaders\":1}",
    "postcss-loader",
    "stylus-loader?{\"sourceMap\":false,\"paths\":[]}"
  ],
  "fallback": "style-loader",
  "publicPath": ""
})
      },
      {
        "test": /\.ts$/,
        "loader": "@ngtools/webpack"
      }
    ]
  },
  "plugins": [
    new NoEmitOnErrorsPlugin(),
    new GlobCopyWebpackPlugin({
      "patterns": [
        "assets",
        "favicon.ico"
      ],
      "globOptions": {
        "cwd": "/var/www/angular/dashboard/src",
        "dot": true,
        "ignore": "**/.gitkeep"
      }
    }),
    new ProgressPlugin(),
    new HtmlWebpackPlugin({
      "template": "./src/index.html",
      "filename": "./index.html",
      "hash": false,
      "inject": true,
      "compile": true,
      "favicon": false,
      "minify": false,
      "cache": true,
      "showErrors": true,
      "chunks": "all",
      "excludeChunks": [],
      "title": "Webpack App",
      "xhtml": true,
      "chunksSortMode": function sort(left, right) {
        let leftIndex = entryPoints.indexOf(left.names[0]);
        let rightindex = entryPoints.indexOf(right.names[0]);
        if (leftIndex > rightindex) {
            return 1;
        }
        else if (leftIndex < rightindex) {
            return -1;
        }
        else {
            return 0;
        }
    }
    }),
    new BaseHrefWebpackPlugin({}),
    new CommonsChunkPlugin({
      "name": "inline",
      "minChunks": null
    }),
    new CommonsChunkPlugin({
      "name": "vendor",
      "minChunks": (module) => module.resource && module.resource.startsWith(nodeModules),
      "chunks": [
        "main"
      ]
    }),
    new ExtractTextPlugin({
      "filename": "[name].bundle.css",
      "disable": true
    }),
    new LoaderOptionsPlugin({
      "sourceMap": false,
      "options": {
        "postcss": [
          autoprefixer(),
          postcssUrl({"url": (URL) => {
            // Only convert root relative URLs, which CSS-Loader won't process into require().
            if (!URL.startsWith('/') || URL.startsWith('//')) {
                return URL;
            }
            if (deployUrl.match(/:\/\//)) {
                // If deployUrl contains a scheme, ignore baseHref use deployUrl as is.
                return `${deployUrl.replace(/\/$/, '')}${URL}`;
            }
            else if (baseHref.match(/:\/\//)) {
                // If baseHref contains a scheme, include it as is.
                return baseHref.replace(/\/$/, '') +
                    `/${deployUrl}/${URL}`.replace(/\/\/+/g, '/');
            }
            else {
                // Join together base-href, deploy-url and the original URL.
                // Also dedupe multiple slashes into single ones.
                return `/${baseHref}/${deployUrl}/${URL}`.replace(/\/\/+/g, '/');
            }
        }})
        ],
        "sassLoader": {
          "sourceMap": false,
          "includePaths": []
        },
        "lessLoader": {
          "sourceMap": false
        },
        "context": ""
      }
    }),
    new AotPlugin({
      "mainPath": "main.ts",
      "hostReplacementPaths": {
        "environments/environment.ts": "environments/environment.ts"
      },
      "exclude": [],
      "tsConfigPath": "src/tsconfig.app.json",
      "skipCodeGeneration": true
    })
  ],
  "node": {
    "fs": "empty",
    "global": true,
    "crypto": "empty",
    "tls": "empty",
    "net": "empty",
    "process": true,
    "module": false,
    "clearImmediate": false,
    "setImmediate": false
  }
};
