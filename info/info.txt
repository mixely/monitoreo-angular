===============================================
Esta información la saqué de .angular-cli.json:
===============================================

"assets/global/plugins/amcharts/amcharts/amcharts.js",
"assets/global/plugins/amcharts/amcharts/serial.js",
"assets/global/plugins/amcharts/amcharts/pie.js",
"assets/global/plugins/amcharts/amcharts/radar.js",
"assets/global/plugins/amcharts/amcharts/themes/light.js",
"assets/global/plugins/amcharts/amcharts/themes/patterns.js",
"assets/global/plugins/amcharts/amcharts/themes/chalk.js",
"assets/global/plugins/amcharts/ammap/ammap.js",
"assets/global/plugins/amcharts/ammap/maps/js/worldLow.js",
"assets/global/plugins/amcharts/amstockcharts/amstock.js",

"assets/global/plugins/jqvmap/jqvmap/jquery.vmap.js",
"assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js",
"assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js",
"assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js",
"assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js",
"assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js",
"assets/global/plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js",

"assets/layouts/layout4/scripts/demo.min.js",

"assets/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js",

"assets/global/plugins/flot/jquery.flot.min.js",
"assets/global/plugins/flot/jquery.flot.resize.min.js",
"assets/global/plugins/flot/jquery.flot.categories.min.js",
